# Implementation

\chapterquote{If you are not monitoring stuff, it is actually out of control.}{John Wilkes}{10.5cm}
\noindent
This chapter presents a monitoring infrastructure and autoscaling policies for a Kubernetes-based application in a production-grade environment.
It details the technical implementation necessary to identify and expose relevant metrics from the target application and execution environment;
aggregate and visualize those metrics with a modern monitoring solution;
install VPA, HPA and KEDA autoscaling components;
and configure the autoscaling policies.
<!-- In order to quantify the improvements made by the autoscaling components, we also develop performance and cost benchmarks for the target application. -->

The application in which we introduce and evaluate autoscaling capabilities is *Ericsson Security Manager* (ESM).
Among other features, it provides policy-based security automation, compliance monitoring and security analytics functions for telecommunication infrastructure.
It aids network operators to automate security controls and maintain them in the desired state.
As the complexity of modern telecommunication infrastructure grows, it is crucial for these systems to be configured securely and remain that way.

One part of the target application is responsible for connecting to the external systems, checking their security settings and if necessary re-configuring them.
The architectural design of the application which covers this functionality is shown in Figure \ref{fig:app-architecture}.
The API server accepts commands from the user, such as *"check security settings of system A and B"*.
It then fetches the necessary connection details from the database and forwards detailed task instructions via the message queue to an executor.
To minimize the security risk of connecting to external services, the executor acts as a "dumb client" for these tasks.
This means that it does not have access to any other parts of the application and only executes the given tasks.
Once finished, it returns the results of the operations to the API server through the message queue.
Finally, the API server stores the results in a database and makes them available to the user.
As connecting to external systems and running these tasks takes time, this entire process is asynchronous.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/target-application-architecture.pdf}
\caption{\label{fig:app-architecture} Partial software architecture of target application}
\end{figure}

The goal of our work in this chapter is to dynamically scale the executor component based on the current load.
This includes setting up monitoring for the system, identifying relevant metrics and setting up autoscaling components based on those metrics.
While we focus our implementation on the target application, the methods and findings are generalizable to any application that uses batch- and queue-based processing.
<!-- At the end we performs detailed measurements to assess the results. -->

## Monitoring

*Monitoring* is the first phase of the *MAPE-K* control loop (Section \ref{autoscaling-in-the-cloud}).
It refers to observing the state of the execution environment to detect failures, trigger alerts and provide information about overall system health.

Modern monitoring systems are based on metrics.
A *metric* is a numeric value of information represented as a time-series, i.e., each value is associated with a unique timestamp.
A *service-level indicator* (SLI) is a metric which measures a specific dimension of the *quality of service* (e.g., response time, error rate).
A *service-level objective* (SLO) defines the range of acceptable values for an SLI within which the service is considered to be in a healthy state.
A *service-level agreement* (SLA) can be based on an SLO and is a formal commitment from a service provider towards its users.
It usually also specifies responsibilities and compensation when an SLO is violated \cite{UnderstandingDistributedSystems_2021}.

<!-- Our monitoring efforts (described in the following section) have identified two scenarios for scaling the executor part of the target application. -->
<!-- First, the executor is under heavy load while it is performing the configuration tasks. -->
<!-- Second, once the tasks are completed, the API server is under heavy load, as it needs to parse the returned data and store it in the database. -->

To collect low- and high-level metrics about the target application, we use an industry-standard monitoring stack consisting of Prometheus and Grafana (shown in Figure \ref{fig:monitoring-prometheus-grafana}).
Prometheus is a time-series database developed to collect and store numeric values (indeed "metrics") \cite{PrometheusDocumentation_2021}.
Grafana is a visualization tool that can use Prometheus as a data source for plotting graphs, heatmaps and diagrams.
This means Grafana itself does not store any data, but fetches the relevant data from Prometheus on the fly using the PromQL query language.

\begin{figure}[ht]
\centering
\includegraphics[width=0.65\textwidth]{images/monitoring_prometheus_grafana.pdf}
\caption{\label{fig:monitoring-prometheus-grafana} Logical view of monitoring infrastructure with Prometheus and Grafana}
\end{figure}

Prometheus itself does not extract metrics from the system or application, but rather relies on so-called *exporters*^[<https://prometheus.io/docs/instrumenting/exporters/>].
These exporters expose relevant metrics through an HTTP endpoint in a plaintext format^[<https://prometheus.io/docs/instrumenting/exposition_formats/>], which then gets queried periodically (according to the *scrape_interval*) --- this process is referred to as *scraping*.
For many commonly used services (databases, message queues, operating systems etc.) open-source exporters already exist^[<https://exporterhub.io/>].
A *custom* exporter needs to be developed to expose metrics from a proprietary or novel application.

We install and configure Prometheus and Grafana with Helm Charts as shown in Appendix \ref{prometheus-setup} and \ref{grafana-setup}, respectively.
Helm Charts^[<https://helm.sh/>] provide a convenient and declarative way to install complex applications into a Kubernetes cluster.
<!-- The installation routine is configured with a *values* file in YAML format. -->
This declarative installation makes our research reproducible in any Kubernetes environment.

It is important to keep in mind that Prometheus is fundamentally a pull-based monitoring solution.
This means the Prometheus server opens a connection to the exporters, not the other way around (push-based).
Thus, the network topology and firewalls must allow this.
In Kubernetes, this is configured through Network Policies^[<https://kubernetes.io/docs/concepts/services-networking/network-policies/>].

Prometheus offers built-in support for Kubernetes service discovery, therefore *scraping targets* (the endpoints from which Prometheus collects metrics) do not need to be defined statically in a configuration file.
Instead, they can be enabled and configured with Kubernetes annotations.
Annotations are small pieces of key-value metadata which can be attached to Kubernetes Pods, Services or Ingresses.
Prometheus then automatically discovers the objects with appropriate labels and starts scraping the associated endpoint.
An example of such a Service annotation is shown Appendix \ref{prometheus-service-discovery-with-kubernetes}.
This behavior is an instance of a self-configuring system according to the concept of autonomic computing \cite{VisionAutonomicComputing_2003}.

## Prometheus Exporters

Since Prometheus itself does not extract metrics from an application, installing and configuring special exporters is necessary.
This section documents which exporters have been tried and deployed to collect metrics as well as the purpose they serve.
We start with low-level metrics and gradually move towards higher-level metrics.
It is important to note that only when metrics from different sources (exporters) are combined, it is possible to obtain a comprehensive view of the application behavior and performance.

The *kube-state-metrics* exporter^[<https://github.com/kubernetes/kube-state-metrics/tree/v1.9.8>] exposes details about the state of objects managed by Kubernetes' control plane.
For example, it reports the number of Deployments and Pods as well as configuration of these objects (e.g., resource requests and limits) to Prometheus.
It should be noted that these metrics only describe the state of virtual objects.
To get real-time information about the state of the Kubernetes worker nodes (CPU, memory, I/O utilization), Prometheus is configured to scrape metrics from *cAdvisor* (refer to Section \ref{kubernetes-components}).
<!-- It provides usage statistics about each container running on a node. -->
Combining these two data sources yields insight into how individual Pods and containers are behaving, as shown in Figure \ref{fig:dashboard-pod-details}.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/dashboard-pod-details.png}
\caption{\label{fig:dashboard-pod-details} Grafana screenshot with Container CPU and Memory Details. Red lines indicate resource limits, blue lines resource requests. Yellow and green lines are actual utilization (two replicas). Purple lines indicate VPA recommendations.}
\end{figure}

Next, we looked into extracting metrics from *RabbitMQ*, the message queue used in the target application.
RabbitMQ comes with a plugin that only needs to be enabled for exposing a metrics endpoint^[<https://www.rabbitmq.com/prometheus.html>].
After setting up a dashboard and observing the metrics, we had to realize that these metrics are useful for accessing the status and health of RabbitMQ itself, but are not detailed enough for our purposes.
In particular, it was not exposing statistics about individual channels and queues (e.g., queue length).
Thus, it was decided to use a third-party exporter for RabbitMQ^[<https://github.com/kbudde/rabbitmq_exporter>].
<!-- which is installed as a sidecar. -->
This exporter gives access to detailed statistics, which are necessary to differentiate the messaging activities of different microservices.

<!-- This is due to the fact that several microservices are using the message queue, but this level of detail is lost in the metrics exposed by RabbitMQ. -->
<!-- For example, the total number of incoming and outgoing messages as well as the number of producers and consumers are exposed, but this information is not differentiated across different message types and microservices. -->
<!-- In summary, while these metrics is helpful for performance troubleshooting (e.g., to identify a bottleneck in the message queue), they are not suited for scaling our target application. -->

Afterwards, we proceeded to a higher level and looked at logical tasks.
The target application uses the *Celery* Python framework^[<https://docs.celeryproject.org/en/stable/>] for sending tasks between microservices.
Celery is using RabbitMQ as a message broker, but provides higher level concepts to the application (e.g., scheduling, rate limiting, persistence) and decouples it from the specific message broker implementation.
Two open-source Prometheus exporters for Celery^[<https://github.com/danihodovic/celery-exporter>] ^[<https://github.com/OvalMoney/celery-exporter>] are available.
Fundamentally, they both work in the same fashion, but expose slightly different metrics.
We found that the metrics these exporters provide give more useful insights than the raw metrics from RabbitMQ, because they differentiate between tasks of different types (not just messages on a queue) and higher-level semantics (e.g., task cancelled).
<!-- Exemplary, this allows tracking how many tasks of a certain type are active at any given time. -->

<!-- On one hand, this is due to the architecture of the target application. -->
<!-- When an action needs to be performed (e.g., a security policy needs to be enforced), one microservice sends a task to another microservice. -->
<!-- The response is also sent as task. -->
<!-- Through the Celery exporters we are able to measure how many and when each of those task types are sent. -->
<!-- But it makes it very difficult to track metrics like how long the task execution took, since two individual tasks cannot be correlated with each other in the monitoring system. -->
<!-- On the other hand, it is also due to business logic inside the application. -->
Still, these metrics did not give us a full picture, because any moderately complex application is more than just the sum of its components.
To accurately measure (and possibly predict) the application performance, also the state of the application and business logic (that connect the individual components) needs to be captured.
Therefore, we decided to build a custom exporter for our target application.
This allows us to extract high-level application metrics which are not accessible otherwise.
For example, our target application allows running tasks according to a schedule.
These tasks are then stored in a database until a certain condition is met, then they are sent to the executor over the message queue.
In this case, simply looking at the tasks in the message queue would give an incomplete picture.

As the name suggests, where and how a custom exporter obtains metrics is highly specific to the application.
Nevertheless, the following section documents a brief example of how to conduct such an endeavor.
Our implementation is written in Go and uses the official Prometheus client library^[<https://github.com/prometheus/client_golang/tree/v1.10.0>].
The library provides a framework for exposing metrics via HTTP.
Specifically, each time a client connects to the HTTP endpoint, the `Collect` method of the associated collectors is called (Listing \ref{src:custom-exporter}).

The management interface of our target application exposes all running, finished and scheduled tasks in JSON format.
The exporter shown in Listing \ref{src:custom-exporter} queries the relevant API endpoint (line 3), parses the data and walks through the list of tasks (line 7).
It categorizes the tasks based on their status (running, finished, failed etc.) and counts the number of tasks in each category (line 8).
This number is then published through the HTTP endpoint of the exporter (line 13).
In this case, the type of metric is a *Gauge* (line 15): an arbitrarily increasing and decreasing value, e.g., number of active tasks \cite{PrometheusDocumentation_2021}.

\begin{lstlisting}[language=Go, numbers=left, caption=Sample code of custom exporters, label=src:custom-exporter]
func (c *Collector) Collect(channel chan<- prometheus.Metric) {
    // query management interface for task information
    tasks, _ := c.queryConfigurationApi()

    // analyze all tasks
    results := make(map[string]float64)
    for _, task := range tasks {
        results[task.Result] += 1
    }

    // publish metric via HTTP endpoint
    for status, count := range results {
        channel <- prometheus.MustNewConstMetric(
            c.tasks, // contains metric name and description
            prometheus.GaugeValue, // type of metric
            count, status) // metric value and metric label
    }
}
\end{lstlisting}

The code in Listing \ref{src:custom-exporter} produces the partial output shown in Listing \ref{src:metric-output} on the `/metrics` HTTP endpoint.

\begin{lstlisting}[language=bash, numbers=none, caption=Sample metric output, label=src:metric-output]
# HELP esm_tasks_total Total number of tasks labeled by status.
# TYPE esm_tasks_total gauge
esm_tasks_total{status="failed"} 0
esm_tasks_total{status="running"} 42
esm_tasks_total{status="finished"} 7
\end{lstlisting}

Additionally, we implemented several other metrics with types Counter and Histogram.
A *Counter* is a monotonically increasing value, which is only reset when restarting the service \cite{PrometheusDocumentation_2021}.
Exemplary, it can be useful for describing the total number of tasks created by the application.
A *Histogram* samples observations into buckets with pre-configured sizes \cite{PrometheusDocumentation_2021}.
It can be used to describe the duration of requests, for instance 0-10ms, 10-100ms, 100ms-1s, 1-10s etc.
A histogram provides a balance between tracking the duration of each task individually (which has high *cardinality*, i.e., expensive in terms of bandwidth and storage resources) and aggregating into mean, minimum and maximum values (which loses information about distribution and outliers).

While developing this custom exporter, we followed the best practices and conventions for writing Prometheus exporters^[<https://prometheus.io/docs/instrumenting/writing_exporters/>] and metric naming^[<https://prometheus.io/docs/practices/naming/>].
The custom exporter was packaged into a container image and deployed alongside the target application.
We were able to confirm that it provides useful high-level metrics about the application by setting up a Grafana dashboard and visualizing the metrics as time-series graphs.
The exposed metrics allow reasoning about the application behavior and making appropriate scaling decisions based on the collected metrics.

\begin{figure}[ht]
\centering
\includegraphics[width=0.9\textwidth]{images/scaling-flow.pdf}
\caption{\label{fig:scaling-flow} Flow of metrics used for scaling. Arrows denote the logical flow of data. Orange arrows symbolize raw HTTP metrics.}
\end{figure}

To use these custom metrics with Kubernetes HPA (Section \ref{horizontal-pod-autoscaler}), another component needs to be installed into the cluster: a *metrics adapter* (Figure \ref{fig:scaling-flow}).
This component is responsible for translating the metrics from Prometheus into a format compatible with the Kubernetes metrics API (Section \ref{kubernetes-components}).
We choose the *prometheus-adapter* project^[<https://github.com/kubernetes-sigs/prometheus-adapter>] for this purpose as our use case focuses solely on Prometheus metrics.
Another project with a similar goal is *kube-metrics-adapter*^[<https://github.com/zalando-incubator/kube-metrics-adapter>] which allows utilizing a wider range of data sources, for example InfluxDB or AWS SQS queues.
The installation of the adapter was performed with a Helm Chart and is detailed in Appendix \ref{prometheus-adapter-setup}.
In essence, the adapter is configured with a PromQL query it should execute.
The query can be parameterized with several labels and parameter overrides.
The result of this query is exposed as a new metric through Kubernetes' metrics API (Figure \ref{fig:scaling-flow}).

## Autoscaling Setup

<!-- After having successfully set up the entire monitoring pipeline, the next task is configuring Kubernetes' autoscalers for these metrics. -->
The previous section outlined the setup of the entire monitoring pipeline.
This section details how Kubernetes' autoscalers need to be configured to use the collected metrics.
As outlined in Chapter \ref{autoscaling}, scaling can be performed in two dimensions: horizontally and vertically.

*Horizontal scaling* (scaling in and out) refers to creating more replicas of the same Pod.
Assuming that the workload is automatically distributed across all instances, scaling out results in a lower workload per replica on average.
It should be noted that this assumption does not always hold true.
Special attention needs to be paid to (partially) stateful services.
Nguyen and Kim \cite{HighlyScalableLoadBalancing_2020} performed an investigation of load balancing stateful applications on Kubernetes.
They found that especially distribution and load balancing of leaders throughout the cluster are important for maximizing performance when scaling horizontally.

Two other aspects need to be considered when implementing horizontal scaling on Kubernetes: microservice startup and shutdown.
These aspects are particularly important when using autoscaling, since individual Pods are frequently created and removed at all times.

If the Pods of a microservice are exposed with a Kubernetes Service (see Section \ref{kubernetes-objects}), the Pods should have *readiness probes* configured.
Based on this probe Kubernetes determines if the application is ready to handle requests (e.g., after it has finished its startup routine).
Only then Kubernetes starts routing network traffic to a newly started Pod \cite{KubernetesPatterns_2019}.
This way the new replicas handle part of the incoming load as soon as possible -- but not too early -- while scaling out.

Any application running as a distributed system should implemented *graceful shutdown* (or *graceful termination*):
when a Pod is shut down, Kubernetes stops routing new traffic to the replica and sends the Pod a SIGTERM signal;
the application should finish serving the outstanding requests it has accepted and terminate itself afterwards \cite{KubernetesPatterns_2019}.

*Vertical scaling* (scaling up and down) refers to adjusting requested resources (compute, memory, network, storage) allocated to a service based on the actual usage.
By giving more resources to one or multiple instances, they are able to handle more workload.
While most industry practitioners only focus on scaling up (allocating more resources), the opposite is actually far more desirable: scaling down.
The *Autopilot* paper from researches at Google shows that significant cost savings can be realized by automatically adjusting the allocated resources, i.e., vertical scaling \cite{AutopilotWorkloadAutoscalingGoogle_2020}.

Some of the research proposals discussed in Section \ref{research-proposals-for-kubernetes-autoscalers} have shown potential to be effective and cost-efficient autoscalers, but none of them offer a publicly available implementation.
For this reason, HPA (Horizontal Pod Autoscaler, Section \ref{horizontal-pod-autoscaler}), VPA (Vertical Pod Autoscaler, Section \ref{vertical-pod-autoscaler}) and KEDA (Kubernetes Event-driven Autoscaler, Section \ref{kubernetes-event-driven-autoscaler}) were chosen for autoscaling:
they are widely deployed in the industry and their implementations are battle-tested.
Furthermore, they feature a plethora of configuration options to adjust the scaling behavior.
This allows developers and administrators to fine-tune the scaling behavior to their \mbox{use cases} and goals.
These options -- as well as their effects -- will be explored in the following sections.


### Vertical Scaling with VPA

<!-- We started off by setting up the VPA. -->
Since VPA is an external component not included in a standard Kubernetes distribution, it needs to be installed separately.
The installation process with a third-party Helm Chart as well as the configuration options to connect VPA to Prometheus are shown in Appendix \ref{vpa-setup}.
Notably, we only configured the *Recommender* component, but not the *Updater* or *Admission Controller* (see Section \ref{vertical-pod-autoscaler}).
This decision was made because as of Kubernetes 1.20, the resource allocation of a created Pod is immutable.
Since only the *PodTemplate* can be modified, the Pod needs to be deleted and created from scratch for new resource requests and limits to take effect \cite{KubernetesPatterns_2019}.
Thus, it has potential for service disruption, especially when the number of Pod replicas is small (removing 1 out of 100 Pod replicas does not make a significant difference, but removing 1 out of 3 replicas can impact overall service health).
Nevertheless, the VPA Recommender can be a useful tool for determining appropriate resource requests and limits, as we show in the following section.

After the component is installed into the cluster, VPA needs to be instructed to monitor our application so that it can build its internal resource usage model and produce an estimate.
VPA is enabled and configured for each application running on Kubernetes individually.
<!-- In our case, we are configuring it for a *Deployment* (see Section \ref{kubernetes-objects}). -->
This is done through a special object called *Custom Resource Definition*, short *CRD*.
CRDs act just like Kubernetes core objects, however they are not implemented by the *Controller Manager* (Section \ref{kubernetes-components}), but through an external component --- in this case: the VPA.

Listing \ref{src:vpa-crd-executor} shows the CRD for configuring VPA to monitor the `executor` Deployment (line 6-9).
VPA is instructed to only provide resource recommendations, but not change the configuration of running Pods (lines 10-11).
We configure VPA to monitor a specific container in the Pod (this avoids interference with sidecar containers) and the types of resources (lines 13-16).
CPU and memory are the only resources supported by VPA.
The CRD is added to the cluster with `kubectl apply` in the same namespace as the target Deployment.

\clearpage

\begin{lstlisting}[language=yaml, numbers=left, caption=Vertical scaling configuration CRD for VPA, label=src:vpa-crd-executor, xleftmargin=\parindent]
apiVersion: "autoscaling.k8s.io/v1"
kind: VerticalPodAutoscaler
metadata:
  name: executor
spec:
  targetRef:
    apiVersion: "apps/v1"
    kind: Deployment
    name: executor
  updatePolicy:
    updateMode: "Off" # Recommendation only
  resourcePolicy:
    containerPolicies:
      - containerName: "executor"
        mode: "Auto"
        controlledResources: ["cpu", "memory"]
\end{lstlisting}

Once the VPA has been able to collect metrics for a while, the resource request and limit recommendations can be retrieved as shown in Listing \ref{src:vpa-recommendation-example}.
Section \ref{vertical-pod-autoscaler} explains the meaning and calculations behind these values.
For our use case, only the upper bound and target recommendations are relevant.
The upper bound can be used as the resource limit, the target value can be used as a resource request for the container.

\begin{lstlisting}[language=yaml, numbers=none, caption=VPA Resource Recommendations, label=src:vpa-recommendation-example]
$ kubectl describe vpa/executor
  [...]
  Recommendation:
    Container Recommendations:
      Container Name: executor
      Lower Bound:
        Cpu:     15m
        Memory:  163355301 # 163 MB
      Target:
        Cpu:     763m
        Memory:  163378051 # 163 MB
      Uncapped Target:
        Cpu:     763m
        Memory:  163378051 # 163 MB
      Upper Bound:
        Cpu:     1045m
        Memory:  174753812 # 175 MB
\end{lstlisting}

Thanks to the extensive monitoring setup deployed in Section \ref{prometheus-exporters}, the operation of the VPA can be visualized with Grafana.
The purple lines in Figure \ref{fig:dashboard-pod-details} indicate the VPA recommendations for the memory and CPU resource.
It can be seen that over time VPA is adjusting the recommendations based on the actual usage of the Deployment.
The longer the Deployment is running with a production workload, the more accurate the VPA estimates are.

As mentioned previously, we are only using the VPA as a tool to provide resource recommendations during development, not for setting the resource values at runtime in production environments.
We plan to use this VPA deployment in a stability and performance test environment, where it can observe the application over a long period of time during a high-load scenario.
This will help guide the developers to make educated decisions about the appropriate resource requests and limits for their containers.
A tool like *Goldilocks*^[<https://goldilocks.docs.fairwinds.com/>] can provide a dashboard to visualize the recommendations and make them easily accessible through a web interface.

### Horizontal Scaling with HPA

This section details the configuration of Kubernetes' Horizontal Pod Autoscaler (HPA) \cite{KubernetesDocumentationHorizontalPod_2021}.
As outlined Section \ref{horizontal-pod-autoscaler}, HPA is implemented in the Controller Manager and therefore part of every Kubernetes installation.
Therefore, no installation is required, HPA just needs to be configured for each scaling target.

The goal of the HPA in our scenario is to give the application similar performance to a static overprovisioning of resources<!-- (low average queue time and time to completion) -->, while keeping the cost <!-- (replica seconds) --> at a minimum.
The exact metrics for quantifying these dimensions are discussed in Chapter \ref{evaluation}.
Additionally, the autoscaler should be able to find this optimum trade-off with varying workload sizes.
Mathematically, the resulting system can be described as a queuing system where the number of workers is adjusted dynamically based on the queue length \cite{QueueingSystemOnDemandNumber_2012}.

The most minimal horizontal scaling policy could be applied with the command `kubectl autoscale executor --cpu-percent=50`.
This would scale the number of replicas based on the average CPU load across all Pods in the Deployment.
However, as discussed at the beginning of this chapter, our workload is neither purely CPU nor memory bound, but also by the throughput of external systems.
Thus, we need to scale this Deployment with a high-level metric which we exposed in Section \ref{prometheus-exporters}.

Based on empirical observations and experiments, we identified the current queue length (used in Figure \ref{fig:hpa-scaling-v0}) as a meaningful autoscaling metric.
Thanks to the metrics adapter installed in Section \ref{prometheus-exporters}, we can configure the HPA to scale based on external metrics, as shown in \mbox{Listing \ref{src:hpa-scale-v0}}.
The object structure is similar to the CRD of the VPA.
It specifies a *target* -- the Kubernetes object which should be scaled (line 6-9) -- and based on which metric it should be scaled (line 13-16).
<!-- We are using an external metric (line 13-16) based on which the target is scaled. -->
The goal of the HPA is to make the metric value equal to target value (line 17-19) by adjusting the number of Pods.
When the metric value is above the target, it creates more instances.
When the metric value is below the target, it removes instances.
For details about the algorithm refer to Section \ref{horizontal-pod-autoscaler}.
Additionally, we specify safety bounds: the Deployment must have at least 1 replica (line 10) and at most 20 replicas (line 11).
This is an important engineering practice to guard against bugs and misconfiguration (e.g., the unit of the metric value changes from seconds to milliseconds), which could lead to automatic creation of large numbers of replicas.

After applying the `HorizontalPodAutoscaler` object  (shown in Figure \ref{src:hpa-scale-v0}) to the cluster, the current configuration as well as operation of HPA can be observed on the command line (Appendix \ref{hpa-log-messages}) as well as visually with the monitoring setup (\mbox{Figure \ref{fig:hpa-scaling-v0}}).

\begin{lstlisting}[caption=Initial HPA Scaling Policy (\texttt{hpav0}), label=src:hpa-scale-v0, language=yaml, numbers=left]
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: executor # name of the autoscaling object
spec:
  scaleTargetRef: # Deployment to be scaled
    apiVersion: apps/v1
    kind: Deployment
    name: executor
  minReplicas: 1 # safety bounds
  maxReplicas: 20
  metrics:
  - type: External
    external:
      metric: # scale based on this external metric
        name: esm_tasks_queued_total
      target: # scale until this value is reached
        type: Value
        value: 1
\end{lstlisting}

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/hpa-scaling-v0.png}
\caption{\label{fig:hpa-scaling-v0} Grafana screenshot of scaling behavior with initial HPA policy (\texttt{hpav0}). Task queue length in green, number of replicas in red.}
\end{figure}

### Horizontal Scaling with KEDA

This section describes the setup and configuration of the *Kubernetes Event-driven Autoscaler* (KEDA).
As noted in Section \ref{kubernetes-event-driven-autoscaler}, KEDA significantly reduces the number of components required to use custom metrics for autoscaling in Kubernetes.
A monitoring tool, exporters and a metrics adapter (as documented in the previous sections) are not required for using KEDA.
The installation procedure with KEDA's Helm Chart is shown in Appendix \ref{keda-setup}.

Similar to the previous section, we will use the RabbitMQ message broker as a trigger for horizontal scaling (Listing \ref{src:keda-v1}, line 14).
Specifically, KEDA is configured to scale the `executor` Deployment based on the number of messages in a specific queue (line 18-20).
Alternatively to scaling based on queue length, the rate of messages could also be used.

Listing \ref{src:scaledobject} in Appendix \ref{keda-setup} shows the status of KEDA's `ScaledObject`, the HPA object (created by KEDA) and the Deployment after applying the CRD from \mbox{Listing \ref{src:keda-v1}}.
Of particular note is that the HPA object has a minimum Pod count of one, but the KEDA agent scales the Deployment to zero replicas anyway.
This allows saving resources when there are no tasks for the system.
In the following chapter we evaluate the effectiveness of this *scale-to-zero* behavior and which side-effects it has.

\begin{lstlisting}[caption=ScaledObject CRD for KEDA autoscaling, label=src:keda-v1, language=yaml, numbers=left]
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
  name: exec-so
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: executor
  pollingInterval: 15
  cooldownPeriod: 60
  maxReplicaCount: 50
  triggers:
  - type: rabbitmq
    metadata:
      protocol: http
      host: 'http://user:password@rabbitmq.namespace.svc:15672/'
      queueName: executorTasks
      mode: QueueLength
      value: '1'
\end{lstlisting}
