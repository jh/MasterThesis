# Autoscaling

\chapterquote{Theoretically-obtainable efficiencies are often hard to achieve in practice \\
because the effort or risk required to do so manually is too high. \\
What we need is an automated way of making the trade-off.}{Rzadca et al. \cite{AutopilotWorkloadAutoscalingGoogle_2020}}{12.6cm}
\noindent
This chapter first introduces the autoscaling concept from first principles and provides an overview of relevant cloud autoscaling literature.
Next, publicly available  autoscaling components for Kubernetes are presented.
Finally, we survey research proposals for novel Kubernetes autoscalers and perform a qualitative evaluation.

*Elasticity* is the ability of a system to increase or decrease the allocated resources on demand \cite{NISTDefinitionCloudComputing_2011}.
*Autoscaling* (sometimes also referred to as *adaptive scaling*) is the process of adjusting the amount of available resources to the current demand.
It has been extensively researched over the last decades \cite{DynamicserverQueuingSimulation_1998, EnergyAwareServerProvisioningLoad_2008}, also outside of the realm of computer science \cite{MMPPQueueCongestionbasedStaffing_2019}.
In particular the widespread adoption of the cloud computing paradigm has accelerated the pace of development and research in this field.

With the proliferation of container orchestration frameworks over the last five years, the topic of container autoscaling has seen particular attention.
In this chapter we specifically focus on autoscaling techniques for the Kubernetes framework.
It has seen the largest adoption in industry as well as academia in recent years.
A large ecosystem of open-source technologies, startups and business models has evolved around it, making it very likely to remain popular in the future.
Other orchestration frameworks are either rarely used (like Mesosphere Marathon) or gradually being phased out by their developers (like Docker Swarm)^[<https://thenewstack.io/mirantis-acquires-docker-enterprise/>].

## Autoscaling in the Cloud

The following section gives an overview of research on the topic of autoscaling and elasticity of cloud infrastructure and applications.

In general, autoscaling approaches are based on the broadly recognized *MAPE-K* control loop.
MAPE-K stands for *Monitor*, *Analyse*, *Plan* and *Execute* over a *shared Knowledge base* \cite{VisionAutonomicComputing_2003}.
It is an instance of a feedback loop widely used for building self-adaptive software systems \cite{AutoScalingWebApplicationsClouds_2018}.
In the monitor phase the execution environment is observed.
The observed data (i.e., metrics) is then used in the analyse phase, which determines whether any adaptation of the system is required.
In the plan phase the appropriate actions to adapt the system are evaluated and a final selection is made, considering feasible adaptation strategies such as scaling up or down.
In the execute phase the system is changed to match the new desired state proposed in the plan phase.
The knowledge base is used as a central store of information about the entire execution environment (i.e., a database) \cite{KeyInfluencingFactorsKubernetes_2020}.

In 2014, Lorido-Botran et al.\ \cite{ReviewAutoscalingTechniquesElastic_2014} published a comprehensive survey on resource estimation techniques for scaling application infrastructure.
Their survey is one of the seminal works in this field.
It gives an overview of 50 research proposals and classifies them into five categories for autoscaling approaches:
threshold-based rules, control theory, reinforcement learning, queuing theory and time-series analysis.
We  go into the details of these categories later.
At that time, containers were not widely used and therefore all of the autoscaling proposals were focused on virtual machines.
Furthermore, the landscape of cloud-native applications and infrastructure was still in its infancy, which complicated a direct comparison between different approaches.

In 2015, Galante et al.\ \cite{AnalysisPublicCloudsElasticity_2016} published a survey on the use of elastic cloud computing for scientific applications.
Their work describes several approaches, advantages and drawbacks of running scientific computations in the cloud.
Their presentation of elasticity mechanisms of public cloud providers focused on IaaS (*Infrastructure-as-a-Service*) and PaaS (*Platform-as-a-Service*) solutions.
They found that most traditional scientific applications are executed in batch mode and have difficulty adapting to dynamic changes in the infrastructure (e.g., addition or removal of one or more worker nodes).
Among other challenges, they found that the elasticity mechanisms lacked good support for scientific batch workloads, as they were more focused on server-based applications (web servers etc.).
They also recognized the lack of cloud interoperability.

In 2016, Hummaida et al.\ \cite{AdaptationCloudResourceConfiguration_2016} published a survey that focused on efficient resource reconfiguration in the cloud from the perspective of the infrastructure provider, instead of the user.
In this context, they defined *Cloud Systems Adaptation* as "*a change to provider revenue, data centre power consumption, capacity or end-user experience where decision making resulted in a reconfiguration of compute, network or storage resources*." \cite{AdaptationCloudResourceConfiguration_2016}

Another large scale survey on the challenges of autoscaling was published by Qu et al.\ in 2018 \cite{AutoScalingWebApplicationsClouds_2018}.
Additionally, they provided a detailed taxonomy of cloud application autoscaling, which we apply in our work.
Also their work focused only on autoscaling proposals for virtual machine based infrastructure, however many of the concepts applied to VMs can also be applied to containers, sometimes even in better ways.
<!-- e.g., faster startup of containers compared to VMs, smaller size allows higher density -->

As well in 2018, Al-Dhuraibi et al.\ \cite{ElasticityCloudComputingState_2018} published a similar survey of state-of-the-art techniques and research challenges in autoscaling.
Their work was the first survey that included both VM- as well as container-based solutions.
While their survey included relevant container orchestration tools at the time, none of the research proposals for autoscaling were developed for Kubernetes.
Furthermore, their survey also presented a taxonomy for cloud application elasticity, which we partially base our classification on.

More recently, Radhika and Sadasivam \cite{ReviewPredictionBasedAutoscaling_2021} provided a study on proactive autoscaling techniques for heterogeneous applications.
None of the proposals evaluated in the work was developed for Kubernetes.

## Built-in Kubernetes Autoscalers

The Kubernetes authors have developed three components that enable elastic scaling of clusters: *Horizontal Pod Autoscaler* (HPA), *Vertical Pod Autoscaler* (VPA) and *Cluster Autoscaler* (CA).
*Kubernetes Event-driven Autoscaler* (KEDA) is a third-party component for Kubernetes that builds on top of HPA.
The purpose and operation of these autoscalers are described in the following sections.

### Horizontal Pod Autoscaler

<!-- https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/ -->

Kubernetes' *Horizontal Pod Autoscaler* (HPA) is one of the control loops integrated in the Controller Manager (Section \ref{kubernetes-components}).
It allows dynamically adjusting the number of Pod replicas for a particular Deployment by consuming the Metrics API \cite{KubernetesDocumentationHorizontalPod_2021}.

By default, HPA scales Pods based on relative CPU utilization.
However, memory utilization, custom and external metrics are also supported.
CPU and memory utilization on the worker nodes are collected with cAdvisor.
Custom or external metrics can be utilized by querying a custom metrics API.
The kube-metrics-adapter project^[<https://github.com/zalando-incubator/kube-metrics-adapter>] offers adapters to convert metrics from popular third-party metric services (such as Prometheus) into a suitable format for HPA.

The core algorithm of HPA is shown in Equation \ref{hpa-algorithm-1} and \ref{hpa-algorithm-2}.
The current metric value $m_i$ is retrieved for all active replicas in the set $R$ and the mean value $\bar{m}$ is calculated.
The target number of replicas $\hat{r}$ is calculated by dividing the \mbox{mean usage $m$} by the desired usage $\hat{m}$, multiplying the result with the current number of \mbox{replicas $r$} and finally rounding up \cite{KubernetesDocumentationHorizontalPod_2021}.

<!-- \begin{equation}\label{hpa-algorithm} -->
<!--   \begin{gathered} -->
<!-- currentMetricValue = \sum_{podReplicas} metricValue \\ -->
<!-- desiredReplicas = \bigg\lceil currentReplicas * \frac{currentMetricValue}{desiredMetricValue} \bigg\rceil -->
<!--   \end{gathered} -->
<!-- \end{equation} -->

\begin{equation}\label{hpa-algorithm-1}
\bar{m} = \frac{\sum_{i \in R} m_i}{R}
\end{equation}

\begin{equation}\label{hpa-algorithm-2}
\hat{r} = \bigg\lceil r * \frac{\bar{m}}{\hat{m}} \bigg\rceil
\end{equation}

As an example, let us assume that the target memory utilization is set to 50%, the Pod memory limit is set to 1 GB and there are currently three Pod replicas which utilize 600 MB, 700 MB and 800 MB, respectively.
In this case, the mean utilization of all Pods is 70%, which is 1.4 times above the target utilization.
Multiplying this number with the current number of replicas yields 4.2, which is rounded up to 5.
Thus, the HPA indicates to the Kubernetes control plane that two more replicas are needed in order to match the target resource utilization.
Of course, a single adjustment might not be enough (due to non-linear scaling of applications and varying load), therefore the HPA runs as a control loop with a default interval of 15 seconds (*sync-period*) \cite{KubernetesDocumentationHorizontalPod_2021}.

In general, the algorithm has a bias towards scaling up faster and scaling down slower.
For example, to avoid *thrashing* (oscillations in the number of Pods) each newly created replica runs for at least one downscale period (by default 5 minutes) before it can be removed again.
The HPA supports several other settings: cluster-wide settings (e.g., scaling stabilization, scaling tolerance, Pod synchronization period) as well as Deployment-wide settings (e.g., specifying a minimum or maximum number of Pod replicas) \cite{KubernetesDocumentationHorizontalPod_2021}.
The authors of \cite{AdaptiveAIbasedAutoscalingKubernetes_2020} have developed a formal, discrete-time queuing model of HPA's algorithm, which gives an approximation of the number of Pods deployed by the autoscaler.
<!-- https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#support-for-configurable-scaling-behavior -->

Scaling decisions taken by HPA are not immediately reflected in the status of the cluster, but first need to propagate through several control plane components (Section \ref{kubernetes-components}).
In particular, Kubernetes needs to perform the following steps before a new Pod is available for handling workloads:

1. The HPA control loop is activated to calculate the new desired number of replicas.
   The result is saved in the ReplicaSet object.
2. The ReplicaSet controller is activated to pick up the changes in the ReplicaSet.
   It creates a new Pod object to fulfill the requirement.
3. The scheduler control loop detects that there is a Pod without an assigned node.
   It selects an appropriate worker node for the new Pod, while taking into account the scheduling policy and cluster status.
   The kubelet on the selected node is notified about the pending Pod.
4. The kubelet on the worker node initiates the Pod creation process.
   This includes downloading container images from the registry and unpacking them, launching and initializing containers associated to the Pod and waiting until it becomes *ready* (indicated through a *Readiness Probe*).

### Vertical Pod Autoscaler

The *Vertical Pod Autoscaler* (VPA) automatically sets resource requests and limits of a Pod based on historical usage.
Thus it allows each Pod to have the necessary resources available while minimizing excess resources (so-called *slack*).
It can both down-scale Pods when the initially requested resources were too high as well as  up-scale Pods when their usage indicates that they under-requested resources \cite{VerticalPodAutoscalingDefinitive_2021}.
This means that the resources in the cluster are used and shared efficiently, because each Pod is adjusted to the amount it currently requires, instead of operating with static thresholds.
Additionally, this improves Pod scheduling on nodes.
VPA can either use the Metrics Server or a Prometheus instance to obtain time-series metrics.
Unlike HPA, VPA only supports scaling based on CPU and memory metrics, but not custom (external) metrics.
This component is not part of a default Kubernetes installation and the following discussion applies to VPA version 0.9.

<!-- Vertical autoscaling is configured with a Custom Resource Definition object called `VerticalPodAutoscaler`, which allows specifying if and how a resource recommendations for a Pods should be applied. -->

The VPA itself is implemented with a microservice architecture consisting of three separate components (Figure \ref{fig:vpa-architecture}):

* The *Recommender* monitors the current and past resource consumption of containers and provides recommended values for CPU and memory requests.
* The *Updater* checks which of the managed Pods have correct resources set and, if the active resources diverge more than 10% from the recommendation, it forwards the recommended values to the Admission Plugin.
  If the state of the cluster allows, it also evicts Pods (terminates running instances) in order for the new resource values to take effect.
  <!-- (*PodDisruptionBudget*^[<https://kubernetes.io/docs/tasks/run-application/configure-pdb/>]). -->
* The *Admission Plugin* intercepts Pod creation requests to set the resource values given by the Updater.

This highlights a major drawback of the current VPA implementation: its operation is disruptive.
In fact, resource adjustments are carried out by terminating the Pod and then re-scheduling it with the newly estimated resources.
This approach works for stateless services (though they may still experience service disruption), but it is a major impediment for stateful and high-performance applications \cite{ExploringPotentialNonDisruptiveVertical_2019}.
As of writing, the Kubernetes authors are working on in-place updates of Pods, which would allow the VPA to operate non-disruptively.^[<https://github.com/kubernetes/enhancements/tree/master/keps/sig-node/1287-in-place-update-pod-resources>]

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/vpa_architecture.pdf}
\caption{\label{fig:vpa-architecture} VPA Architecture}
\end{figure}

Compared to the HPA, the VPA's algorithms are more complex as they also take into account historic usage and cluster events such as out-of-memory errors.
Since the Metrics API only offers access to live usage data, but not historical metrics, the VPA needs to built its own internal model for historical metric values \cite{VerticalPodAutoscalingDefinitive_2021}.
While this kind of complexity is usually undesired by infrastructure operators, this scaling logic has been used successfully in Google's Borg infrastructure.
<!-- https://www.youtube.com/watch?v=Y4vnYaqhS74 -->

For CPU resources, VPA collects historic CPU utilization and samples it into buckets with exponentially growing boundaries, from 0.001 cores up to 1,000 CPU cores with an exponential growing rate of 5%.
For memory resources, VPA collects the memory usage peaks of each 24 hour interval during the last 8 days.
These 8 samples are put into buckets with exponentially growing boundaries of 5% (e.g., 10-11.2MB, 11.2-12.66MB, ...).
Both types of samples then get a decaying weight with a half-life of 24 hours, meaning that the most recent samples get a weight of (slightly below) 1, 24 hour old samples a weight of 0.5 and so on.

Then, three values are calculated (Equation \ref{eq:vpa-estimate}): the lower boundary $b_l$ (50th percentile of historic usage $H$), the target value $t$ (90th percentile of $H$) and the upper boundary $b_u$ (95th percentile of $H$).
Exemplary, the 90th percentile describes the boundary below which the resource utilization is for 90% of the time.
Each of these bounds is then scaled with a safety margin $m$ of 15% \cite{VerticalPodAutoscalingDefinitive_2021}. <!-- (*recommendation margin fraction*). -->

The target value $t$ is the recommended *resource request* for the Pod.
The *resource limit* is either scaled proportionally to the initial ratio between request and limit or set to a specific maximum.
For example, when the initial request is 100MB with a limit of 200MB, and VPA recommends the request to be 175MB, then the proportionally scaled limit will be 350MB (unless a LimitRange^[<https://kubernetes.io/docs/concepts/policy/limit-range/>] is specified).

Finally, the calculated upper and lower bounds are multiplied with a confidence interval $c$ which is based on the amount of collected samples (number of days of historic data), i.e., with more historic data the confidence of the estimations is higher (Equation \ref{eq:vpa-estimate}).

The lower bound estimate $e_l$ means that any value below this bound is likely not enough for the application.
The upper bound estimate $e_u$ means that any value above this bound is likely to be wasteful.
These two estimates are used by the Updater to decide if a Pod should be evicted.
In this sense, these bounds act as a proxy for determining if it is worth adjusting the resources of a Pod, which avoids overly frequent changes and thereby thrashing \cite{VerticalPodAutoscalingDefinitive_2021}.

<!-- \begin{equation}\label{eq:vpa-estimate} -->
<!-- \begin{aligned} -->
<!-- margin &= 15\% \\ -->
<!-- lowerBound &= P_{50}(historicUsage) * margin \\ -->
<!-- targetValue &= P_{90}(historicUsage) * margin \\ -->
<!-- upperBound &= P_{95}(historicUsage) * margin \\ -->
<!-- confidence &= 1 + \frac{1}{history\ length\ in\ days} \\ -->
<!-- lowerEstimate &= lowerBound * confidence^{-2} \\ -->
<!-- upperEstimate &= upperBound * confidence -->
<!-- \end{aligned} -->
<!-- \end{equation} -->

\begin{equation}\label{eq:vpa-estimate}
\begin{aligned}
m &= 15\% \\
b_l &= P_{50}(H) * m \\
t &= P_{90}(H) * m \\
b_u &= P_{95}(H) * m \\
c &= 1 + \frac{1}{days(H)} \\
e_l &= b_l * c^{-2} \\
e_u &= b_u * c
\end{aligned}
\end{equation}

<!-- https://povilasv.me/vertical-pod-autoscaling-the-definitive-guide/ -->
<!-- https://github.com/kubernetes/autoscaler/tree/vpa-release-0.9/vertical-pod-autoscaler/pkg/recommender -->

### Cluster Autoscaler

The *Cluster Autoscaler*^[<https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler>] (CA) is the third component that enables scaling on Kubernetes.
Specifically, it adjusts the size of the cluster in terms of the number of worker nodes.
It can either add nodes when the compute resources in the current cluster are insufficient, or remove nodes when there are unutilized nodes.
This is achieved through integration with the APIs for provisioning and deprovisioning virtual machines of several public cloud platforms.

The CA adds new nodes to the cluster when there are Pods in *unschedulable* state, meaning the scheduler was unable to assign a node to the Pod due to insufficient resources.
This can happen for several reasons: a new application is deployed to the cluster, the HPA increases the number of replicas of a Deployment or the VPA increases the resource requests for a workload.
Conversely, the CA decreases the size of the cluster when some nodes are consistently unneeded for a significant amount of time, i.e., it has low utilization and all of its Pods can be scheduled on other nodes.
All of these decisions are made subject to several constraints the cluster administrator can set to prevent the CA from affecting the functionality of the cluster (e.g., eviction policy or Pod disruption budget).

While the interaction between the two Pod autoscalers (HPA and VPA) and the CA is crucial for successfully operating an elastic Kubernetes cluster, the CA is not relevant to the work carried out in this thesis.
Thus, a technical study of its internal mechanism is omitted here.

### Kubernetes Event-driven Autoscaler

<!-- It is built to be able to activate a Kubernetes deployment (i.e. no pods to a single pod) and subsequently to more pods based on events from various event sources. -->
<!-- It uses a CRD (custom resource definition) and the Kubernetes metric server so you will have to use a Kubernetes version which supports these. Any Kubernetes cluster >= 1.16.0 has been tested and should work. -->
<!-- KEDA is a Kubernetes-based Event Driven Autoscaler. With KEDA, you can drive the scaling of any container in Kubernetes based on the number of events needing to be processed. -->

<!-- KEDA is a single-purpose and lightweight component that can be added into any Kubernetes cluster. KEDA works alongside standard Kubernetes components like the Horizontal Pod Autoscaler and can extend functionality without overwriting or duplication. With KEDA you can explicitly map the apps you want to use event-driven scale, with other apps continuing to function. This makes KEDA a flexible and safe option to run alongside any number of any other Kubernetes applications or frameworks. -->
<!-- KEDA performs two key roles within Kubernetes: -->

<!--     Agent — KEDA activates and deactivates Kubernetes Deployments to scale to and from zero on no events. This is one of the primary roles of the keda-operator container that runs when you install KEDA. -->
<!--     Metrics — KEDA acts as a Kubernetes metrics server that exposes rich event data like queue length or stream lag to the Horizontal Pod Autoscaler to drive scale out. It is up to the Deployment to consume the events directly from the source. This preserves rich event integration and enables gestures like completing or abandoning queue messages to work out of the box. The metric serving is the primary role of the keda-operator-metrics-apiserver container that runs when you install KEDA. -->

While Kubernetes' HPA can easily be configured to use CPU and memory utilization, scaling based on custom metrics requires several other components:
metrics need to be exposed;
an external monitoring tool needs to be provisioned and configured;
metrics need to be fed into Kubernetes' Metrics API;
and finally, those metrics can be used for autoscaling with HPA (these steps are detailed in Chapter \ref{implementation}).

The *Kubernetes Event-driven Autoscaler* (KEDA) \cite{KEDADocumentationVersion_2021} addresses this complexity by taking a different approach:
instead of reactively scaling based on metrics from a monitoring system, it observes events as they are happening at the source.
KEDA supports a wide range of event sources (including message queues, databases, streaming services and monitoring solutions)^[<https://keda.sh/docs/2.2/scalers/>], which can be cluster-internal and cluster-external.
<!-- Internally, KEDA uses a very similar metrics pipeline to the one we have manually built in previous sections. -->
<!-- However, -->
With KEDA the end user only needs to configure a simple \mbox{\textit{ScaledObject}} CRD (discussed in Section \ref{horizontal-scaling-with-keda}).
<!-- , similar to the way VPA works (Section \ref{vertical-scaling-with-vpa}). -->

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/keda_architecture.pdf}
\caption{\label{fig:keda-architecture} Overview of KEDA autoscaling. KEDA components marked in blue.}
\end{figure}

Figure \ref{fig:keda-architecture} shows KEDA's two components: *agent* and *metrics API server* \cite{KEDADocumentationVersion_2021}.
The agent is responsible for scaling a Deployment between zero and one replicas.
This is a workaround for the limitation that HPA does not scale Deployments to less than one replica.
The metrics API server is responsible for listening to the event source and exposing new events through Kubernetes' metrics API
(this component serves the same purpose as the metrics adapter we discuss in Section \ref{prometheus-exporters}).

Figure \ref{fig:keda-architecture} shows the autoscaling operation of KEDA.
When idle (i.e., no incoming events) KEDA scales the Deployment to zero replicas.
As soon as events are detected, KEDA scales the Deployment to one replica.
Further scaling (depending on the number or rate of incoming events) is then carried out by the HPA, which consumes the metrics exposed by KEDA's metrics server \cite{KEDADocumentationVersion_2021}.

Unlike other event-driven and serverless frameworks (e.g., Knative^[<https://knative.dev/>]), KEDA is a single, lightweight component that focuses exclusively on autoscaling.
Because Deployments need to be explicitly marked as managed by KEDA, it is a flexible solution safe to run alongside existing Kubernetes applications without major modifications.

## Research proposals for Kubernetes Autoscalers

While autoscaling has been adequately considered in the literature, the following survey provides an overview and discussion of proposals for novel Kubernetes autoscalers.
<!-- As there is a lack of overview about Kubernetes autoscalers in the literature, we address this issue by presenting and discussing high-quality research proposals for Kubernetes pod autoscalers from relevant, peer-reviewed publications. -->
The survey considers only cluster-internal scaling mechanisms (i.e., vertical and horizontal scaling of Pods), external cluster scaling is outside the scope of this study (e.g., \cite{ExperimentalEvaluationKubernetesCluster_2020,DynamicallyAdjustingScaleKubernetes_2019}).
This choice was made because the nature of autoscaling decisions between these dimensions is quite different.
The same reasoning applies to scheduling algorithms.
While there have been interesting proposals for improved Kubernetes schedulers \cite{ClientSideSchedulingBasedApplication_2017,CaravelBurstTolerantScheduling_2019,ImprovingDataCenterEfficiency_2019}, scaling and scheduling are two fundamentally different operations.
Nevertheless, there are certainly advances to be made by having some amount of coordination between scaling and scheduling.

<!-- challenges of autoscalers: See Qu et al Auto-scaling web applications -->

\nop{}
{\renewcommand{\arraystretch}{1.5}
\begin{table}[h!]
    \centering
    \caption{Comparison of Kubernetes autoscaler proposals}
    \label{tab:comparison-k8s-autoscalers}
    % just scale the table down to fit (for now)
    % https://tex.stackexchange.com/a/121178
    % \resizebox{\textwidth}{!}{
    \begin{scriptsize}
    \begin{sf}
    \begin{tabular}{c|c|c|c|c|c|c}
      \textbf{Reference} & \textbf{Architecture} & \textbf{Technique} & \textbf{Approach} & \textbf{\bigcell{c}{Scaling \\ Timing}} & \textbf{Metric} & \textbf{\bigcell{c}{Workload \\ Pattern}} \\
      \hline \hline
 KHPA-A \cite{StudyPerformanceMeasuresAutoscaling_2019}           & single          & threshold-based     & reactive   & horizontal             & CPU               & unspecified \\
       \hline
 HPA+ \cite{AdaptiveAIbasedAutoscalingKubernetes_2020}            & single          & forecast-based      & proactive  & horizontal             & no. of timeouts   & \bigcell{c}{predictable \\ burst} \\
       \hline
 Libra \cite{AdaptiveScalingKubernetesPods_2020}                  & single          & threshold-based     & reactive   & \bigcell{c}{horizontal \& \\ vertical} & \bigcell{c}{CPU \& \\ throughput} & unspecified \\
       \hline
 Microscaler \cite{MicroscalerAutomaticScalingMicroservices_2019} & single          & threshold-based     & reactive   & horizontal             & response time     & unspecified \\
       \hline
 Q-Threshold \cite{EfficientCloudAutoScalingSLA_2018}             & single          & RL-based & reactive   & horizontal             & response time & \bigcell{c}{predictable \\ burst} \\
       \hline
 RUBAS \cite{ExploringPotentialNonDisruptiveVertical_2019}        & single          & threshold-based     & reactive   & vertical               &  \bigcell{c}{CPU \& \\ memory} & unspecified \\
       \hline
 me-kube \cite{HierarchicalScalingMicroservicesKubernetes_2020}   & multi           & threshold-based     & proactive  & horizontal             & SLA               & unspecified \\
       \hline
 Chang \cite{KubernetesBasedMonitoringPlatformDynamic_2017}       & single          & threshold-based     & reactive   & horizontal             & CPU               & unspecified \\

    \end{tabular}
    % } % end of resizebox
    \end{sf}
    \end{scriptsize}
\end{table}
% end of renewcommand
}

\clearpage

The nomenclature in Table \ref{tab:comparison-k8s-autoscalers} mostly follows the  taxonomy of \cite{AutoScalingWebApplicationsClouds_2018} and \cite{ReviewAutoscalingTechniquesElastic_2014}.
The *Architecture* refers to the logical application architecture the autoscaler focuses on.
By default, an autoscaler scales each service individually based on a set of criteria, in which case the column is labeled *single*.
For complex microservice architectures it can be beneficial to use a centralized scaling algorithm that coordinates the scaling of multiple services.
This case is labeled as *multi*.

The *Technique* column refers to the model used for determining the need for scaling and estimating the necessary resources.
*Threshold-based autoscaling* relies on simple heuristics to identify the need for scaling the application.
It is the most popular approach and offered out of the box by many cloud platforms.
The performance of this technique depends on the quality of the thresholds and how well the usage scenario can be modeled with static thresholds.
Instead of setting static thresholds, reinforcement learning methods can be used to dynamically learn the scaling threshold required to meet a target SLA (*RL-based*).

<!-- The CPU utilization of an application is commonly for this purpose: when the average utilization (across all instances of the application) rises above 80%, create new instances until the value stabilizes below 80%. -->
<!-- A similar lower bound is applied for scaling down. -->
<!-- Alternatively, high-level metrics (such as application response time) can be used for the same purpose. -->


*Forecast-based autoscaling* (also called *time series-based*) uses some form of machine learning to model the current workload and predict short-term future workloads.
The models can be learned either on-line (at runtime) or off-line (before runtime), though the latter is considered risky as it can have difficulty adjusting to unforeseen workload patterns.
The objective function of the machine learning algorithms commonly tries to match an SLA (e.g., application response time), while minimizing the amount of allocated compute resources.

<!-- * Forecast-based / Time-series based: short-term future demand modeling -->
<!--   Major drawback: uncertainty of prediction accuracy -->
<!--   "Energy-aware server provisioning and load dispatching for connection-intensive internet services" -->
<!--   "Empirical prediction models for adaptive resource provisioning in the cloud" -->
<!--   Huang: "Resource prediction based on double exponential smoothing in cloud computing" -->
<!--   Mi: "Online self-reconfiguration with performance guarantee for energy-efficient large-scale cloud computing data centers" -->
<!--   "Efficient autoscaling in the cloud using predictive models for workload forecasting" -->


The *Timing* column describes the nature of the algorithm.
If the autoscaler works purely based on the current demand, it is considered to be *reactive* because it only reacts to changes in the workload.
If the algorithm also partially predicts future demand and scales accordingly, it is considered *proactive*.

The *Scaling Method* refers to the dimension of scaling.
Horizontal scaling is characterized by adjusting the number of instances of the same type.
Vertical scaling is defined by adjusting the amount of resources allocated to a particular instance.
Ideally, both approaches should be combined \cite{AutopilotWorkloadAutoscalingGoogle_2020}.

The *Metric* column specifies based on which time-series values the autoscaler decides to scale and perform its resource estimation.

The *Workload Pattern* column indicates if the authors developed or tested their algorithm for a particular usage scenario.
*Predictable burst* refers to a cyclical usage pattern with a large difference between minimum and maximum utilization.
This is a common pattern for news and social media websites, which have large amounts of traffic during the day and very little at night.
In case of *unspecified*, the authors did not indicate a specific workload pattern.

<!-- "Application workload patterns can be categorized in three -->
<!-- representative patterns: (a) the Predictable Bursting pat- -->
<!-- tern indicates the type of workload that is subject to periodic -->
<!-- peaks and valleys typical for services with seasonality trends or -->
<!-- high performance computing, (b) the Variations pattern reflects -->
<!-- applications such as News&Media, event registration or rapid -->
<!-- fire sales, and (c) the ON&OFF pattern reflects applications -->
<!-- such as analytics, bank/tax agencies and test environments" -->


Unfortunately, none of the implementations in Table \ref{tab:comparison-k8s-autoscalers} have been made publicly available.
This is absolutely necessary to effectively and objectively benchmark different algorithms against each other.
Thus, the following focuses on a qualitative evaluation of the algorithms.

### KHPA-A
<!-- ### A study on performance measures for auto-scaling CPU-intensive containerized applications (2018) -->
<!-- KUBERNETES AUTOSCALER -->

Casalicchio \cite{StudyPerformanceMeasuresAutoscaling_2019} and Casalicchio and Perciballi \cite{AutoScalingContainersImpactRelative_2017} studied the relation between absolute and relative usage metrics for CPU-intensive workloads.
Relative usage measures refer to the utilization reported as a percentage of the allocated resource capacity.
These relative measures are exposed through the `cgroup` kernel primitives and are used by most container tools, such as Docker and cAdvisor.
They are commonly used because they provide an intuitive notion for horizontal scaling and defining usage quotas \cite{AutoScalingContainersImpactRelative_2017}.
For example, two containers with maximum CPU utilization running on the same host are reported to consume 50% CPU resources, while the host system is consuming 100% CPU resources.
However, if just one application is running and the host system is at 50% load, the response time from that application would be quite different.
Absolute usage measures refer to the actual, system-wide utilization of resources on the host system.
The authors studied this discrepancy and found that
the required capacity tends to be underestimated with relative usage metrics, which makes them unsuitable for determining the necessary resources needed to meet a service level objective \cite{StudyPerformanceMeasuresAutoscaling_2019}.

In particular, their findings revealed that there is a linear correlation between relative and absolute usage metrics.
Using this linear correlation allows transforming relative metrics (such as container quotas and limits) into absolute metrics.
Accordingly, the authors developed a scaling component similar to Kubernetes' HPA, but based this one on absolute metrics.
The correlation coefficients are the only additional input parameters to the algorithm.
This *KPHA-A* performed significantly better in single- and multi-container use cases with high load on the system (more than 80% total CPU load).
Specifically, for single instance workloads the application response time with standard HPA was 50% higher compared to using KHPA-A.
With concurrent workloads the response time was more than 3 times higher, depending on the particular workload.
The KHPA-A was able to achieve this by scaling the number of Pods about 10% higher than standard HPA \cite{StudyPerformanceMeasuresAutoscaling_2019}.
<!-- Unfortunately, the implementation of KHPA-A has not been made publicly available. -->

The authors argue that the results show that relative usage metrics cannot be reliably used if consistent QoS levels (e.g., application response time) are desired.
With their proposed algorithm, QoS metrics can be translated into CPU usage metrics.
It should be noted that the workloads in the study (`sysbench` and `stress-ng`) were highly artificial and solely CPU bound.
Real-world applications will certainly behave differently when utilizing CPU, memory, storage and network resources.
Furthermore, the linear coefficients required for the transformation from relative to absolute usage metrics are specific to the workload and execution environment.
Thus, they need to be constantly re-evaluated and it is unclear if this linear correlation also holds true in real-world scenarios with more diverse workloads.

<!-- "for CPU intensive workloads KHPA under-dimensions the number of -->
<!-- deployed Pods because the use of relative metrics. As -->
<!-- consequence, that will make impossible to setup an auto- -->
<!-- scaling policy capable to satisfy QoS requirements like -->
<!-- constraints on the responce time. On the contrary, with -->
<!-- KHPA-A, the scaling in/out actions are determined by the -->
<!-- absolute metrics (or better an estimation of the value -->
<!-- assumed by the absolute metrics). Absolute metrics, e.g., -->
<!-- the value of the CPU utilization of the virtual/phisical host, -->
<!-- have the advantage to measure the real usage of the host -->
<!-- system end hence can be used to compute QoS metrics like -->
<!-- the responce time. With KHPA-A a constraint on the -->
<!-- responce time can be translated into a constraint on the -->
<!-- CPU utilization," -->

### Libra
<!-- ### Adaptive Scaling of Kubernetes Pods (2020) -->
<!-- KUBERNETES AUTOSCALER -->

<!-- Is Libra publicly available? -->
<!-- Confusingly, there is an auto-scaler for Nomad with the same name: https://github.com/YotpoLtd/libra -->

<!-- Services made up of single-threaded applications can only increase their overall throughput by scaling horizontally. -->
<!-- These days however, many programming languages natively support multi-threading, which means each application can also increase its throughput by using more resources, i.e. vertical scaling. -->

<!-- Therefore, Balla et al.\ \cite{AdaptiveScalingKubernetesPods_2020} argue, solely horizontal auto-scaling is insufficient for enabling a service to be fully elastic, due to the difficulty of setting the correct minimum and maximum pod resource threshold. -->
<!-- As mentioned in the Kubernetes object introduction, pods can be assigned minimum and maximum amount of resources, such as relative CPU shares. -->
<!-- Assigning appropriate resource thresholds is a non-trivial task (even for simple services, as the required resources may depend on the input to the service) and can usually only be set after observing the service for a long time period. -->
<!-- Therefore, this is a task that should be taken care of by an auto-scaling component. -->

Balla et al.\ \cite{AdaptiveScalingKubernetesPods_2020} argue that solely horizontal autoscaling is insufficient for enabling a service to be fully elastic.
Therefore, they propose an autoscaler called *Libra* which performs both vertical scaling (determining the appropriate CPU limit) as well as horizontal scaling (determining the correct number of replicas).

The autoscaler starts out by determining the adequate CPU limit.
Libra deploys at least two Pods to avoid affecting the QoS too much while performing these measurements.
The *production* Pod is deployed with high CPU limits and serves 75% of the incoming traffic, while the *canary* Pod is used to find the appropriate CPU limit and serves the residual 25% of traffic.
The latter is assigned a low initial limit, which is then gradually increased by Libra, until the average number of served requests and the response time converge towards a stable value.

Afterwards, Libra updates the production Pod with the newly determined CPU limit and acts as a horizontal autoscaler.
In particular, it increases the number of Pods when the average response time is double the value determined in the previous phase or when the amount of served requests approaches 90% of the value associated to CPU limit.
For example, if the appropriate CPU limit has been determined to be 70% and it has been empirically measured that the Pod can serve 1,000 requests with that value, Libra starts adding another Pod when each of the running instances is serving more than 900 requests.
Conversely, if the requests per Pod fall below 40%, Libra removes Pod replicas \cite{AdaptiveScalingKubernetesPods_2020}.

The authors' experiments showed that Kubernetes' default HPA did not scale the Deployment enough, which led to 40% lower throughput (requests per second), while Libra scaled the Deployment to double the number of Pods.
Determining the appropriate resource requests and limits for a service by deploying different kinds of Pods is an excellent idea:
it enables accurate live measurements on real-world data without a large impact on clients using the service.
Unfortunately, the conducted benchmark was quite simplistic: a web server that simply returns the string "Hello" (and does not perform any other computations or I/O operations).
Thus, from these experiments it is unclear whether the same results could be obtained for more sophisticated applications and workload scenarios.
<!-- Also, in order to implement the request routing between production and canary pods the authors used an Istio service mesh with Envoy proxies as sidecars. -->
<!-- This is a pattern known to have performance issues due to the additional hops in routing HTTP requests and other overheads. \todo{citation} -->
<!-- Finally, it is great that the authors found an approach to automatically determine the adequate CPU resources thresholds. -->
<!-- A similar approach could be used for determining I/O and network resource thresholds. -->
<!-- When the application uses too much of any of these resources, it will simply get slowed down by the kernel. -->
<!-- Unfortunately, the approach does not work for memory values, since when the application uses too much memory, it will be terminated by the kernel (*OOM killer*) instead of degrading gracefully. -->

### RUBAS
<!-- ### Exploring Potential for Non-Disruptive Vertical Auto Scaling and Resource Estimation in Kubernetes (2019) -->
<!-- KUBERNETES AUTOSCALER -->

Rattihalli et al.\ \cite{ExploringPotentialNonDisruptiveVertical_2019} have designed a vertical autoscaler that aims to eliminate the dependency on resource thresholds set by human operators entirely.
The *Resource Utilization Based Autoscaling System* (*RUBAS*) is a vertical scaling component that can scale Pod resource limits non-disruptively.
Unlike Kubernetes' VPA, which terminates running Pods and creates new ones from scratch (see Section \ref{vertical-pod-autoscaler}), RUBAS uses *CRIU* (*Checkpoint Restore in Userspace*) to save the application state and restore it when launching a new Pod.
This is an excellent achievement that is particularly important for stateful workloads, i.e., those where recreating the service state from scratch would incur a significant performance penalty.
An example here would be a database, which needs to have complex and large data structures in memory to operate efficiently.

In contrast to VPA, which uses the peak resource consumption for estimating the new resource requirements, RUBAS bases the estimates on the median of past observations.
The authors argue that a temporary peak in resource consumption by the application should not be used for estimating future resource demands.

Their experiments showed that the non-disruptive migration can significantly reduce the execution time of applications (16% runtime improvement) when the initial resource thresholds specified by the user were too low.
In this case both VPA and RUBAS need to update the resource thresholds several times before converging on the optimal solution.
This result is particularly relevant for one-off batch jobs, not so much for long-running services.
They also found that due to resource allocation based on average utilization, RUBAS had to perform fewer migrations (stopping and restarting of Pods) compared to VPA.
Conversely, this also increased the CPU (72% compared to 82%) and memory utilization (76% compared to 86%).
Higher utilization means that tasks on the cluster will complete faster overall.

A major drawback of RUBAS is that it exists entirely outside of Kubernetes, instead of integrating itself as a control-plane component.
This violates the design philosophy of Kubernetes^[<https://kubernetes.io/docs/concepts/extend-kubernetes/>] and requires managing and provisioning additional infrastructure.
Moreover, RUBAS requires access to both the cluster-level management API (to control workloads in the Kubernetes cluster) as well as underlying infrastructure resources (direct access to worker nodes for creating snapshots; shared volumes for storing snapshots).
To this end, the presented architecture is unlikely to find adoption in the Kubernetes ecosystem.

### HPA+
<!-- ### Adaptive AI-based auto-scaling for Kubernetes (2020) -->
<!-- KUBERNETES AUTOSCALER -->

Toka et al.\ \cite{AdaptiveAIbasedAutoscalingKubernetes_2020} proposed a proactive autoscaling approach based on demand forecasting with machine learning.
Unlike most scaling algorithms which are reacting only based on the current load of the system (such as the default HPA), their autoscaling engine *HPA+* takes into account a larger window of time and future demand forecasts based on machine learning models.

<!-- Fundamentally, the authors assume a linear relation between the number of the number of pods and the served requests per second of an application. -->
<!-- Furthermore, they state that Kubernetes is most commonly used for web services, where this application profile holds true. -->
<!-- However, even simple web services usually include some form of database or disk access, which will inevitably break this linear relationship as the number of requests rises. -->

Under the hood, the HPA+ utilizes several models for scaling: an auto-regression (AR) model, a supervised Hierarchical Temporal Memory (HTM) neural network and an unsupervised Long Short-Term Memory (LSTM) neural network.
In \cite{MachineLearningbasedScalingManagement_2020} the same authors also added a fourth, reinforcement learning-based (RL) model.

<!-- After training and testing the models on their dataset, they found that the AR model produced the lowest root mean square error, the best predictive power ($R^2$) and had the fastest training time by several orders of magnitude. -->
<!-- However, the AR model is sensitive to outliers, unlike the LSTM model, which also required the most time for training by several orders of magnitude. -->
<!-- HTM provided a trade-off between robustness, computational requirements and predictive performance. -->
<!-- Additionally, this algorithm can be continuously trained based on the incoming data. -->
<!-- The RL model exhibited the worst prediction performance, despite long learning times. -->
Because each model performed poor or well depending on the particular usage pattern, the authors combined all models in a single autoscaling engine.
This engine continuously runs all models, but only the model with the best performance on the most recent input (last two minutes) is considered for scaling decisions.

While the underlying algorithms are quite complex, HPA+ packages them into a single parameter which the end user can tune: *excess* describes the trade-off between lower loss (amount of unserved client requests) and higher resource utilization.
This is achieved through resource over- and under-provisioning.
Compared to the original HPA, the HPA+ only scaled the Pods about 3-9% more (depending on the excess parameter), but had significantly lower request loss.
In their follow-up article \cite{MachineLearningbasedScalingManagement_2020}, the authors confirmed these findings with more extensive benchmarks.
They generated synthetic data based on real-world traces of Facebook.com website visits on a university campus.
While the use of recent real-world data for their tests is commendable, it should be noted that the artificially-generated traffic pattern was much more spiky (meaning large, positive outliers) than the original input traces, which can be considered as an artifact in the data.

With their autoscaling solution, the authors tried to focus on ease of usability by introducing a parameter which controls the trade-off between resource usage and QoS level violation.
However, since the proposed models need to be trained and fitted to the application at hand, setting up such a system is a non-trivial task.
In addition, it requires a sizable amount of clean data that closely mirrors the usage scenario of the application, because not every application follows the usage patterns of a social media website.


### Microscaler
<!-- ### Microscaler: Automatic Scaling for Microservices with an Online Learning Approach (2019) -->
<!-- KUBERNETES AUTOSCALER -->

Yu et al.\ \cite{MicroscalerAutomaticScalingMicroservices_2019} presented *Microscaler*, a horizontal autoscaler which combines an online learning approach with a heuristic approach for cost-optimal scaling of microservices while maintaining desired QoS levels.
The authors introduced a criterion called *service power* to determine the need for scaling and to estimate the appropriate scale.
Service power represents the ratio between the average latency of the slowest 50 percent of requests ($P_{50}$) and the average latency of the slowest 10 percent of requests ($P_{90}$) during the last 30 seconds.
When the service power is close to or above 1 (i.e., $P_{90} \approx P_{50}$), the application can handle most of the requests within the desired QoS level.
When the service power falls significantly below 1, it means that the service quality is degraded.

Microscaler mainly considers the QoS experienced by the user, instead of QoS of individual services.
As a consequence it scales not just because a service has high CPU utilization or increased response time, but only when a user-facing SLA is violated.
This way, it can avoid detecting false-positive scaling events.
In their case, response time SLAs can either be violated by falling below the minimum threshold $T_{min}$ or by rising above the maximum threshold $T_{max}$.
The service power criterion is then incorporated into a Bayesian Optimization approach, which allows minimizing an objective function (i.e., cost) while obeying constraints (i.e., SLA bounds).
<!-- With each lower or upper bound SLA violation, the cost model is updated. -->

Unfortunately, the results presented in their work are inconclusive:
while Microscaler converges slightly faster to the desired QoS level than other autoscaling approaches, the difference is not significant.
Also, the mathematical model carries complexity and several parameters which need to be adjusted depending on the application \cite{MicroscalerAutomaticScalingMicroservices_2019}.
<!-- For routing and generating metrics, the authors used a service-mesh architecture (Istio) inside Kubernetes. -->
<!-- This architecture provides allows scaling applications without having to modify the application source code in order to emit metrics such as response time. -->
<!-- However, routing all traffic through pod sidecars has significant performance overheads. -->
<!-- which partially undoes the cost-optimization in the first place -->

### Q-Threshold
<!-- ### Efficient Cloud Auto-Scaling with SLA objective using Q-Learning (2018) -->
<!-- KUBERNETES AUTOSCALER -->

Horovitz and Arian \cite{EfficientCloudAutoScalingSLA_2018} proposed an algorithm called *Q-Threshold* for dynamically adjusting horizontal scaling thresholds.
It is important to note that this solution is not a full autoscaler by itself:
instead it is a machine learning algorithm that only learns and suggests the ideal thresholds for scaling.
For example, "to not raise response time above 100ms, add more replicas when CPU utilization is above 78.5%".
This makes the operation more transparent to the cluster administrator and requires less trust, as the administrator remains in full control.

<!-- At its core is the reinforcement learning-based Q-Learning approach but it is enhanced with several optimizations for faster convergence. -->
As the name suggest, Q-Threshold leverages a Q-Learning algorithm and is enhanced with several optimizations for faster convergence.
Q-learning is a model-free reinforcement-learning algorithm that learns the optimal actions in state space through a reward function.
In the context of horizontal scaling the goal of Q-Learning is finding the optimal autoscaling policy while obeying a specified SLA.
Therefore, the reward function needs to trade-off SLA violations (in their case response time, which should be as low as possible) for resource utilization (which should be as high as possible).
When the SLA is violated, the reward is negative.

The authors have conducted extensive simulations with different variations of their algorithm and found satisfying results: Q-Threshold completely avoids SLA violations and has a stable behavior when scaling due to workload changes.
They also compared their algorithm against a static scaling threshold of CPU utilization (25% lower bound, 75% upper bound).
Unsurprisingly, the proposed algorithm performed better than a static threshold in this comparison.
Unfortunately, the authors neither gave a detailed description of the system implementation nor how the tests were conducted.
Therefore, it is ultimately unclear how well the Q-Threshold algorithm would work in a real-world scenario, despite promising simulation results.

### me-kube
<!-- ### Hierarchical Scaling of Microservices in Kubernetes (2020) -->
<!-- KUBERNETES AUTOSCALER -->

Rossi et al.\ \cite{HierarchicalScalingMicroservicesKubernetes_2020} presented *Multi-Level Elastic Kubernetes* (*me-kube*), an autoscaler that coordinates the horizontal scaling of microservice-based applications.
It is based on a two-layered control loop.
On the lower layer, the *Microservice Manager* controls the scaling of a single service by monitoring and analyzing metrics with a local policy.
This local policy can either be proactive (in this case reinforcement learning-based) or reactive (application metric-based).
On the higher layer, the *Application Manager* controls the scaling of an entire application (which can be composed of multiple services) by observing the application performance.
When the Microservice Manager detects a need to scale the service, it sends a *proposal* to the Application Manager.
A proposal contains the desired number of replicas for the service as well as a *score*:
it describes the estimated improvement of the proposed adaptation.

Conversely, when the Application Manager detects an SLA violation, it requests proposals from the Microservice Managers.
The Application Manager then coordinates the scaling of several microservices to avoid interfering scaling decisions (e.g., service B is scaled down because it is not receiving traffic from service A, which is under high-load).
To evaluate scaling decisions, the Application Manager considers all submitted reconfiguration proposals and chooses the one with the highest score, since it considers that one to have the highest impact on the overall application SLA.
This process is repeated iteratively until the target response time is met again or until there are no more proposals.
Once a decision has been made, the scaling action is communicated down to the Microservice Managers.

In their experiments the authors tested several scaling approaches.
They found that Q-Learning performed the worst in a moderately complex deployment scenario, which drastically increased the state space and thereby made it hard to learn for the model.
Kubernetes' HPA produced several policy violations, mainly because the workload was not necessarily CPU bound but HPA still scales based on CPU utilization by default.
The hierarchical (i.e., centrally coordinated) scaling approach combined with a local predictive policy (ARIMA) performed the best, since it only caused SLA violations at the very beginning of the stress test.

In summary, the central coordination of scaling decisions in microservice-based applications is a promising approach.
The authors have not documented how they deployed or configured the additional scaling components, therefore it is somewhat unclear which impact these aspects would have on a real-world deployment.
Moreover, an additional Microservice Manager is required for each microservice, which can have significant compute requirements, depending on the model and amount of data used for the scaling algorithm.

<!-- "We select ARIMA as it -->
<!-- is able to estimate the trend even from a few points. In -->
<!-- ARIMA, the future value of a variable is assumed to be a linear -->
<!-- function of several past observations and random errors [20]. -->
<!-- The AR-part of ARIMA indicates that the evolving variable of -->
<!-- interest is regressed on its own prior observations. The MA- -->
<!-- part indicates that the regression error is a linear combination -->
<!-- of the error terms that occurred in the past. The I-part indicates -->
<!-- that the data values have been replaced with the difference -->
<!-- between their values and the previous values. The purpose -->
<!-- of these features is to create a model that fits the data as -->
<!-- well as possible. Non-seasonal ARIMA models are generally -->
<!-- denoted ARIMA(p,d,q), where parameters p, d, and q are non- -->
<!-- negative integers: p is the order of the autoregressive model, d -->
<!-- is the degree of differencing, and q is the order of the moving- -->
<!-- average model. The ARIMA parameters should be determined -->
<!-- considering the workload characteristics." -->
<!-- "To forecast the input rate without relying on a a-priori training -->
<!-- set, each manager uses an ARIMA(0,1,1) model, so it is a -->
<!-- basic exponential smoothing model" -->


### Chang
<!-- ### A Kubernetes-Based Monitoring Platform for Dynamic Cloud Resource Provisioning (2017) -->
<!-- KUBERNETES AUTOSCALER -->

<!-- The authors point out that  Kubernetes' resource provisioning algorithm is fixed and cannot readily be exchanged for a custom implementation. -->

Chang et al.\ \cite{KubernetesBasedMonitoringPlatformDynamic_2017} proposed a generic platform for dynamic resource provisioning in Kubernetes with  three key features: comprehensive monitoring, deployment flexibility and automatic operation.
All three are essential for operating a large, distributed system on top of Kubernetes (or any other orchestration platform).

Their monitoring stack is based on Heapster (for collecting low-level system metrics), Apache JMeter (for generating application load and measuring response time), InfluxDB (time-series database for storing metrics) and Grafana (as a visualization tool).
It needs to be pointed out that the authors decided to inject artificial load into the system to measure application performance, instead of collecting "native" system metrics.
In this sense, they are measuring and collecting performance data at the client side, instead of at the server-side.
This approach has the advantage that it captures the service level experienced by clients more accurately \cite{UnderstandingDistributedSystems_2021}.
However, it also introduces additional stress on the system, which might be undesirable when the system is already under heavy load.

The authors described their *Resource Scheduler Module* and *Pod Scaler Module*, which adjust the number of running Pods based on CPU utilization with static thresholds.
The application (or the application developer) cannot directly set these thresholds, instead they need to be set by the cluster operator.
So while it is true that their setup eases "automatic operation", it does not allow any configuration of scaling parameters at runtime.
However, this is somewhat understandable as this was one of the first articles giving an extensive coverage of a custom autoscaling infrastructure in Kubernetes.


## Summary

The official autoscaling components for Kubernetes (HPA, VPA, CA) are widely and successfully used.
On one hand, their fundamental algorithms are simple (no complex mathematical equations or machine learning algorithms), giving cluster operators the ability to intuitively understand and trust them.
On the other hand, there are several parameters that can be tweaked for scaling, which gives operators the ability to adjust the scaling behavior to their application and use case.
Most importantly, these components come with a well-defined API (in form of Kubernetes objects) and have been *battle-tested* in production systems, meaning they are proven to be reliable and effective.

The considered research articles have explored several novel directions for Kubernetes autoscalers.
Most of the authors focused on horizontal scaling, as scaling in this dimension tends to be less error-prone compared to vertical scaling.
Additionally, horizontal scaling works well at small as well as large scales, whereas vertical scaling is better suited for larger systems (due to Kubernetes limitations discussed in \ref{vertical-pod-autoscaler}).
However, work from Google \cite{AutopilotWorkloadAutoscalingGoogle_2020} has shown that vertical autoscaling has two important benefits.
First, vertical autoscalers adjust resource reservation and limits much better to actual usage compared to static allocation by developers, which results in cost savings due to increased resource utilization.
Delimitrou and Kozyrakis \cite{QuasarResourceEfficientQoSAwareCluster_2014} found that 70% of the time developers request more resources than their application actually requires, while in 20% they under-request resources.
Second, these autoscalers alleviate the developer's burden of having to accurately set reservations and limits on their services.
This increases productivity, because humans are not only slow and inaccurate at estimating required resources, but they also need to continuously perform this task while the software is developed and updated.

Most of the solutions looked at scaling each service individually;
only one (\mbox{me-kube} \cite{HierarchicalScalingMicroservicesKubernetes_2020}) implemented centralized (*hierarchical*) scaling of several services.
While coordinated scaling can offer significant benefits, it is difficult to generalize this approach from simple test cases to large, complex meshes of production systems.
Bauer et al.\ \cite{ChamulteonCoordinatedAutoScalingMicroServices_2019} have also considered hierarchical scaling research, but they have not implemented and validated their proposal with Kubernetes.

From the research it seems clear that proactive autoscaling (i.e., scaling not only based on current load, but based on predicted future load) is beneficial for aggressive up- and down-scaling.
The major impediment here is the complexity of the algorithms.
More complex algorithms take more time to train and evaluate. Potentially they need to be fed with large amounts of data, too.
Additionally, cluster operators are no longer able to understand why the system is behaving in a certain way, which is highly undesirable.
Thus, a balance needs to be struck: simple predictive models such as ARIMA appear to work well for this \cite{HierarchicalScalingMicroservicesKubernetes_2020,EfficientAutoscalingCloudUsing_2011}.

No conclusion has been reached about whether a service should be scaled based on low-level (e.g., CPU, memory utilization) or high-level metrics (SLA).
While several of the previously presented articles found that high-level metrics work much better, Rzadca et al.\ \cite{AutopilotWorkloadAutoscalingGoogle_2020} advise against directly optimizing application metrics based on their experience at Google.
They argue that this job should be left to the service developers to allow them to reason about the performance of their service.
It also separates concerns between infrastructure and services.
Thus, the choice of scaling metrics depends on the development context and application usage scenario.

One common theme among all works reviewed above is the lack of details about implementation.
Not only the underlying algorithms are important when setting up a production-grade system, but also how the system is configured and operated.
Kubernetes provides excellent tools for managing resources inside a cluster.
Yet none of the proposed autoscalers have been integrated into Kubernetes the way HPA, VPA and KEDA are: once installed, they can be easily configured for each individual Deployment by attaching *Custom Resource Definitions* (CRDs).


## Modular Kubernetes Autoscaler

We believe there is a gap between industry and academia in the area of autoscaling research.
On the one hand, researchers want to test and evaluate their novel algorithms for autoscaling without having to worry about the integration with other Kubernetes components.
This can be a challenging task due to the high development velocity of the Kubernetes ecosystem.
On the other hand, industry practitioners value reliability and interoperability:
Kubernetes has been designed from the ground up with a strong focus on reuseable and well-defined APIs.
Thus, any new autoscaling component should use -- as well as expose -- these APIs.

We propose a modular autoscaling component for Kubernetes that combines these two goals: it provides a higher-level abstraction for the autoscaling algorithm by placing it inside a sandbox with simple interfaces.
This provides a separation of concerns: the authors of the algorithm can focus only on the scaling logic, while the autoscaling component takes cares of the interaction with Kubernetes and other external systems.
In the following, we outline its key design and architectural aspects.
The autoscaling component should be implemented using the Go programming language to align with other Kubernetes components and make use of the official Kubernetes client libraries^[<https://github.com/kubernetes/client-go>].
The architecture of the autoscaling component (shown in Figure \ref{fig:wasmpa-architecture}) is similar to HPA and VPA:
it fetches metrics from the Metrics API or an external monitoring system, and is configured with Kubernetes CRD objects.
The CRD object is the interface between the user and Kubernetes cluster since it contains the workload-specific scaling configuration.
Appendix \ref{modular-kubernetes-autoscaler-crd} shows a preliminary example of this CRD.

At runtime, the autoscaling component passes the metrics and scaling configuration to a WebAssembly sandbox, which runs the core autoscaling algorithm and returns scaling results.
Afterwards, the autoscaling component performs the actions described by the results of the algorithm (e.g., increase the amount of replicas to a certain number) by communicating with the Kubernetes API.

WebAssembly^[<https://webassembly.org/>] is a portable binary instruction format that can be used as a compilation target for many programming languages.
Its main features are speed, memory safety and debuggability \cite{DifferentialFuzzingWebAssembly_2020}.
Running the core scaling algorithm inside a WebAssembly sandbox has several advantages for researchers:

* the algorithm can be implemented in any programming language (Python, JavaScript, Rust etc.) and then compiled to WebAssembly bytecode;
* the code runs at near-native speed (faster than interpreted languages), allowing the implementation of complex and resource-intensive algorithms (e.g., neural networks);
* the sandbox provides simple interfaces for data input and output (i.e., no need to interact directly with the complex and evolving Kubernetes API), which simplifies development, testing and simulation.

Cluster operators gain the following advantages from the WebAssembly sandbox:

* memory safety avoids security bugs and strictly bounds the potential impact of errors (if the algorithm fails it might produce garbage output, but will not affect the stability of other system components);
* the WebAssembly bytecode can be replaced on the fly, thus allowing upgrading or changing the algorithm dynamically;
* the autoscaler can host multiple WebAssembly sandboxes, which allows different services to be scaled with individual algorithms;
* the same scaling algorithm can be used across different hosts and environments because the bytecode is agnostic in terms of processor architecture and operating system.

For similar reasons, the Kubewarden project^[<https://www.kubewarden.io/>] uses WebAssembly sandboxes to implement modular security policies in Kubernetes.

\begin{figure}[ht]
\centering
    \includegraphics[width=\textwidth]{images/wasmpa-architecture.pdf}
    \caption{\label{fig:wasmpa-architecture} High-level operational diagram of modular Kubernetes autoscaler with WebAssembly (WASM) sandbox}
\end{figure}

Additionally, the modularity of this autoscaler allows one important distinction from HPA and VPA:
it can combine the decision for horizontal and vertical scaling into one algorithm and one component.
HPA and VPA are separate, uncoordinated components which cannot be used to scale on the same metric
(otherwise race-conditions might occur and lead to unstable behavior^[<https://github.com/kubernetes/autoscaler/blob/master/vertical-pod-autoscaler/README.md#known-limitations>]).
The proposed modular autoscaling component does not have this issue since horizontal and vertical scaling decisions are generated from the same component -- and are therefore conflict-free.

The implementation of this modular Kubernetes autoscaler is left as future work.


<!-- by providing a Kubernetes components that modularizes the core scaling logic and abstracts away the infrastructure work an autoscaling component needs to take care of. -->

<!-- ### Horizontal Pod Autoscaling in Kubernetes for Elastic Container Orchestration (2020) -->
<!-- <\!-- KUBERNETES AUTOSCALER -\-> -->

<!-- <\!-- The authors of \cite{HorizontalPodAutoscalingKubernetes_2020} provide a fundamental analysis of Kubernetes HPA's operational behaviors based on experiments. -\-> -->
<!-- <\!-- Scaling tendency, metric collection, request processing, cluster size, latency -\-> -->

<!-- Ngyuen et al.\ \cite{HorizontalPodAutoscalingKubernetes_2020} analyzed the impact of HPA's *sync period* (the interval with which the control loop is executed) -->
<!-- They found that longer intervals cause higher number of failed requests (in their use case) because the required resources are added to slowly. -->
<!-- At the same time, longer intervals result in overall more efficient resource allocation, since pods are not first scaled up and then down again. -->
<!-- They also rightly point out that not only HPA's sync period affects the timing of scaling decisions, but also delay of the entire resource metric pipeline^[<https://kubernetes.io/docs/tasks/debug-application-cluster/resource-usage-monitoring/>] needs to be considered (metrics-server, cAdvisor et cetera), as each of those components has an individual update interval. -->

<!-- They also found that using external metrics (through Prometheus Adapter^[<https://github.com/kubernetes-sigs/prometheus-adapter>]) yielded more accurate results than using Kubernetes' internal CPU metrics directly. -->
<!-- They explained this with the fact that the metrics adapter, which transform Prometheus metrics into metrics suitable for HPA, can perform extrapolation to provide the metrics when raw data points are outdated. -->
<!-- If HPA queries the metrics adapter when in the middle of a scraping cycle, the Prometheus Adapter will calculate the requested metric based on the previously collected data points. -->
<!-- Overall, this allows HPA to react much faster to varying workloads. -->

<!-- They also found that using application-level metrics (HTTP request rate) performed better in terms of scaling accuracy and number of failed requests compared to using CPU utilization metrics. -->

<!-- ### Research on Resource Prediction Model Based on Kubernetes Container Auto-scaling Technology (2019) -->
<!-- <\!-- KUBERNETES AUTOSCALER -\-> -->

<!-- <\!-- not convinced by this paper -\-> -->

<!-- Zhao et al.\ \cite{ResearchResourcePredictionModel_2019} proposed an extension for Kubernetes' HPA that resolves the response delay problem during the scaling up phase. -->
<!-- The authors point out that there is non-negligible delay between the HPA deciding to increase the number of pods and those pods being available to serve load. -->
<!-- Due to this they suggest a prediction strategy for scaling, which not only takes the into account the current load, but also the estimated future load based on recent history. -->

<!-- The prediction model combines empirical mode decomposition (EMD) and auto regressive integrated moving average (ARIMA) models. -->
<!-- EMD is a method for analyzing and smoothing non-stationary signals. -->
<!-- ARIMA is an effective model for analyzing and predicting time-series data. -->
<!-- Their tests confirmed that this enhancement was able to scale up quicker compared to default HPA during peaks. -->
<!-- Unfortunately, the experimental setup was not described in-depth and the presented data is sparse. -->
<!-- Nevertheless, their work shows that due to the pod startup delay, it is beneficial to use a short-term predictive approach when scaling horizontally. -->

<!-- ### K8-Scalar: a workbench to compare cluster autoscalers (2018) -->

<!-- <\!-- not convinced by this paper -\-> -->

<!-- Delnat et al.\ \cite{K8scalarWorkbenchCompareAutoscalers_2018} proposed a workbench to compare autoscalers of container-based orchestration frameworks. -->
<!-- Their use case focused specifically on the autoscaling of database clusters. -->
<!-- Autoscaling for databases is particularly difficult due to their stateful nature and to avoid re-shuffling large amounts of data upon scaling. -->
<!-- Thus, all parts of the system (load balancing, vertical scaling, horizontal scaling) need to be fine-tuned to the database and workload at hand. -->
<!-- Some databases, such as Cassandra, are optimized for write-heavy workloads whereas other, such as MongoDB, perform better under read-heavy workloads. -->
<!-- The authors have made the source code and configuration for their framework publicly available^[<https://github.com/k8-scalar/k8-scalar>]. -->
<!-- Unfortunately, we could not identify any other works which actually use this framework for evaluating the performance of an autoscaler. -->

<!-- ### Key influencing factors of the Kubernetes autoscalers (2020) -->

<!-- <\!-- not convinced by this paper -\-> -->

<!-- Taherizadeh and Grobelnik \cite{KeyInfluencingFactorsKubernetes_2020} performed a general inquiry into the key factors which need to be considered when developing autoscaling methods for the cloud. -->
<!-- In particular, they looked at application performance and QoS under three different workload patterns: on-and-off, predictable and unpredictable bursting. -->

<!-- The identified three factors: the *conservative constant* $\alpha$, the *adaptation interval* CLTP (control loop time period) and the stopping of at most one container per CLTP (this is referred to as pod disruption budget in Kubernetes). -->

<!-- The conservative constant determines how large the threshold for triggering autoscaling actions is. -->
<!-- With a very small $\alpha$, a change in workload demand will quickly be picked up and acted upon by the autoscaler. -->
<!-- At the same time, this can cause undesired fluctuations / oscillations in the number of replicas, which can make the entire system unstable due to the overhead of provisioning and deprovisioning containers. -->
<!-- However, large values of $\alpha$ will increase the threshold of taking autoscaling actions and therefore possibly delay them, which can lead to longer time periods of over- or under-provisioned resources. -->

<!-- The adaptation intervals describes the minimum time between two successive autoscaling actions. -->
<!-- Much like the conservative constant, a short adaptation interval will make the system more response, but can also lead to system instability in dynamic environments with unpredictable workloads. -->
<!-- Therefore, a dynamic adaptation interval (i.e. one that is automatically adjusted based on workload) should be explored. -->

<!-- Their experiments with Kubernetes' HPA were inconclusive. -->
<!-- This demonstrates that each of these factors need to be evaluated on a case by case basis. -->
<!-- It also depends on whether the autoscaler should optimize for cost (minimize number of running instances) or QoS (response time etc.). -->
<!-- While these two goals are not strictly mutually exclusive, a trade-off has to be made nevertheless. -->

<!-- ### An Experimental Evaluation of the Kubernetes Cluster Autoscaler in the Cloud (2020) -->
<!-- CLUSTER AUTOSCALER -->

<!-- Tamiru et al.\ \cite{ExperimentalEvaluationKubernetesCluster_2020} evaluated the performance of the Kubernetes Cluster Autoscaler (CA) in terms of cost and application performance metrics. -->

<!-- CA supports two modes of operation: selecting nodes from a single, homogeneous node pool or selecting nodes from multiple, different node pools (referred to as *node auto-provisioning*, NAP). -->
<!-- The former is the default and is easier to set up, while the latter has the advantage that an appropriately sized node (in terms of compute and memory capacity) can be chosen. -->
<!-- Indeed, the authors were able to confirm this advantage with their experiments: the autoscaling performance (based on over-/under-provisioning and instability of elasticity) of NAP is better than the default CA, even though no significant cost saving were realized. -->

<!-- Their work also confirmed that Kubernetes' default algorithms are conservative in the sense that they tend to towards over-provisioning, but in turn provide better under-provisioning accuracy. -->
<!-- Like many other Kubernetes components, the CA has many configuration parameters and can offer outstanding performance when tuned properly to the workload. -->

<!-- ### SPEC Research: The Future of Cloud Metrics (2016) -->

<!-- The SPEC Research Group has worked out a detailed definition of scalability and elasticity. -->

<!-- > Scalability is the ability of a system to sustain increasing workloads with adequate performance provided that hardware resources are added. -->

<!-- > Elasticity is the degree to which a system is able to adapt to workload changes by provisioning and de-provisioning resources in an autonomic manner, such that at each point in time the available resources match the current demand as closely as possible. -->

<!-- When evaluating elasticity, the following aspects need to be considered: -->

<!-- * Autonomic Scaling: What adaptation process is used for autonomic scaling? -->
<!-- * Elasticity Dimensions: What is the set of resource types scaled as part of the adaptation process? -->
<!-- * Resource Scaling Units: For each resource type, in what unit is the amount of allocated resources varied? -->
<!-- * Scalability Bounds: For each resource type, what is the upper bound on the amount of resources that can be allocated? -->


<!-- Furthermore, the research group proposes several metrics for elasticity which allow capturing the accuracy and timing of scaling decisions. -->

<!-- * Under- and over-provisioning accuracy: measures average resource amount deviations -->
<!-- * Under- and over-provisioning timeshare: measures ratio of time spent in under- or over-provisioned state -->
<!-- * Jitter for amount of scaling decisions: measures difference in reactivity to demand and supply changes -->

<!-- They also go into a discussion of commonly used cloud SLAs. -->


<!-- ### Leveraging Kubernetes for adaptive and cost-efficient resource management (2019) -->

<!-- Verreydt et al.\ \cite{LeveragingKubernetesAdaptiveCostefficient_2019} explored the effects of different configurations and combinations of horizontal and vertical scaling in -->
<!-- Kubernetes with multiple applications and workloads. -->
<!-- They take into account the resource utilization of the nodes as well as the application performance, but only examine CPU intensive workloads. -->

<!-- They found that distributing CPU shares between different applications based on Kubernetes' resource requests is effective for bursty as well as predictable workloads. -->
<!-- Therefore it allows increasing cost-efficiency, as low-priority applications (with low resource requests) can over-subscribe when there is idle capacity in the cluster, while high-priority applications (with higher resource requests) still get the necessary resources during high load. -->

<!-- Their experiments also showed that the settings of Kubernetes' HPA need to be adjusted according to the expected workload. -->
<!-- In particular, they performed a burst test every 5 minutes and found that HPA did not scale down the number of replicas between bursts. -->
<!-- This is due to the HPA's *downscale stabilization* parameter, which by default is set to 5 minutes and determines how long HPA should wait for downscaling after detecting the necessity to do so. -->
<!-- Therefore, the appropriate value for this parameter entirely depends on the definition of burst in the application context. -->
<!-- In some contexts  a burst might last several hours (e.g., online retail in the evening), whereas in other contexts a burst might be just a few minutes. -->

<!-- ### A comparison of Reinforcement Learning Techniques for Fuzzy Cloud Auto-Scaling (2017) -->

<!-- The work of Arabnejad et al.\ \cite{ComparisonReinforcementLearningTechniques_2017} focused on horizontally auto-scaling VMs in the OpenStack cloud, but the same concept could also be applied to containers and pods. -->
<!-- They used two reinforcement learning approaches, namely fuzzy SARSA learning (FSL) and fuzzy fuzzy Q-learning (FQL) for dynamically adjusting the number of provisioned VMs while optimizing SLA compliance in the form of response time. -->
<!-- The authors use fuzzy logic in order to be able to encode expert human knowledge, in this case knowledge regarding application metrics and SLAs, into the model. -->
<!-- However, this presupposes that such an expert knowledge is available in the first place. -->
<!-- In terms of matching the desired application response time, FSL was superior in predictable workload patterns, whereas FQL performed better with variable workload patterns. -->
<!-- While their results show that FQL and FSL can be used to perform effective auto-scaling of cloud applications, it is unclear whether these approaches are actually better than the standard threshold-based approaches. -->
<!-- Additionally, the mathematical models for fuzzy-logic and reinforcement learning come with significant complexity. -->

<!-- <\!-- "The purpose of the fuzzy logic system is to model a human -\-> -->
<!-- <\!-- knowledge. Fuzzy logic allows us to convert expert knowledge -\-> -->
<!-- <\!-- in the form of rules, apply it in the given situation and -\-> -->
<!-- <\!-- conclude a suitable and optimal action according to the expert -\-> -->
<!-- <\!-- knowledge. Fuzzy rules are collections of IF-THEN rules -\-> -->
<!-- <\!-- that represent human knowledge on how to take decisions -\-> -->
<!-- <\!-- and control a target system" -\-> -->

<!-- ### Autopilot: workload autoscaling at Google (2020) -->

<!-- Researchers and engineers at Google have developed the powerful service *Autopilot* service for horizontally and vertically scaling workloads running on the Borg job scheduler, which runs Google's internal cloud \cite{AutopilotWorkloadAutoscalingGoogle_2020}. -->
<!-- It does both vertical scaling for CPU and memory limits as well as horizontal scaling. -->
<!-- In their paper the authors extensively describe how the memory scaling in relation with out-of-memory errors is implemented. -->

<!-- Their measurements showed that manually configured jobs do not utilize 46% of allocated resources, whereas jobs configured through Autopilot just leave 23% underutilized. -->
<!-- At Google's scale, these are significant savings. -->
<!-- At the same time, those automatically configured jobs have significantly lower out-of-memory error rates (i.e. increased reliability) while requiring no external configuration for most services. -->
<!-- The vertical autoscaler achieves this by using several moving average functions as well as an ensemble of simple machine learning models. -->

<!-- One interesting design choice in Autopilot is the fact that it does not try to optimize the performance of services (e.g., time to completion or response time). -->
<!-- This concern is shifted to the service developer, who can look at the resource limits set by Autopilot and act accordingly. -->

<!-- Another special design constraint is the environment Autopilot operates in: it can leverage the huge scale at which Google's internal cloud operates. -->
<!-- There are two advantages: 1) all services are designed from the beginning to absorb failures (such as the ones caused by OOM errors) and 2) there are always sufficiently many instances of a service running at the same time that individual failures (for example due to insufficient memory) can be tolerated. -->
<!-- Subsequently, Autopilot can optimize the memory configuration of the service for the average case instead of the worst case. -->

<!-- Many infrastructure and service operators do not have this freedom of being able to tolerate service instance failures. -->
<!-- In turn, they are paying with higher resource usage due to over-provisioning, as they need to configure the resource limits for the worst case (maximum case). -->

<!-- ### Dynamically Adjusting Scale of a Kubernetes Cluster Under QoS Guarantee (2019) -->
<!-- CLUSTER AUTOSCALER -->

<!-- Wu et al.\ \cite{DynamicallyAdjustingScaleKubernetes_2019} propose a generic system for dynamically adjusting the scale of Kubernetes cluster (in order to improve resource utilization) while guaranteeing QoS, specifically response time. -->
<!-- Their system comprises four modules: -->

<!-- * The *monitor module* is responsible for cluster-level metric collection. -->
<!--   It uses Heapster to monitor the status of cluster and InfluxDB to store the monitoring data. -->
<!-- * The *QoS module* deploys an additional pod alongside the regular application pods. -->
<!--   This pod is used to perform stress tests, i.e. artificial load on the CPU, of varying intensity. -->
<!--   Meanwhile, the application response time is measured. -->
<!--   Thereby a (possible non-linear) relationship between the per-node CPU utilization and application response time can be established. -->
<!--   Given an upper bound for the response time (application specific), the maximum permissible CPU utilization can be calculated. -->
<!-- * The *scaling module* determines the ideal number of nodes in the cluster based on the CPU utilization calculated by the QoS module and the current CPU utilization in the cluster collected from the monitor module. -->
<!--   This step assumes a even distribution of work across all nodes in the cluster. -->
<!-- * The *executing module* receives the ideal number of nodes as input and adjusts the scale of the cluster accordingly by adding or removing nodes with kubectl and kubeadm^[kubeadm: tool for provisioning Kubernetes nodes, <https://github.com/kubernetes/kubeadm>] -->

<!-- In their experiments a Kubernetes cluster with a fixed number of nodes had an average CPU utilization of around 50%, whereas the proposed system increased the CPU utilization by 27%-30%, depending on the workload. -->
<!-- This is quite significant. -->
<!-- While the authors tested their system with Kubernetes v1.10, unfortunately they did not compare it to Kubernetes' Cluster Autoscaler^[Cluster Autoscaler: <https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler>], which fulfills the same purpose and is available since Kubernetes v1.8. -->



<!-- ------------------------------------ -->

<!-- "CPU usage can be approximated by the ratio of number of -->
<!-- served and of the maximum number of requests that can be -->
<!-- served" -->


<!-- Auto-scaling approaches: -->

<!-- * Control theory-based -->
<!--   Using control theory, the behavior of a system is adjusted by comparing the output values to reference values: the aim of the controller is minimizing the difference between the actual output and desired output. -->
<!--   In the context of auto-scaling, the reference value could a targeted SLA, such as response time or CPU load. -->

<!--   Zhu: "Resource provisioning with budget constraints for adaptive applications in cloud environments" -->
<!--   Ali-Eldin: "Efficient provisioning of bursty scientific workloads on the cloud using adaptive elasticity control"; "An adaptive hybrid elasticity controller for cloud infrastructures" -->
<!--   Kalyvianaki: "Self-adaptive and self-configured CPU resource provisioning for virtualized servers using Kalman filters" -->
<!--   Baresi: "A discrete-time feedback controller for containerized cloud applications" -->


<!-- * Performance prediction-based: -->
<!--   Yu: "Microscaler: Automatic scaling for microservices with an online learning approach" -->
<!--   Wajahat "Using machine learning for black-box autoscaling" -->
<!--   Rahman: "Predicting the end-to-end tail latency of containerized microservices in the cloud" -->
<!--   Rzadca: "Autopilot: workload autoscaling at Google" -->



<!-- 14 Jamshidi: Autonomic resource provisioning for cloud-based software -->
<!-- "Fuzzy control systems make it easier to incorporate human knowledge in the decision making process in the form of fuzzy rules, but also reduce state space (?)" -->
<!-- https://en.wikipedia.org/wiki/Fuzzy_control_system -->


<!-- Other things that need to be considered when scaling on Kubernetes: -->

<!-- Load balancers: -->
<!-- * Takahashi: "A portable load balancer for Kubernetes cluster" -->
<!-- * NGINX Ingress: https://github.com/nginxinc/kubernetes-ingress -->


<!-- Isolating resources: -->
<!-- * Xu: "NBWGuard: Realizing Network QoS for Kubernetes" -->

<!-- Replica Consistency: -->
<!-- * Netto: "Koordinator: A Service Approach for Replicating Docker Containers in Kubernetes" -->
