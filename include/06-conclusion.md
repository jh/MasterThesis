# Summary and Future Work {#conclusion}

\chapterquote{While developing these systems we have learned \\
almost as many things not to do as ideas that are worth doing.}{Burns et al. \cite{BorgOmegaKubernetes_2016}}{11.2cm}
\noindent
This thesis tackled the questions of how to effectively dimension cloud-native applications and how to assess their performance.
While modern cloud platforms allow developers to allocate arbitrary amounts of resources, operating a production-grade service on Kubernetes requires deep insights into the performance behavior of the application \cite{ReadyRainViewSPEC_2016}.
Once this question had been answered, we moved on to the challenge of scaling our application based on the current workload, i.e., *autoscaling*.

<!-- In Chapter \ref{background} we explored the background of Kubernetes and the concepts it is based on: cloud computing, containers and microservices. -->
<!-- We found that there is a significant correlation between the rise in popularity of these technologies, and that Kubernetes is the logical conclusion of this development. -->
<!-- Additionally, the chapter also gave a brief introduction to the Kubernetes concepts relevant for scaling. -->

We gave an overview of the available literature on the subject of autoscaling applications in the cloud.
This revealed that while there have been numerous articles and surveys about VM- and container-based autoscaling, only recently have researchers started investigating specifically Kubernetes.
A comprehensive review of the algorithms and technical architectures of publicly available autoscaling components for Kubernetes (HPA, VPA, CA, KEDA) was performed to understand the technologies currently used in the industry.
Finally, a survey of research proposals for novel Kubernetes autoscalers was conducted and the proposals were evaluated qualitatively.
This research made it clear that proactive autoscaling (i.e., scaling not only based on current load, but based on predicted future load) is beneficial for aggressive scaling.
However, this leads to more complex algorithms (which require more time to train and potentially large amounts of data) as well as system behavior that is more opaque to cluster operators.
Thus, these two aspects need to be balanced.

The literature is inconclusive about whether a service should be scaled based on low-level (e.g., CPU and memory utilization) or high-level metrics (such as response time).
<!-- While several articles found that high-level metrics work much better, Rzadca et al.\ \cite{AutopilotWorkloadAutoscalingGoogle_2020} advise against directly optimizing application metrics based on their experience at Google. -->
Ultimately, the choice of scaling metrics depends on the development context and application usage scenario.
For this purpose, our work outlined the necessary steps to expose and identify metrics relevant for scaling an application running on Kubernetes.

Unfortunately, none of the reviewed proposals have a publicly available implementation.
This is problematic because it prevents evaluating the technical soundness of the implementation and its integration with Kubernetes.
In the end, not only the underlying algorithms are important when setting up a production-grade system, but also how the operators need to configure and interact with it.

For this reason, we proposed the design and architecture of a novel Kubernetes autoscaler:
its main characteristic is modularity by using a WebAssembly sandbox for running the core scaling algorithm.
The modular autoscaler gives cluster operators safety and reliability guarantees.
At the same time, it offers flexibility and ease-of-use to researchers looking to implement and test their scaling algorithms.
The implementation of this Kubernetes autoscaling component <!-- as a *Kubernetes Operator*^[<https://kubernetes.io/docs/concepts/extend-kubernetes/operator/>] --> is left as future work.

\clearpage

We then presented the necessary monitoring infrastructure and autoscaling policies for an application in a production-grade environment.
We believe that this is highly relevant for industry practitioners looking to get started with monitoring an application running on Kubernetes.
Prometheus was chosen as a monitoring tool as it is the industry-standard for metrics-based monitoring in the cloud.
Grafana was used as a visualization layer to get an intuition for the behavior and correlation of metrics from different system components.
The discussion then provided a reference point for which kinds of metrics should be collected by monitoring system to allow the operators to have a complete picture of the application behavior:
low-level metrics (e.g., CPU and memory usage), high-level metrics (e.g., response time), platform-level metrics (e.g., Kubernetes Pods), service-level metrics (e.g., message queue status) and application-level metrics (e.g., number of users).
Additionally, we described how to identify metrics relevant for scaling and how to configure Kubernetes autoscaling components (HPA, VPA, KEDA) based on these metrics.
While the implementation we have shown is specific to the target application, the principles and methodologies can be applied to any cloud-native application.
Since we provided detailed documentation about our setup, industry professionals and researchers are able to replicate similar setups in their own environments.

Finally, we performed a quantitative evaluation of several autoscaling policies.
Our findings showed that the target application is able to achieve maximum performance with the autoscaling policies, while having only minor variances in performance.
At the same time, we achieved significant cost-savings due to downscaling during times of low load.
Despite the benchmark results being specific to our target application, other researchers and professionals can reuse the same benchmarking procedures for any queue-based cloud application.
Furthermore, the discussed scaling optimizations (delayed scale-down, overscaling etc.) are applicable to any system leveraging autoscaling.
In particular, the criteria for evaluating the performance (*time to completion*) and cost (*replica seconds*) dimensions are valuable for anyone carrying out performance-and-cost optimizations with container-based infrastructure.

Concerning future work, we think it would be valuable to compare the current implementation of the target application with an event-driven implementation.
Event-driven architectures are at the core of popular *serverless* or *functions-as-a-service* offerings of public cloud providers (e.g., AWS Lambda, GCP Cloud Run, Azure Functions).
In this architecture there are no constantly running workers that are listening.
Instead, for each *event* (e.g., a task in a message queue) a new instance of the worker is created and once the worker finishes processing the task, the worker is terminated.
This frequent creation and termination of workers is made possible by running stateless services with extremely lightweight isolation.
Subsequently, such an architecture has the potential for even more elasticity.
Mohanty et al.\ \cite{EvaluationOpenSourceServerless_2018} published a survey about open-source event-driven platforms, which can be used as a basis for this future work.
More recently, the SPEC research group conducted a general review of use cases for serverless architectures \cite{ReviewServerlessUseCases_2020}.
<!-- For this comparison, the same performance and cost criteria should be used. -->

Overall, this thesis provided foundational and relevant knowledge on the topic of autoscaling for researchers and industry practitioners alike.
While not all software architectures and deployment models were discussed in our work (in particular stateful applications), the reader should have gained insights into tackling the challenging tasks of dimensioning, optimizing and scaling their cloud-native applications.
The key takeaway is that a solid foundation of metrics (collected from several components) allows effectively dimensioning and scaling any application with state-of-the-art cloud-native solutions.
