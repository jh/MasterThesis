# Background

\chapterquote{Cloud-native is a term describing software designed to run and scale reliably \\ and predictably on top of potentially unreliable cloud-based infrastructure.}{Duncan Winn}{13.4cm}
\noindent
This chapter examines the history of the trends towards cloud computing and microservices and highlights the connection to Kubernetes.
Afterwards, essential Kubernetes concepts are presented, as they are required for understanding the effects of the changes and configuration settings in the following chapters.

## Cloud Computing and Containers

Cloud computing offers users instant access to a wide variety of processing and storage services, all of which can be accessed over the Internet.
One of the most prominent features of "the cloud" is full elasticity of computing resources \cite{NISTDefinitionCloudComputing_2011}.
In fact, cloud resources can be dynamically requested and adjusted.
Unlike buying a fixed amount of physical servers, only the requested resources need to be paid for.

Originally, the term elasticity has been used in physics to refer to the property of material that is capable of returning to its original state after deformation.
In economics, elasticity describes the effect that changing one variable has on another variable.
More recently, the concept of elasticity has been applied to the context of cloud computing.
However, unlike in physics and economics, elasticity in cloud computing does not convey a quantitative meaning and is rarely rigidly defined.
More often, it is simply used as buzzword on the frontpage of public cloud providers' websites.
The SPEC Research Group has produced a detailed definition of elasticity in the context of cloud computing: it is the ability to scale up and down the number of resources allocated for an application \cite{ReadyRainViewSPEC_2016}.

Cloud computing is the latest incarnation of a continuous trend to make computing resources more flexible, configurable and efficient.
The introduction of multi-tasking (*time-sharing*) operating systems allowed multiple users to simultaneously access the same physical hardware.
This trend is also reflected in the shift from specialized servers (*mainframes*) to using commodity hardware at a large scale.
Virtualization even abstracts away this commodity hardware into a *computing substrate*: an abstract platform for performing computations.
Finally, containers further reduce dependency on the underlying operating system and execution environment.
System administrators no longer need to decide on which specific machine an application runs or worry if enough resources are available.

The "modern" *container* is an implementation specific to the Linux kernel, but other operating systems have similar concepts.
The idea of restricting the access of a particular process goes back to 1982, when *chroot* environments in Unix allowed restricting the accessible filesystem for a particular process to a specific directory.
In 2000, FreeBSD introduced *Jails*^[<https://docs.freebsd.org/en/books/handbook/jails/>], which builds on the chroot concept and provides additional isolation and security guarantees (e.g., separate namespaces for process IDs).
In 2002, Oracle introduced *Zones*, also known as *Solaris Containers*, as a first-class concept in the Solaris operating system \cite{BorgOmegaKubernetes_2016}.
In 2005, *OpenVZ* was the first operating-system-level virtualization for Linux, but required a modified Linux kernel.
By 2008, the Linux kernel natively supported enough features to host *Linux Containers* (*LXC*).
Containers allow limiting the resources (CPU, memory, filesystem, network etc.) available to a process -- or set of processes -- through a Linux kernel feature called *cgroups* (control groups).
Like regular processes in an operating system, containers share the same kernel with all other processes on the host.
Unlike regular processes, each container only sees and has access to its own, separate environment, which is achieved through namespace isolation.
By combining both resource restriction and namespace isolation containers implement *operating-system-level virtualization*.
In contrast to full virtualization, where multiple kernels are running on the same host, containers have a lower resource footprint (CPU, memory and storage utilization), thereby enabling higher application performance \cite{UpdatedPerformanceComparisonVirtual_2015}.
Subsequently, this allows a higher density of applications per host and more efficient resource usage by colocating different types of applications \cite{BorgOmegaKubernetes_2016}.
Since the size of container images tends to be an order of magnitude smaller compared to virtual machine disk images \cite{HypervisorsVsLightweightVirtualization_2015}, they can easily and efficiently be shared online through container image registries.
Finally, containers can be started within seconds, as opposed virtual machines which can take minutes to initialize.
This allows frequently adding and removing  container instances without much overhead, thereby improving elasticity \cite{QuantifyingCloudElasticityContainerbased_2019}.

The Docker project introduced a tool that can manage the entire life cycle of a container: building an image from a set of instructions (*Dockerfile*); sharing this container image over the internet (*DockerHub*); as well as creating, running and deleting containers based on images.
This is what is commonly understood when referring to the modern container \cite{BorgOmegaKubernetes_2016}.
All these features mean that containers can not only be used to run applications and their components, but also to package them up in a convenient format alongside their configuration.
Thus, containers provide a higher level of abstraction for the application lifecycle, including not only starting and stopping, but facilitating also upgrades and replication in a seamless way \cite{AutoScalingContainersImpactRelative_2017}.

Since Docker's introduction in 2013, the *containerization* of applications has seen widespread adoption.
The monitoring company *Datadog* found in their 2018 report that 23.4% of their customers had adopted Docker, <!-- \cite{SurprisingFactsRealDocker_2018}. -->
<!-- As Datadog's 2018 report shows, containers have seen rapid adoption within the software industry, -->
both at small (single developers and small startups) and large scales (enterprise software development) \cite{SurprisingFactsRealDocker_2018}.

## Microservices

Coincidentally, containers provide a flexible abstraction for composing a collection of microservices, which is a software architecture that has increased in popularity over the last ten years \cite{BuildingMicroservicesDesigningFinegrained_2021}.
With the microservice architecture, a single application is decoupled into multiple, distributed services.
Each service follows the *Single Responsibility Principle* by providing independent functionality and communicates with other services via language-agnostic *Application Programming Interfaces* (APIs).
The main advantage of microservices is organizational: each service can be developed and operated by a different development team, and therefore each team can make independent organizational decisions (such as software releases) as well as technological decisions (programming languages, frameworks etc.) \cite{KeyInfluencingFactorsKubernetes_2020}.
As a result of the shift towards microservices, the backend architecture of many applications has seen an increase in complexity as well.
Already in 2001, IBM has pointed out that the main obstacle in the IT industry is the growing software complexity \cite{VisionAutonomicComputing_2003}.

When an application is made up of many different microservices, these services should be distributed across multiple machines for two reasons: fault tolerance and performance.
A greater degree of fault tolerance enables high availability.
If all services of an application are running on the same machine and that machine encounters a hardware fault (such as power loss), the entire application will be offline (Figure \ref{fig:local-microservices}).
In the case of loosely coupled microservices distributed across multiple machines, a failure of a single machine only partially affects the availability of the application (Figure \ref{fig:distributed-microservices}).
At the same time, distributing the individual services across multiple machines increases the performance of the overall application as it is no longer constrained by the available CPU, memory or storage resources of an individual machine.
Additionally, each service can be scaled with fine granularity, which reduces the cost compared to the conventional replication of the entire application.
However, such an architecture comes at the cost of increased software complexity, larger amounts of network traffic and susceptibility to network outages.

\begin{figure}[ht]
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=140pt]{images/monolith.pdf}
    \caption{}
    \label{fig:monolith}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=140pt]{images/local-microservices.pdf}
    \caption{}
    \label{fig:local-microservices}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[height=140pt]{images/distributed-microservices.pdf}
    \caption{}
    \label{fig:distributed-microservices}
  \end{subfigure}

  \caption{Comparison of monolithic, local and distributed microservice architectures}
  \label{fig:monolith_vs_microservices}
\end{figure}

Additionally, there is another major challenge: increased operational complexity.
While all the services may be simple to install and run with container tools such as Docker, system administrators need to operate many of these applications across a large number of machines (dozens, hundreds or even thousands).
These and related tasks are commonly referred to as *orchestration*.
More specifically, orchestration is the management of (virtual) infrastructure required by an application during its entire lifecycle: deploying, provisioning, running, adjusting, terminating.
This is part of the vision of *autonomic computing* introduced in 2001 \cite{VisionAutonomicComputing_2003}: software systems that can manage themselves.

This is exactly where Kubernetes comes in: its main goal is making the orchestration of complex distributed systems easy while leveraging the density improvements offered by containers \cite{BorgOmegaKubernetes_2016}.

## Kubernetes

<!-- Kubernetes is at the heart of the Cloud-native Computing Landscape\footnote{\url{https://landscape.cncf.io/}}. -->

Kubernetes is an open-source framework for automating the deployment, scaling and management of distributed applications in the context of a cluster.
A *cluster* is a set of worker nodes which are orchestrated by one control plane and appear as a single unit to the outside.
Kubernetes' initial design was based on Google's internal *Borg* and *Omega* systems, both cluster management systems that the company uses to schedule workloads across machines in its datacenters \cite{BorgOmegaKubernetes_2016}.
Kubernetes was introduced in 2015 and the name is commonly abbreviated as *K8s*.
Since then, it has become a widely used platform for deploying distributed applications.
This is made apparent <!-- evident --> by the fact that all major public cloud platforms offer a managed Kubernetes service (AWS EKS, GCP GKE, Azure AKS, IBM Cloud Kubernetes, AlibabaCloud ACK).
Unless otherwise noted, all statements in this thesis regarding Kubernetes refer to version 1.20 (released in December 2020).

Kubernetes itself is implemented as an application with a microservice architecture.
This means the individual parts -- which are referred to as *components* -- are loosely coupled and have distinct functional responsibilities.
Each component provides a set of services to the other components through well-defined APIs^[<https://github.com/kubernetes/community/blob/master/contributors/devel/sig-architecture/api-conventions.md>].
This allows the Kubernetes architecture to be open and extensible, which is one of the explicit development goals.

Using a microservice architecture for Kubernetes makes sense for two reasons: the software managing other applications needs to be highly available (fault tolerancy); and the software is developed in a distributed fashion by many *special interest groups*^[<https://sigs.k8s.io>] (SIGs).
<!-- , which are overseen by the *Steering Committee*\footnote{\url{https://github.com/kubernetes/steering}}. -->
Furthermore, it enables anyone to enhance or replace every single one of the components individually without having to modify the rest of the system.
Finally, many of the components are optional and thus do not necessarily need to be used in every environment.
One example of the extensibility is the Crossplane project^[<https://crossplane.io/>], which exposes resources outside of a Kubernetes cluster (such as databases or virtual machines) through the Kubernetes API.
The result of this extensibility is that Kubernetes itself is a complex mesh of microservices.
It is absolutely necessary to have a firm understanding of its components to be able to effectively operate and optimize it.

Kubernetes refers to individual machines as *nodes* and to a set of nodes controlled by the same Kubernetes instance as a *cluster*.
To the user, Kubernetes presents a declarative interface for describing the state of *objects* in the cluster.
The most common *core objects* (supported by default) are Pods and Services.
As described before, the list of objects is extensible through Kubernetes' microservice architecture.
*Declarative* means that Kubernetes continuously tries to the converge the current state of the objects towards the desired state, which is defined by a *specification* (or *Spec*) in Kubernetes.
Practically speaking, when the user specifies that 5 instances of a web server should be running, Kubernetes creates 5 instances and monitors that they are available.
When one of them is no longer available, for example due to a software bug or hardware failure, Kubernetes automatically creates a new instance.

### Kubernetes Objects

In Kubernetes, a *Pod* -- not a single container -- is the smallest deployment unit \cite{KubernetesDocumentationWorkloads_2021}.
A Pod can comprise one or more containers.
It has an associated configuration (the *PodSpec*) that determines how exactly the container(s) are run, including attached compute, network and storage resources.
All containers within the same Pod share these resources.
Furthermore, Kubernetes supports two types of resource declarations: *requests* and *limits*.
*Resource requests* define the minimum value of a given compute resource that has to be guaranteed to the Pod.
Requests are used by the scheduler to decide on which worker node to place the Pod.
*Limits* define the maximum amount of resources available to the Pod.
By default, the supported resources are CPU and memory resources \cite{KubernetesDocumentationWorkloads_2021}.
Resource reservations and limits for other metrics (e.g., network or disk usage) can be added by installing third-party extensions --- these define so-called *extended resources*^[<https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/>].

<!-- https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#resource-units-in-kubernetes -->

Kubernetes uses *ReplicaSets* to manage the number of concurrently running instances of the same Pod (replicas).
A *Deployment* is a higher-level concept that manages the ephemeral ReplicaSets and Pods (Figure \ref{fig:k8s-objects}).
It provides many useful features to describe the desired state of an application \cite{KubernetesDocumentationWorkloads_2021}.
For example, if a node fails (hardware fault, power outage etc.), Kubernetes does not "recreate" individual Pods previously running on this node.
However, when Kubernetes detects that a Pod which is part of a Deployment is unavailable, it creates a new instance of the same Pod type (a behavior referred to as *self-healing*).
Therefore, Deployments provide a declarative orchestration interface for applications running on Kubernetes.

<!-- https://kubernetes.io/docs/concepts/workloads/controllers/deployment/ -->

\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{images/k8s_objects.pdf}
\caption{\label{fig:k8s-objects} Overview of built-in Kubernetes objects and their relations}
\end{figure}

A *Service* is an abstraction that gives a distinct network identity to an application running in one or more Pods (Figure \ref{fig:k8s-objects}).
This is necessary because Pods are ephemeral, meaning that they can be created or destroyed all the time -- alongside their associated IP addresses.
When an application communicates with another one through a *Service*, Kubernetes automatically forwards all requests to the *Service* to the associated *Pods*.
Thus, a Service provides a mechanism for *service discovery* (each Pod has a unique IP address, but Pods can be created and destroyed) as well as *load balancing* (by default Kubernetes uses a round-robin algorithm distribute requests across all Pods associated to a Service).
An example of a Service definition is shown in Appendix \ref{prometheus-service-discovery-with-kubernetes}.
A Service can also describe an entity outside the Kubernetes cluster, such as an external load balancer \cite{KubernetesDocumentationServices_2021}.

<!-- https://kubernetes.io/docs/concepts/services-networking/service/ -->

A *StatefulSet* is the equivalent of a Deployment but tailored for applications that require guarantees about the ordering and uniqueness of the application instances.
In particular, a StatefulSet offers stable, unique IP addresses; unique, stable Pod names; and ordered, graceful deployments \cite{KubernetesDocumentationWorkloads_2021}.
These constraints are vital for the correct and efficient operation of many stateful applications, such as databases or message queues.

<!-- https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/ -->

This section only covered the most important Kubernetes objects relevant to the work in this thesis.
An extended discussion of all objects can be found in \cite{KubernetesPatterns_2019}.

### Kubernetes Components

<!-- https://kubernetes.io/docs/reference/command-line-tools-reference/ -->
<!-- https://kubernetes.io/docs/concepts/overview/components/ -->

The *control plane* is the layer of components that exposes the API and interfaces to define, deploy and manage the lifecycle of Kubernetes objects \cite{KubernetesDocumentationComponents_2021}.
These components are drawn blue in Figure \ref{fig:k8s-architecture}.
The *data plane* is the layer that provides compute capacity (such as CPU, memory, network and storage resources) where objects can be scheduled.
Such capacities are made available through the *kubelet* running on each worker node: the daemon is responsible for the communication with the control plane.
The kubelet continuously gathers facts about its host and the workloads running on it (e.g., CPU, memory, filesystem, and network usage statistics), and sends them to the control plane.
These statistics are collected with *cAdvisor*^[<https://github.com/google/cadvisor>], which is an tool for container resource usage and performance analysis.

Based on the information provided by the worker nodes, the *scheduler* decides which workloads (i.e., Pods) will be placed on the worker node, subject to predefined constraints and runtime statistics.
The default scheduling policy is to place Pods on nodes with the most free resources, while distributing Pods from the same Deployment across different nodes.
In this way the scheduler tries to balance out resource utilization of the worker nodes \cite{KubernetesDocumentationComponents_2021}.

Then, the kubelet on the corresponding worker node receives the scheduling decision in the form of *PodSpecs* (Pod specifications).
It implements the received instructions by launching and monitoring the containers through the *container runtime* (e.g., containerd^[<https://containerd.io/>] or cri-o^[<https://cri-o.io/>]).
The kubelet also manages the lifecycle of other host-specific resources, such as storage volumes and network adapters \cite{KubernetesDocumentationComponents_2021}.

The *controller manager* (CM) implements the core functions of Kubernetes with control loops (such as replication, endpoints and namespace controller).
A control loop is a non-terminating loop that regulates the state of a system.
It is commonly found in industrial control systems and robotics.
The controller manager monitors the state of the cluster (including all its objects) and tries to converge the state of the system towards the desired state, thereby implementing Kubernetes' declarative nature \cite{KubernetesDocumentationComponents_2021}.

The user can interact with the control plane through a well-defined API.
The *kubectl* command line tool allows the user to retrieve and define the state of the cluster in a human-friendly manner.
Automation tools (e.g., Terraform) can also directly interact with the Kubernetes API for the same purpose.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/k8s_control_plane.pdf}
\caption{\label{fig:k8s-architecture} Architecture of Kubernetes (components in blue are part of control plane)}
\end{figure}

The *API server* provides the entrypoint to the control plane for internal and external components (Figure \ref{fig:k8s-architecture}).
Thus, it performs authentication, authorization, versioning and semantic validation functions.
It is responsible for communicating with all the other control plane components and is the only component which has direct access to the database \cite{KubernetesDocumentationComponents_2021}.
Kubernetes uses the strongly-consistent, distributed key-value store *etcd* as a shared database for the control plane.

The *Metrics Server* is an efficient and scalable cluster-wide aggregator of live utilization statistics.
It tracks CPU and memory usage across all worker nodes, as reported by the kubelet's cAdvisor (Figure \ref{fig:k8s-architecture}).
The Metrics Server implements the Metrics API^[<https://github.com/kubernetes/metrics>] and is the successor of the deprecated Heapster^[<https://github.com/kubernetes-retired/heapster>].
Other implementations (e.g., Prometheus) and adapters can be used as an alternative source for the Metrics API.
Notably, the Metrics API does not offer access to any historical usage statistics.

<!-- Importantly, all components interact through clearly defined APIs so that the implementation of each component can be replaced.
An example here is Virtual Kubelet^[<https://virtual-kubelet.io/>], which acts like a regular kubelet to the cluster, while provisioning resources on third-party platforms (e.g., AWS Fargate, HashiCorp Nomad) instead of an actual worker node. -->

There are several other components in the Kubernetes control plane which are not shown in Figure \ref{fig:k8s-architecture}, as they are not relevant to the work in this thesis.
