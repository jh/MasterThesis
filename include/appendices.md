# Appendices

## Prometheus Setup

\begin{lstlisting}[language=bash, numbers=none, caption=Installation of Prometheus Helm Chart with kube-state-metrics Exporter, label=src:prometheus-helm-chart]
helm repo add kube-state-metrics \
    https://kubernetes.github.io/kube-state-metrics
helm repo add prometheus-community \
    https://prometheus-community.github.io/helm-charts
helm repo update
helm install -n monitoring prometheus -f prometheus.values.yaml \
    --version 13.6.0 prometheus-community/prometheus
\end{lstlisting}

\begin{lstlisting}[language=yaml, numbers=none, caption=Configuration of Prometheus Helm Chart (\texttt{prometheus.values.yaml}), label=src:prometheus-helm-values]
alertmanager:
  enabled: false

kubeStateMetrics:
  enabled: true # deploys kube-state-metrics exporter

kube-state-metrics:
  image:
    repository: k8s.gcr.io/kube-state-metrics/kube-state-metrics
    tag: v1.9.8
  # enable only specific metric collectors:
  collectors:
    certificatesigningrequests: false
    configmaps: true
    cronjobs: true
    daemonsets: true
    deployments: true
    endpoints: true
    horizontalpodautoscalers: true
    ingresses: true
    jobs: true
    limitranges: true
    mutatingwebhookconfigurations: true
    namespaces: true
    networkpolicies: true
    nodes: true
    persistentvolumeclaims: false
    persistentvolumes: false
    poddisruptionbudgets: true
    pods: true
    replicasets: true
    replicationcontrollers: true
    resourcequotas: true
    secrets: true
    services: true
    statefulsets: true
    storageclasses: false
    validatingwebhookconfigurations: true
    verticalpodautoscalers: true
    volumeattachments: false

nodeExporter:
  enabled: false

pushgateway:
  enabled: false

configmapReload:
  prometheus:
    enabled: false

server:
  enabled: true
    repository: quay.io/prometheus/prometheus
    tag: v2.24.0
  global:
    scrape_interval: 15s
  retention: 15d
\end{lstlisting}

## Grafana Setup

\begin{lstlisting}[language=bash, numbers=none, caption=Installation of Grafana Helm Chart, label=src:grafana-helm-chart]
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install -n monitoring grafana -f grafana.values.yaml \
    --version 6.6.4 grafana/grafana
\end{lstlisting}

\begin{lstlisting}[language=yaml, numbers=none, caption=Configuration of Grafana Helm Chart (\texttt{grafana.values.yaml}), label=src:grafana-helm-values]
image:
  repository: grafana/grafana
  tag: 7.4.5

persistence:
  enabled: true

grafana.ini:
  server:
    domain: localhost
    root_url: "%(protocol)s://%(domain)s/grafana"
    serve_from_sub_path: true

ingress:
  enabled: true
  hosts:
    - "localhost"
  path: "/grafana"

datasources:
 datasources.yaml:
   apiVersion: 1
   datasources:
    - name: Prometheus
      type: prometheus
      access: proxy
      url: http://prometheus-server:80/prometheus
      default: true
\end{lstlisting}


## Prometheus Service Discovery with Kubernetes

\begin{lstlisting}[caption=Kubernetes Service Definition with Prometheus Annotations, label=src:prometheus-service-annotation, language=yaml, numbers=none]
apiVersion: v1
kind: Service
metadata:
  name: api-server # name of this service definition
  annotations: # tells prometheus to scrape this service
    prometheus.io/scrape: "true"
    prometheus.io/port: "2112"
spec:
  ports:
    - name: "http"
      port: 8080
      protocol: TCP
      targetPort: http
    - name: "pm-exporter"
      port: 2112
      protocol: TCP
      targetPort: pm-exporter
  selector: # binds service to matching pods:
    service: api-server
\end{lstlisting}

## Prometheus Adapter Setup

\begin{lstlisting}[caption=Installation of Prometheus Adapter Helm Chart, label=src:prometheus-adapter-helm-chart, language=bash, numbers=none]
helm repo add prometheus-community \
     https://prometheus-community.github.io/helm-charts
helm repo update
helm install -n monitoring prometheus-adapter --version 2.12.1 \
     -f prometheus-adapter.values.yaml \
     prometheus-community/prometheus-adapter
\end{lstlisting}

\begin{lstlisting}[caption=Configuration Prometheus Adapter Helm Chart (\texttt{prometheus-adapter.values.yaml}), label=src:prometheus-adapter-helm-values, language=yaml, numbers=none]
image:
  repository: directxman12/k8s-prometheus-adapter-amd64
  tag: v0.8.3

prometheus:
  url: http://prometheus-server.monitoring
  path: /prometheus
  port: 80

rules:
  default: true
  external:
    - seriesQuery: '{__name__=~"^esm_execution_tasks_total$"}'
      resources:
        overrides:
          kubernetes_namespace: {resource: "namespace"}
      # metric series from custom ESM exporter
      metricsQuery: esm_tasks_total{status="queued"}
      name:
        matches: ""
        as: "esm_execution_tasks_queued_total"
    - seriesQuery: '{__name__=~"^rabbitmq_queue_messages$"}'
      resources:
        overrides:
          kubernetes_namespace: {resource: "namespace"}
      # metric series from RabbitMQ exporter
      metricsQuery:'rabbitmq_queue_messages{queue="restrictedQueue"}'
      name:
        as: "esm_executor_queue_messages"
    - seriesQuery: '{__name__=~"^esm_configuration_runs_total$"}'
      resources:
        overrides:
          kubernetes_namespace: {resource: "namespace"}
      # metric series from custom ESM exporter:
      metricsQuery:'esm_configuration_runs_total{status="processing"}'
      name:
        as: "esm_configuration_runs_processing"
\end{lstlisting}

## HPA Scaling Policy

Scaling policy `sp-v1` had the `averageValue` set to 1 and `sp-v2` to 2, respectively.

\begin{lstlisting}[caption=HPA scaling policy \texttt{sp-v1} for production-like environment, label=src:real-autoscaling-policy, language=yaml, numbers=none]
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: executor
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: executor
  minReplicas: 1
  maxReplicas: 50
  metrics:
  - type: External
    external:
      metric:
        name: esm_executor_queue_messages
      target:
        type: AverageValue
        averageValue: 1
  - type: External
    external:
      metric:
        name: esm_configuration_runs_processing
      target:
        type: AverageValue
        averageValue: 1
  behavior:
    scaleDown:
      policies:
        - type: Percent
          value: 50
          periodSeconds: 60
      stabilizationWindowSeconds: 60
\end{lstlisting}

## HPA Log Messages

\begin{lstlisting}[caption=HPA log messages (abbreviated), label=src:hpa-log-messages, language=bash, numbers=none]
$ kubectl describe hpa/executor
Name: executor
Deployment pods: 1 current / 1 desired
Metrics:             ( current / target )
  "esm_tasks_queued_total":  0 / 1
Messages:
  Recommended size matches current size
  The HPA was able to successfully calculate a replica count from
  external metric esm_tasks_queued_total
  The desired replica count is less than the minimum replica count
  [...]
  New size: 5; external metric esm_tasks_queued_total above target
  New size: 10; external metric esm_tasks_queued_total above target
  New size: 5; All metrics below target
\end{lstlisting}

## VPA Setup

\begin{lstlisting}[caption=Installation of VPA Helm Chart, label=src:vpa-helm-chart, language=bash, numbers=none]
helm repo add fairwinds-stable https://charts.fairwinds.com/stable
helm repo update
helm install vpa fairwinds-stable/vpa -f vpa.values.yaml \
     --version 0.3.2 --namespace vpa --create-namespace
\end{lstlisting}

\begin{lstlisting}[caption=Configuration of VPA Helm Chart (\texttt{vpa.values.yaml}), label=src:vpa-helm-values, language=yaml, numbers=none]
recommender:
  enabled: true
  image:
    tag: "0.9.2"
  extraArgs:
    v: '4' # verbosity level
updater:
  enabled: false
admissionController:
  enabled: false
\end{lstlisting}

\clearpage

## KEDA Setup

\begin{lstlisting}[caption=Installation of KEDA Helm Chart, label=src:keda-helm-chart, language=bash, numbers=none]
helm repo add kedacore https://kedacore.github.io/charts
helm repo update
helm install keda -f keda.values.yaml kedacore/keda \
    --version v2.2.2 --namespace keda --create-namespace
\end{lstlisting}

\begin{lstlisting}[caption=Configuration of KEDA Helm Chart (\texttt{keda.values.yaml}), label=src:keda-helm-values, language=yaml, numbers=none]
image:
  keda:
    repository: ghcr.io/kedacore/keda
    tag: 2.2.0
  metricsApiServer:
    repository: ghcr.io/kedacore/keda-metrics-apiserver
    tag: 2.2.0
\end{lstlisting}

\begin{lstlisting}[caption=Installation of KEDA ScaledObject, label=src:scaledobject, language=bash, numbers=none]
$ kubectl get hpa
NAME               REFERENCE         TARGETS     MINPODS   MAXPODS
keda-hpa-exec-so   deploy/executor   0/1 (avg)   1         50
$ kubectl get scaledobject
NAME      SCALETARGETNAME  MAX   TRIGGERS   READY   ACTIVE   AGE
exec-so   executor         50    rabbitmq   True    False    1m
$ kubectl get -n esm deployment -l service=executor
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
executor   0/0     0            0           1h
\end{lstlisting}

## Modular Kubernetes Autoscaler CRD

\begin{lstlisting}[caption=Example CRD of modular Kubernetes autoscaler, label=src:wasmpa-crd, language=yaml, numbers=none]
apiVersion: wasmpa.io/v1alpha1
kind: ScaledObject
metadata:
  name: server-so
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: server
  metrics:
  - type: Resource
    metricName: cpu
    target: 0.6 # 60% CPU load
  - type: External
    metricName: http_request_duration_seconds
    target: 0.1 # 100ms response time
  algorithm:
    name: wasmpa-arima # use ARIMA algorithm for estimations
    params: [] # list of additional parameters
\end{lstlisting}
