# Evaluation

\chapterquote{Sometimes magic is just someone spending more time on something \\ than anyone else might reasonably expect.}{Raymond Joseph Teller}{12cm}
\noindent
The previous chapter documented the necessary steps for setting up a metric collection system and implementing an autoscaling mechanism on top of it.
This chapter focuses on a quantitative evaluation of the performance and cost improvements made by autoscaling.
Our findings demonstrate that the target application is able to achieve maximum performance with the autoscaling policies, while having only minor variances in performance.
At the same time, significant cost-savings (more than 19%) can be realized thanks to downscaling during times of low load.
We follow the guidelines on scientific benchmarking by Hoefler and Belli \cite{ScientificBenchmarkingParallelComputing_2015} to ensure reproducibility and interpretability of our results.

<!-- Rule 1: When  publishing  parallel  speedup,  report  if  the  basecase is a single parallel process or best serial execution, as wellas the absolute execution performance of the base case. -->
<!-- Rule 2: Specify the reason for only reporting subsets of standardbenchmarks or applications or not using all system resources. -->
<!-- Rule 3: Use the arithmetic mean only for summarizing costs. Use the harmonic mean for summarizing rates. -->
<!-- Rule 4: Avoid summarizing ratios; summarize the costs or ratesthat the ratios base on instead.  Only if these are not availableuse the geometric mean for summarizing ratios. -->
<!-- Rule 5: Report if the measurement values are deterministic. For nondeterministic data,  report confidence intervals of the measurement. -->
<!-- Rule 6: Do not assume normality of collected data (e.g.,, basedon the number of samples) without diagnostic checking. -->
<!-- Rule 7: Compare nondeterministic data in a statistically soundway, e.g.,, using non-overlapping confidence intervals or ANOVA. -->
<!-- Rule  8: Carefully  investigate  if  measures  of  central  tendencysuch as mean or median are useful to report.  Some problems,such as worst-case latency, may require other percentiles. -->
<!-- Rule 9: Document all varying factors and their levels as well asthe complete experimental setup (e.g., software, hardware, techniques) to facilitate reproducibility and provide interpretability. -->
<!-- Rule 10: For parallel time measurements, report all measure-ment, (optional) synchronization, and summarization techniques. -->
<!-- Rule 11: If possible, show upper performance bounds to facili-tate interpretability of the measured results. -->
<!-- Rule 12: Plot as much information as needed to interpret theexperimental results. Only connect measurements by lines if theyindicate trends and the interpolation is valid. -->

As outlined in the introduction, autoscaling always needs to make a trade-off between optimizing for application performance and optimizing for cost, i.e., allocated resources.
If cost is not an issue, one could simply allocate a fixed, large number of resources and always keep those running --- known as dimensioning for peak load.
However, this is not cost-effective, especially not in a world where cloud resources (such as virtual machines and containers) can be allocated and are billed by the second.
Thus, the goal is to always keep the number of allocated resources as low as possible --- with some reasonable safety margin to compensate unforeseen deviations.

## Benchmark Setup

<!-- In order to quantify the improvements made by the autoscaling components, we also develop performance and cost benchmarks for the target application. -->
To measure the performance of the system we use one of the application-level metrics we previously exposed in Section \ref{prometheus-exporters}: the duration of configuration runs.
A configuration run refers to the configuration of a fixed set of systems being checked and updated.
This process is commonly triggered by the user through a web interface, thus it is deemed a relevant metric for measuring the performance of the application.

To measure the cost of horizontal scaling we use *replica seconds*: the number of running Pods per second integrated over the time period of the benchmark.
For example, if the benchmark lasts one minute and two replicas are running the entire time, this would result in 120 replica seconds.
Prometheus is not suitable for such time-accurate measurements \cite{PrometheusDocumentation_2021}, therefore we implement our own tool that fetches this data directly from the Kubernetes API.

<!-- \begin{leftrule} -->
<!-- \emph{If you need 100\% accuracy, such as for per-request billing, Prometheus is not a good choice as the collected data will likely not be detailed and complete enough.} -->
<!-- \newline -->
<!-- -- Prometheus Documentation \cite{PrometheusDocumentation_2021} -->
<!-- \end{leftrule} -->

We set up a performance benchmark for the application with a constant workload size, which allows us to measure the cost-reductions that can be achieved through autoscaling.
Thus, our experiment evaluates the *strong scaling* behavior \cite{ScientificBenchmarkingParallelComputing_2015} of the application and -- by extension -- the autoscaler.
In the context of computing performance, strong scaling refers to the addition of more processors while the problem size is constant.
An individual benchmark run is structured as follows:

\clearpage

1. Set up and initialize target application from scratch.
2. Start the benchmark timer.
3. After 60 seconds, start a configuration run. Sequentially repeat ten times.
4. Wait until all configuration runs finish (this marks the *time to completion*).
5. Continue running the application for 30 minutes after starting the benchmark timer. Then, stop the *replica seconds* counter.
6. Terminate the application.

The entire benchmark procedure is repeated three times.
We verified the benchmark results do not contain outliers or other inconsistencies.
<!-- If there are outliers they are reported, otherwise the results are averaged where appropriate. -->
Between each benchmark the target application is completely removed from the cluster (the Kubernetes namespace is deleted) and re-installed into a new namespace.
This ensures the benchmark runs are completely isolated and no transient side effects (such as warm caches) are present.
The setup phase described above is entirely scripted to minimize potential for variation.

Configuration runs are started exactly 60 seconds apart from each other.
Since they take longer than 60 seconds (see Figure \ref{fig:config-run-variance-static}), the application needs to process multiple configuration runs in parallel.
Each configuration run contains the same amount of work.
The *time to completion* marks the point when all configuration runs have finished processing.
The application continues to run afterwards (until a fixed timeout) to demonstrate the cost-savings made possible by autoscaling.
If the benchmark was stopped as soon as the workload was completed, only the performance (but not the cost) could be compared.
Because of the fixed benchmark duration, the cost-savings can be extrapolated to one day, week or month.

The benchmarks are performed on a Kubernetes cluster consisting of one control plane node and two worker nodes.
The worker nodes have a combined capacity of 136 CPU cores and 1134 GiB memory.

## Functional Verification

This section focuses on testing the functionality of the autoscaling setup, as there are many components involved and several parameters to be tweaked.
For this reason an artificial task (`openssl speed`) is given to the executor component of the target application (see Chapter \ref{implementation}).
This allows us to iterate quickly and test out different configuration settings and scaling metrics.
In Section \ref{real-world-test-scenario} we set up a production-like environment for performing the benchmarks with a real workload, which connects to and configures external systems.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/mock-results/performance-cost-benchmark-static.pdf}
\caption{\label{fig:performance-cost-benchmark-static} Results of scaling benchmark. Scaling setting indicates static number of replicas or autoscaling policy.}
\end{figure}

As explained at the beginning of Chapter \ref{implementation}, the number of executors in the target application can be adjusted.
To get a baseline for the application behavior, we tested several static values for replicas.
The graph in Figure \ref{fig:performance-cost-benchmark-static} shows the results with the cost (replica seconds) on the x-axis and the performance (time to completion, i.e., time until the simulated user has all desired results) on the y-axis.
Naturally, the lowest cost (4.000 replica seconds) is achieved when using 2 replicas, which also has by far the largest time to completion (2.000 seconds).
The highest cost (14.000 replica seconds) is recorded with 20 replicas, which also has the lowest time to completion (4.000 seconds).
Due to the constant workload size, the benchmarks with 10, 15 and 20 replicas have almost the same time to completion, while having significantly more replicas and therefore replica seconds.
This means that for this workload size, more than 10 replicas are inefficient, since most of the replicas are idle (unused) during the execution.
In this scenario, the goal of the autoscaler is to optimize towards the bottom left corner (low cost and high performance), irrespective of the type and size of the given workload.

<!-- \begin{figure}[ht] -->
<!-- \centering -->
<!-- \includegraphics[width=0.85\textwidth]{images/replicas-20-vs-replicas-3.png} -->
<!-- \caption{\label{fig:20-vs-3-replicas} Performance results of static scaling.  Left: 20 Replicas, Right: 3 Replicas} -->
<!-- \end{figure} -->

<!-- The same behavior is illustrated in Figure \ref{fig:20-vs-3-replicas}, but with metrics collected from the monitoring system: the left side shows a benchmark run with 20 replicas, the right side one with 3 replicas. -->
<!-- With just 3 replicas, there are many tasks queued for a long period of time (yellow line), which causes the average queue time (green line) to continuously rise, up to a value of 6 minutes. -->
<!-- With 20 replicas, the average queue time never rises above 30 seconds. -->

Data from the monitoring system shows that with 3 replicas the average queue duration (time before tasks are actually executed) continuously rises during the benchmark up to a value of 360 seconds.
With 20 replicas, the average queue time quickly converges to a value of 30 seconds.
This highlights the effect of not having enough executor replicas available which leads to significant delays, since each executor may only process one task at a time.

Figure \ref{fig:config-run-variance-static} shows the execution time of individual configuration runs during the benchmark.
It confirms our intuition that with an overall lower time to completion (as it was the case in Figure \ref{fig:performance-cost-benchmark-static}), the time of individual workloads is also smaller.
Furthermore, it shows that a low number of replicas has a significant effect on the variance of the configuration run's execution times.
While each configuration run has the same workload, with a low number of replicas a significant increase in variance (in addition to an increase of the average) can be observed.
This is explained by the fact that with few replicas, the same executor needs to run multiple workloads sequentially, which slows some of them down drastically.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/mock-results/config-run-variance-static.pdf}
\caption{\label{fig:config-run-variance-static} Execution time of individual configuration runs (3 benchmarks, 10 configuration runs per benchmark). Numbers in blue indicate the mean value.}
\end{figure}

After establishing the performance and cost characteristics of static configurations, we evaluate the characteristics of a basic autoscaling setup.
The first version of the autoscaling policy was shown in Listing \ref{src:hpa-scale-v0}.
We now evaluate the behavior of this policy, which is labeled as `hpav0` in the figures.
As Figure \ref{fig:performance-cost-benchmark-static} shows, the benchmark with this policy had a similar performance and cost as a static configuration with 10 replicas: the application used around 18.600 replica seconds during the benchmark and the total time to completion was approx. 700 seconds.

This result highlights the strengths and weaknesses of this autoscaling policy: it enabled the application to achieve the same performance as with a static configuration of 10 replicas.
As Figure \ref{fig:performance-cost-benchmark-static} shows, this is the maximum amount of performance the application is able to deliver for this particular workload.
At the same time, the autoscaling policy was just as costly as a static configuration of 10 replicas, even though for significant periods the application was running with only 1 replica.
This is explained by Figure \ref{fig:hpa-scaling-v0}: the policy overscaled the number of replicas (more than the necessary value established previously) and even reached the replica limit (`maxReplicas` from Listing \ref{src:hpa-scale-v0}).

Furthermore, the variance in execution time between different configuration runs was slightly larger than with 10 replicas.
This can be explained by the fact that the autoscaler gradually needs to scale up the Deployment at the beginning of the benchmark.
Thus, the executor Deployment does not have the optimal resources immediately available and some tasks need to wait longer in the queue.
This behavior can be observed in \mbox{Figure \ref{fig:hpa-scaling-v0}}.

In summary, the autoscaling policy has performed well (no performance loss, minor introduction of variance), but has not realized any cost-savings in our experiments due to drastic overscaling (provisioning more replicas than required for the workload).

\clearpage

## Cost Optimization

While the initial scaling policy successfully scaled the Deployment during the benchmark, we identified several aspects for improvement.
This section addresses these issues by fine-tuning the scaling policy.

* **Delayed scale-down**: the number of replicas is not reduced soon enough after the workload has finished, as is apparent from the number of queued tasks compared to the number of replicas (Figure \ref{fig:hpa-scaling-v0}).
* **Potential for premature scale-down**: if a task has a long execution time, the autoscaler might reduce the number of replicas too early because the scaling metric only depends on queued tasks.
* **Overscaling of replicas**: our previous experiments with static replica configurations have shown that provisioning 20 replicas is ineffective, since it does not increase performance (as shown in Figure \ref{fig:performance-cost-benchmark-static}).

The delayed scale-down can be tackled by adjusting the *stabilization window* of the scaling policies (also referred to as *cool-down period*).
This setting (shown in Listing \ref{src:downscale-behavior}) specifies how soon HPA starts removing replicas from the Deployment after it detects that the scaling metric is below the target value \cite{KubernetesDocumentationHorizontalPod_2021}.
The default value is 5 minutes (observable in Figure \ref{fig:hpa-scaling-v0}); we adjust the value to 1 minute (line 7).
Decreasing the downscale stabilization window can lead to thrashing (continuous creation and removal of Pods) when workload bursts are more than the specified window apart, but offers better elasticity \cite{QuantifyingCloudElasticityContainerbased_2019}.
In our case this is an acceptable trade-off because these containers start quickly, as this application component is lightweight and does not hold any internal state.
Additionally, this risk is partially mitigated by only allowing HPA to remove 50% of the active replicas per minute (Listing \ref{src:downscale-behavior}, line 4-6).
By default, HPA is allowed to deprovision all Pods <!-- (down to `minReplicas`) --> at the same time \cite{KubernetesDocumentationHorizontalPod_2021}, as it is illustrated in Figure \ref{fig:hpa-scaling-v0} (rapid decrease from 20 to 1 replica).

\begin{lstlisting}[caption=Improved Downscale Behavior for HPA, label=src:downscale-behavior, language=yaml, numbers=left]
  behavior:
    scaleDown:
      policies:
        - type: Percent
          value: 50
          periodSeconds: 60
      stabilizationWindowSeconds: 60
\end{lstlisting}

The potential for premature scale-down is reduced by not only considering the queued tasks as a scaling metric, but also the currently running tasks.
Just because all tasks have been taken out of the message queue by the executors does not mean that the number of executors can be reduced, as they might still be processing the tasks.
For this purpose, HPA allows specifying multiple metrics (Listing \ref{src:improved-scaling-logic}): it calculates the desired replica count for all specified metrics individually and then scales the Deployment based on the maximum results.

Finally, the issue of overscaling replicas can be mitigating by switching to a different baseline scaling metric, shown in Listing \ref{src:improved-scaling-logic} (line 5).
This metric immediately represents the number of tasks available for the executor, unlike all future tasks as before.
This distinction is important because future tasks might have interdependencies (e.g., if task #1 fails, task #2 and #3 does not need to be executed).
Additionally, until now we have been using an absolute value as a target, e.g., the total number of tasks in queue.
It makes more sense to use a metric that incorporates the current number of replicas as a ratio.
This is necessary because -- with the change explained previously -- the scaling metric contains the number of available tasks, which are by definition distributed across all executors.
Instead of using the scaling metric directly, the raw value is averaged: it is divided by the number of active Pod replicas (Listing \ref{src:improved-scaling-logic}, line 7-8) \cite{KubernetesDocumentationHorizontalPod_2021}.

\begin{lstlisting}[caption=HPA scaling based on multiple metrics, label=src:improved-scaling-logic, language=yaml, numbers=left]
  metrics:
  - type: External
    external:
      metric:
        name: esm_tasks_queued_total
      target:
        type: AverageValue
        averageValue: 1
  - type: External
    external:
      metric:
        name: esm_configuration_runs_processing
      target:
        type: AverageValue
        averageValue: 1
\end{lstlisting}

To find the appropriate value for `averageValue`, we perform several benchmarks, the results of which are shown Figure \ref{fig:performance-cost-benchmark-autoscaling} to \ref{fig:config-run-variance-autoscaling}.
The different `averageValues` have been labeled as `hpav1`, `hpav2`, `hpav3` and `hpa4` for the values 1, 2, 3, and 4, respectively.
These new benchmarks also include the other optimizations outlined above.
For comparison, the following figures also show the previously discussed benchmark results of the initial autoscaling policy (`hpav0`) and a statically scaled Deployment with 5 and 10 replicas.

Due to the downscaling optimization outlined above, all of the scaling policies were able to reduce the cost (Figure \ref{fig:performance-cost-benchmark-autoscaling}) by 50% as they allow scaling the Deployment down sooner.
This behavior is illustrated in Figure \ref{fig:comparison-hpa-scaling-policies}.
Additional cost savings can be realized by reducing the replica count to 0 when there are no tasks to be processed.
In our application architecture this is feasible because the executors are taking the tasks out of a message queue, which acts as a buffer when no executors are available (yet).
However, HPA does not support this by default \cite{KubernetesDocumentationHorizontalPod_2021}: setting `minReplicas` field (see Figure \ref{fig:hpa-scaling-v0}) to 0 is only possible when changing the configuration of the Controller Manager, which requires re-deploying the cluster.
KEDA works around this HPA limitation by having an agent that scales the Deployment to zero when no tasks are active.

\clearpage

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{plots/mock-results/performance-cost-benchmark-autoscaling.pdf}
\caption{\label{fig:performance-cost-benchmark-autoscaling} Results of autoscaling benchmark. Scaling setting indicates static number of replicas or autoscaling policy.}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{images/comparison-of-hpa-scaling-policies.png}
\caption{\label{fig:comparison-hpa-scaling-policies} Grafana screenshot of different horizontal scaling policies. Y-axis represents the number of active replicas, X-axis represents time.}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{images/scaling-policies-average-queue-time.png}
\caption{\label{fig:autoscaling-average-queue} Grafana screenshot of queuing behavior with different autoscaling policies. Y-axis shows average time tasks are queued in seconds.}
\end{figure}

\clearpage

Despite the significant cost reduction, all scaling policies performed nearly as well as the  fastest configuration (with 10 replicas).
This establishes the effectiveness of the autoscaling policies, as confirmed by comparing the average time tasks spend in queue (Figure \ref{fig:autoscaling-average-queue}): there is a gradual increase from 38.1 seconds with `hpav1`, 45.4 seconds with `hpav2`, 76.4 seconds with `hpav3` to 104.0 seconds with `hpav4`.
A comparison against static dimensioning shows that the values are quite stable (i.e., the average value is not continuously rising) and are almost as low as the best performing static scaling configuration (30 seconds with 20 replicas).
Therefore, different parameters for `averageValue` can be used to tweak the trade-off between application performance and cost.
With our specific test scenario a value of one provides excellent performance while already realizing major cost savings.
<!-- The ideal value depends on the workload scenario. -->
<!-- Thus, it should be empirically determined and adjusted based on user preferences. -->
Clearly, different user preferences (performance-cost trade-off) will require different values.
<!-- We found this value to work well in our case, but generally the ideal value should be adjusted based on user preferences (performance-cost trade-off) and workload scenario. -->

Figure \ref{fig:config-run-variance-autoscaling} shows the impact of the `averageValue` parameter on the duration of individual configuration runs.
A larger value effectively allows more tasks to be waiting in the queue without triggering any scaling.
Since configuration runs are made up of many individual tasks, the waiting time of these tasks affects the duration of the entire configuration run.
All of the autoscaling policies exhibit higher variance in configuration run durations than a static replica number of 10.
Between the autoscaling policies, the `averageValue` does not seem to have a major impact on variance.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/mock-results/config-run-variance-autoscaling.pdf}
\caption{\label{fig:config-run-variance-autoscaling} Execution time of individual configuration runs. Numbers at the top (in blue) indicate the mean value.}
\end{figure}

\clearpage

## Real-world test scenario

The previous sections validated the functionality of the autoscaling setup as well as evaluated several scaling metrics and parameters based on an artificial workload.
In this section we set up production-like target systems, consisting of 25 virtual machines, and configure the application to connect to these systems via SSH.
In addition, the configuration scripts used to interact with the systems are representative of tasks carried out in production environments:
each script consists of 42 checks for system security settings such as administrator access, login retry interval etc.

We repeat the benchmark scenario described in Section \ref{benchmark-setup} with adjustments for the production-like scenario:
the overall benchmark time (75 minutes), number of configuration runs (50) and maximum replicas (50) are increased to accommodate workload.
Thus, the results from Section \ref{benchmark-setup} cannot be compared in absolute terms to the results presented in these sections, though we expect to confirm the trends from our previous findings.

Appendix \ref{hpa-scaling-policy} (Listing \ref{src:real-autoscaling-policy}) shows the full horizontal autoscaling policy used in HPA benchmarks.
The KEDA benchmarks use the scaling policy shown in Listing \ref{src:keda-v1}.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/real-results-static/performance-cost-benchmark.pdf}
\caption{\label{fig:real-performance-cost-benchmark} Results of autoscaling benchmark in production-like scenario with constant workload. Scaling setting indicates static number of replicas or autoscaling policy.}
\end{figure}

The benchmark results shown in Figure \ref{fig:real-performance-cost-benchmark} confirm our previous experiments:
the autoscaling policy `sp-v1` (blue) as well as KEDA (green) achieve the same performance as a static replica number of 10.
This is the maximum performance the application is able to achieve in this scenario, because even with higher replica counts (e.g., 20 in Figure \ref{fig:real-performance-cost-benchmark}) the performance remains the same.
<!-- 10 is the ideal replica count in this scenario because even with 20 replicas, no higher performance can be achieved. -->
At the same time, all scaling policies are able to consistently reduce the cost during the benchmark:
`sp-v1` has 19.3% lower cost and `keda` has 18.1% lower cost while maintaining the same performance as 10 replicas.
The same performance is explained by the fact that internally KEDA uses HPA for autoscaling and in this case the same target metric and value was specified for KEDA and HPA.
Logically, the `keda` autoscaling policy should have a lower cost than `sp-v1` because KEDA has the ability to scale the Deployment down to zero replicas (as opposed to `sp-v1` which has a minimum of one replica).
These cost savings did not manifest themselves in the benchmarks because most of the time there is load on the system.
Scaling the Deployment to zero has larger benefits when there are significant periods where a particular service is completely idle.
`sp-v2` has 38.6% lower cost while having worse performance than 10 replicas.
This is due to the fact that `sp-v2` allows more tasks to be in queue compared to `sp-v1` and `keda`, thereby increasing the time to completion.

Looking at the execution time of individual configuration runs (Figure \ref{fig:real-config-run-variance}), we see that the autoscaling policies have a higher variance compared to a static over-provisioning of resources.
While the means of `sp-v1` and `keda` are just slightly elevated compared to the result of 10 static replicas, the mean of `sp-v2` is double that of `sp-v1`.
Logically this is correct because scaling policy `sp-v2` allows twice the number tasks in queue.
Additionally, we can also see that the variance of `keda` is slightly higher than that of `sp-v1`.
This can be explained by the additional latency that KEDA has when scaling the Deployment up from zero to one replica.


\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/real-results-static/config-run-variance.pdf}
\caption{\label{fig:real-config-run-variance} Execution time of individual configuration runs in production-like scenario with constant workload. Numbers at the top (in blue) indicate the mean value.}
\end{figure}

Exemplary behaviors of the autoscaling policies during the benchmark are shown in Figure \ref{fig:real-scaling-activity-static}.
In blue it shows the replica count of `sp-v1`, orange the replica count of `sp-v2` and green the replica count of `keda`.
The red peaks indicate when new work has been submitted to the system (their height does not have any significance).
From this example it is clear that scaling policy `sp-v2` suffers from *thrashing*: scaling operations are frequently made and then reverted shortly afterwards again.
`sp-v1` exhibits a much smoother line, which is expected with a constant workload, and takes 13 minutes to converge to a stable number of replicas (11).
Similarly, the scale up of `keda` was slightly delayed and converged to the same number of replicas after 18 minutes.
Overall, `sp-v2` scales the Deployment to a slightly lower number (and thereby higher cost, Figure \ref{fig:real-performance-cost-benchmark}), as it allows for more tasks to be in queue.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/real-scaling-activity-static.png}
\caption{\label{fig:real-scaling-activity-static} Grafana screenshot of scaling activity during production-like scenario with constant workload. Red bars indicate the launch of new configuration runs.}
\end{figure}

In general, the results in Figure \ref{fig:real-performance-cost-benchmark}, \ref{fig:real-config-run-variance} and \ref{fig:real-scaling-activity-static} show that the monitoring systems, autoscalers and scaling policies described in the previous sections are able to scale the executor Deployment with satisfying results.
The scaling policy is able to maintain overall application performance (the time until the user is shown the result) while reducing the cost (amount of allocated cluster resources) and also incurs a minor penalty in the variance of the duration of individual configuration runs.

In addition, we want to investigate how these autoscaling policies behave under a highly variable workload.
For this purpose, the workload of individual configuration runs is increased and their schedule is adjusted: eight configuration runs are launched (in one minute intervals), followed by a three minute period without new work.
This pattern is repeated five times, resulting in a total of 40 configuration runs.
This scenario tests the elasticity of the autoscaling polices. <!-- to scale in and out. -->

Naturally, the results of these experiments are more variable than the previous ones.
Figure \ref{fig:variable-performance-cost-benchmark} shows the performance-cost trade-off and in particular the lower cost achieved by the autoscaling policies, while almost reaching the maximum performance (total time to completion).
This illustrates that the autoscaling policies `sp-v1` and `keda` are able to adjust to the varying workload and deliver competitive results.
However, Figure \ref{fig:variable-config-run-variance} highlights a minor flaw:
on average individual configuration runs are much slower compared to the best case (10 static replicas) and there is a large variance in their durations (even though each configuration run contains the same amount of work).
We believe this is at least partly due to the frequent scaling operations required during this benchmark (Figure \ref{fig:real-scaling-activity-variable}).
In the scenario with varying workload, especially `sp-v1` and `keda` had trouble converging to a stable number of replicas (compare blue and green lines in Figure \ref{fig:real-scaling-activity-static} to \ref{fig:real-scaling-activity-variable}).
As discussed previously, a trade-off has to be made between stability of the system (in this case less scaling operations by increasing the cool-down period) and agility (more frequent scaling operations allow for more cost-savings).

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{plots/real-results-variable/performance-cost-benchmark.pdf}
\caption{\label{fig:variable-performance-cost-benchmark} Results of autoscaling benchmark with varying workload. Scaling setting indicates static number of replicas or autoscaling policy.}
\end{figure}

One limitation that is not reflected in our test scenario is the *noisy neighbor* problem.
While CPU and memory resources can be isolated with Kubernetes by using resource requests (Section \ref{kubernetes-objects}), this is not supported for storage I/O and network resources \cite{NBWGuardRealizingNetworkQoS_2018}.
Thus, when creating more replicas of a Pod, it is possible that these instances compete for the same shared, non-isolated resources, and subsequently fail to achieve maximum performance.
In our benchmarks we minimized the effect of this issue by provisioning enough cluster capacity of these resources.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{plots/real-results-variable/config-run-variance.pdf}
\caption{\label{fig:variable-config-run-variance} Execution time of individual configuration runs with varying workload. Numbers at the top (in blue) indicate the mean value.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/real-scaling-activity-variable.png}
\caption{\label{fig:real-scaling-activity-variable} Grafana screenshot of scaling activity during production-like scenario with varying workload. Red bars indicate launch of new configuration runs.}
\end{figure}
