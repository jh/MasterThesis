# Introduction

\chapterquote{A distributed system is one where the failure of a computer \\ you didn't even know existed can render your own computer unusable.}{Leslie Lamport}{12.5cm}
\noindent
Cloud computing <!-- is a \improve{computing paradigm} that --> allows users to access compute, storage and network resources on-demand over the Internet.
Resources can be allocated and released whenever needed, thus *elasticity* is one of the most prominent features of cloud computing \cite{NISTDefinitionCloudComputing_2011}.
Around the same time that this computing paradigm became widely used, the *microservices* software architecture also became popular.
In fact, these two trends are correlated, since cloud infrastructure facilitates the development of distributed microservice architectures \cite{BuildingMicroservicesDesigningFinegrained_2021}.

To take full advantage of the elasticity in cloud computing, *autoscaling* (sometimes also referred to as *adaptive scaling*) needs to be implemented.
It is a technique to automatically scale the application (and the services it is composed of) based on the current demand.
In general, *scaling* refers to acquiring and releasing resources while maintaining a certain application performance level, such as response time or throughput \cite{UnderstandingDistributedSystems_2021}.
Scaling an application can be achieved in two ways: *horizontal scaling* and *vertical scaling*.
Horizontal scaling, also referred to as *scaling out*, refers to creating more instances of the same service.
The workload is then distributed across all instances, resulting in a lower workload per instance (*load balancing*).
An example here is adjusting the number of web servers according to the amount of incoming website requests.
Vertical scaling, also referred to as *scaling up*, refers to giving more resources (compute, memory, network, storage) to a particular instance of the service.
By giving more resources to one or multiple instances, they are able to handle more workload.
An example for this is providing more memory resources to a database instance:
this commonly results in faster response times, because the database can fit more data in memory instead of having to load it from disk.

Due to the higher cost of cloud infrastructure compared to on-premise infrastructure, it is vital to take advantage of its elasticity and implement autoscaling.
This autoscaling needs to provision and release cloud resources without human intervention.
Overprovisioning leads to paying for unused resources, while underprovisioning causes the application performance to degrade \cite{AutoScalingWebApplicationsClouds_2018}.
The scaling logic needs to balance between these two goals: minimizing resource usage (and thereby cost) with an acceptable service quality and minimizing service-level agreement violations by provisioning sufficient amount of resources.

To reliably and consistently achieve this in the first place, extensive monitoring of low- and high-level metrics needs to be set up.
These metrics are not only used as inputs for a scaling policy, but also to ensure that the system is not spiraling out of control (e.g., erroneously requesting more and more compute resources, thereby incurring large bills or starving other services for resources).
Even then, efficiently operating and scaling a complex mesh of microservices is a task with many challenges \cite{MicroscalerAutomaticScalingMicroservices_2019}.

## Contribution and Outline

This thesis investigates how applications running on top of Kubernetes can be dynamically scaled, thereby allowing to take full advantage of the elasticity of cloud computing.
In particular, we focus on the prerequisites (metrics-based monitoring) and the challenges of autoscaling (identifying the right metrics and eliminating bottlenecks).

The contributions of this thesis are the following:

* a thorough discussion of Kubernetes concepts and components relevant for autoscaling;
* an overview of generic autoscaling literature and a qualitative comparison of research proposals for Kubernetes autoscalers;
* a proposal for a novel, modular Kubernetes autoscaler with a WebAssembly sandbox;
* the implementation of an extensive monitoring solution for a production-grade application running on Kubernetes (with Grafana, Prometheus and several metric exporters);
* a discussion of which types of metrics are suitable for scaling and how metrics can be used to get a holistic view of application performance;
* the implementation and fine-tuning of autoscaling policies for the target application (with HPA and KEDA);
* a quantitative evaluation of several autoscaling policies according to performance and cost criteria.

<!-- The thesis will investigate which type of scaling can be used for which kind of workload and how these scaling mechanism can be implemented for Kubernetes-based -->

The rest of this thesis is organized as follows.
Chapter \ref{background} elaborates on the joint rise of cloud computing, containers and microservices, and how Kubernetes unifies these three concepts.
This is followed by an introduction of Kubernetes' architecture and components relevant to scaling.
Chapter \ref{autoscaling} outlines state-of-the-art autoscaling components for Kubernetes.
It also presents and evaluates recent research about Kubernetes autoscalers, and presents a solution to close the gap between academia and industry on this topic.
Chapter \ref{implementation} documents the concrete setup of metrics-based monitoring for an application running on Kubernetes and the associated autoscaling infrastructure.
In Chapter \ref{evaluation}, this infrastructure is used to conduct quantitative experiments about the behavior, performance and cost of different autoscaling policies.
It also discusses and validates several policy optimizations.
Finally, \mbox{Chapter \ref{conclusion}} provides concluding remarks and outlines future work.
