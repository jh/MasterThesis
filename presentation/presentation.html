<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title>Dimensioning, Performance and Optimization of Cloud-native Applications - Jack Henschel</title>
	<meta name="description" content="Jack Henschel's Master's thesis presentation">
	<meta name="author" content="Jack Henschel">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="dist/reset.css">
	<link rel="stylesheet" href="dist/reveal.css">
	<link rel="stylesheet" href="dist/theme/serif.css" id="theme">
	<link rel="stylesheet" href="charter.css">
	<!-- Theme used for syntax highlighting of code -->
	<link rel="stylesheet" href="plugin/highlight/monokai.css">
    <style>
      :root {
          --r-main-font: Charter, Palatino Linotype, Book Antiqua, Palatino, FreeSerif, serif;
          --r-heading-font: Charter, Palatino Linotype, Book Antiqua, Palatino, FreeSerif, serif;
      }
      .reveal ol, .reveal dl, .reveal ul {
	      display: block !important;
	      text-align: left;
	      margin: 0 auto;
      }
      i {
          font-style: italic;
      }
    </style>
  </head>
  <body>
	<div class="reveal">
	  <div class="slides">
		<section>
		  <h3>Dimensioning, Performance and Optimization of Cloud-native Applications</h3>
          <br>
		  <p>
            Jack Henschel
            <br>
            <small>2021-06-22</small>
		  </p>
          <p>
            <img data-src="Aalto_University_logo.svg" height="150em">
            &nbsp; &nbsp;
            <img data-src="Eurecom.svg" height="150em">
            &nbsp; &nbsp;
            <img data-src="ericsson.png" height="150em">

            <span style="width: 100%; text-align: center;"><small>
                Mario Di Francesco
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                Raja Appuswamy
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                Yacine Khettab
            </small></span>
          </p>

          <aside class="notes">
            Good morning everyone!
            In my thesis I have been tackling the challenge of how cloud-native applications can be effectively dimensioned, their performance assessed and based on that optimized and scaled.
            One particular focus of my thesis was the topic of Kubernetes autoscaling, which builds on top of these concepts.
            <br>
            My supervisor from Aalto University is Prof. Mario Di Francesco and my supervisor from Eurecom is Prof. Raja Appuswamy.
            I carried out this work at Ericsson Finland with my advisor Yacine Khettab, where I applied this work to the Ericsson Security Manager application.
          </aside>
		</section>

        <!-- Agenda -->
		<section>
		  <h3>Agenda</h3>

          <ul>
		    <li>Motivation</li>
            <li>Demo</li>
            <li>Implementation</li>
            <li>Experimental Results</li>
            <li>Literature Survey</li>
          </ul>
		</section>

        <!-- Motivation -->
        <section>
          <h3>Motivation</h3>
        </section>

        <section>
          <img src="basic-esm-chart.png">

          <aside class="notes">
            First, I'd like to tell you a bit about the target application of my efforts and experiments.
            Ericsson Security Manager is an application that telecom operators can used to check and configure the security settings of devices in their networks.
            These devices, referred to as "Assets", can be 5G base stations, Virtual Network Functions or other appliances.
            Usually, we are talking about thousands of devices here.
            <br>
            Ericsson Security Manager connects to these assets and checks their security configuration according to a set of policies.
            <br>
            Like many other applications these days, ESM is developed with a microservice architecture and deployed on Kubernetes.
          </aside>
        </section>

		<section data-auto-animate>
          <aside class="notes">
            To understand the background of Kubernetes, we first need to look at the recent history of microservices.
            Microservices are one of the key reasons for the rapid adoption of Kubernetes.
            Modern, "cloud-native" applications can be made up of dozens of microservices communicating with each other.
            Kubernetes takes care of running all of these microservices reliably across a cluster of machines by providing high-level abstraction such as Deployments, Services, and Service Discovery.
            This means we no longer need to worry about where which component runs.
            <br><br>
            Great. But that doesn't mean the system won't have any issues.
            To identify issues and potential bottlenecks, we need observability of the system.
            This will also allow us to reliably estimate the resource footprint of the system and its component.
            This way, we can take full advantage of the cloud.
          </aside>

          <div class="r-stack">
            <img src="random-service-mesh.svg" height="500em">
            <img class="fragment fade-in" src="cloud.png" height="350em">
          </div>

          <aside class="notes">
            As you know, one of the main promises of the cloud is elasticity: resources can be provisioned and released at any point in time, and we only need to pay for what we provision.
            How do we get elasticity from our application? -> Scaling
          </aside>
		</section>

        <section data-auto-animate>
          <div class="r-stack">
            <img src="random-service-mesh.svg" height="500em">
            <img src="cloud.png" height="700em">
          </div>
        </section>

        <section data-auto-animate>
          <div class="r-stack">
            <!-- <img src="random-service-mesh.svg" height="500em"> -->
            <img src="cloud.png" height="250em">
          </div>

          <aside class="notes">
            While most people only think about scaling up (so allocating more resources), the opposite is actually far more desirable: scaling down.
            A research paper from Google showed that significant cost savings can be realized by automatically adjusting the allocated resources,
            instead of relying on the resource guestimates made by developers.
          </aside>
        </section>

        <section>
          <h3>Vertical scaling:</h3>
          <i class="fragment">Adjusting compute and memory resources allocated to a component to the actual usage (scaling up or down)</i>

          <br><br>

          <h3>Horizontal scaling:</h3>
          <i class="fragment">Creating more replicas of the same component <br>(scaling in and out)</i>

          <aside class="notes">
            There are two dimensions for scaling: horizontal and vertical.

            Different applications and use-cases require different types of scaling.
            Sometimes, both approaches can be combined.
          </aside>
		</section>

		<!-- <section> -->
          <!--   <h3>Scaling in Kubernetes</h3> -->

		  <!--   <pre data-id="code-animation" class="fragment"><code class="hljs" data-trim> -->
              <!--       kubectl scale deployment/myapp --replicas=5 -->
		      <!--   </code></pre> -->

          <!--   <img src="random-load-over-time.png" class="fragment "> -->

          <!--   <p class="fragment"><b>Auto</b>scaling</p> -->
          <!--   <aside class="notes"> -->
            <!--     But how are we going to deal with varying load? -> Autoscaling, sometimes also referred to as adaptive scaling -->
            <!--   </aside> -->
		  <!-- </section> -->

        <!-- Demo -->
        <section>
          <h3>Demo</h3>

          <p>Part 1</p>
        </section>

        <!-- Implementation -->
        <section>
          <h3>Implementation</h3>

          <aside class="notes">
            Just setting up the autoscaler is the easy part.
            Let's talk a bit about how this actually works.
          </aside>
        </section>

        <section>
          <h3>Scaling based on metrics</h3>

          <img src="random-load-over-time.png">

          <p class="fragment">
            Which metrics?

            Throughput, latency, error rate, ...
          </p>

          <p class="fragment">
            And where are they coming from?
          </p>

          <aside class="notes">
            As you saw in the demo, a fundamental part of this work is the collection and aggregation of metrics.
            But the questions is: which types of metrics should we scale on? ...
            And where are they coming from?
            <br><br>
            My goal was to find out which metrics would be suitable to scale the executor component of Ericsson Security Manager
            and figure out how this scaling could be implemented.
        </section>

        <section>
          <span class="r-stack">
            <img src="metrics-collection-1.svg" class="fragment">
            <img src="metrics-collection-2.svg" class="fragment">
            <img src="metrics-collection-3.svg" class="fragment">
            <img src="metrics-collection.svg" class="fragment">
          </span>

          <aside class="notes">
            Platform-level metrics
            <br>
            Application-level metrics
            <br>
            Service-level metrics
            <br>
            <br>
            Great, now all the metrics are collected by Prometheus.
            But to be able to use them from the Kubernetes components (horizontal pod autoscaler and vertical pod autoscaler),
            we need to put the metrics "back" into Kubernetes.
          </aside>
        </section>

        <section>
          <img src="k8s-metrics-pipeline.svg">

          <aside class="notes">
            This is done through the so-called Kubernetes Metrics Pipeline.
            A special prometheus-adapter reads metrics from Prometheus and feeds them into the Kubernetes API.
            From there, any other Kubernetes component can access them through an API.
            Like for example the horizontal pod autoscaler.
          </aside>
        </section>

        <section>
          <pre><code data-trim data-line-numbers="2,6,9|13-17|19-23">
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: executor
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: executor
  metrics:
    - type: External
      external:
        metric:
          name: esm_executor_queue_messages
        target:
          type: AverageValue
          averageValue: 1
    behavior:
      scaleDown:
        policies:
          - type: Percent
            value: 50
            periodSeconds: 60
        stabilizationWindowSeconds: 60
          </code></pre>
        </section>

        <section>
          <h3>Demo</h3>

          <p>Part 2</p>
        </section>

        <!-- Experimental Results -->
        <section>
          <h3>Experimental Results</h3>
        </section>

        <section>
          <aside class="notes">
            As part of my work, I have also carried out several experiments to quantify the improvements.
            For this purpose, I used two criteria.
            <br>
            *Time-to-completion* to assess the performance.
            Practically speaking, this is the time between the user submitting jobs through the web interface (what I have done before) and the system finishing the job.
            <br>
            *Replica seconds* was used to measure the cost of horizontal scaling: the number of running pods per second integrated over the time period of the benchmark.
            For example, if the benchmark lasts 1 minute and we have a constant number of 2 replicas running all the time, this would result in 120 replica seconds.
          </aside>

          <p class="fragment">
            Performance: <b>Time-to-completion</b>
          </p>
          <p class="fragment">
            Cost: <b>Replica seconds</b>
          </p>

          <img class="fragment" src="real-scaling-activity-static.png">
        </section>

        <section>
          <img src="real-results-static_performance-cost-analysis.png">

          <span class="r-stack">
            <p class="fragment current-visible">
              Compared to static dimensioning with 10 replicas:
              <br>
              <b>19.3% lower cost, same performance (maximum reachable)</b>
            </p>

            <!-- real results static: -->
            <!-- 5: (24235+24295+24305)/3 = 24278.3333333 -->
            <!-- sp-v1: (36257+36207+35963)/3 = 36142.3333333 -->

            <!-- cost: 1 - 36142.3333333/24278.3333333 = 48.9% -->

            <!-- sp-v1: (3199+3195+3209)/3 = 3201 -->
            <!-- 5:  (4864+4883+4881)/3 = 4876 -->
            <!-- performance: 1 - 3201/4876 = 34.4% -->

            <p class="fragment current-visible">
              Compared to static dimensioning with 5 replicas:
              <br>
              <b>48.9% higher cost, 34.4% better performance</b>
            </p>
          </span>
        </section>

        <section>

          <aside class="notes">
            Another aspect I look at was the variance in the execution time of individual tasks.
            In the previous slide we saw that there is very little variance in the overall execution time (time-to-completion).
            But the autoscaling actually introduces a some variability on an individual task level.
            This is what we can see here ...
            <br>
            This can be explained by the fact that autoscaler is purely reactive, so sometimes there might not be immediately enough workers available to complete the new tasks.
            Then, new replicas need to be created first before the task can be processed.
          </aside>

          <img src="real-results-static_config-run-variance.png">

          Slight increase in variance of individual task durations
        </section>

        <!-- Research -->
        <section>
          <aside class="notes">
            As part of my thesis, I have also conducted an extensive literature survey about research proposals specifically for Kubernetes autoscalers.
          </aside>

          <h3>Literature survey about <br> Kubernetes autoscalers</h3>

          <br>
          <div style="width: 40%; float: left;" class="fragment fade-in-then-semi-out">
            <ul>
              <li>KHPA-A</li>
              <li>Libra</li>
              <li>RUBAS</li>
              <li>HPA+</li>
              <li>Microscaler</li>
              <li>Q-Threshold</li>
              <li>me-kube</li>
            </ul>
          </div>

          <div style="width: 59%; float: right;" class="fragment">
            <ul>
              <li>proactive & reactive approaches</li>
              <li>forecast-based scaling</li>
              <li>reinforcement learning-based scaling</li>
              <li>workload patterns</li>
              <li>different metrics</li>
              <li>not open source</li>
            </ul>
          </div>

          <aside class="notes">
            HPA+: forecast-based autoscaler, uses four different machine learning models under the hood
            <br>
            Q-Learning: reactive, reinforcement-learning approach; input: desired response time, output: required CPU threshold for scaling
          </aside>
        </section>

        <section> <!-- Questions -->
          <h3>Dimensioning, Performance and Optimization of Cloud-native Applications</h3>
          <br>
          <p>
            Jack Henschel
            <br>
            <small>2021-06-22</small
          </p>
          <br>
          <br>
          <img src="erasmus-mundus.png" height="100em">
        </section>

      </div>
    </div>

    <script src="dist/reveal.js"></script>
    <script src="plugin/zoom/zoom.js"></script>
    <script src="plugin/notes/notes.js"></script>
    <script src="plugin/search/search.js"></script>
    <script src="plugin/markdown/markdown.js"></script>
    <script src="plugin/highlight/highlight.js"></script>
    <script>
      Reveal.initialize({
	      controls: false,
	      progress: true,
	      center: true,
	      hash: true,
          transition: "slide", //"none",
          transitionSpeed: 'fast',

	      // Learn about plugins: https://revealjs.com/plugins/
	      plugins: [ RevealZoom, RevealNotes, RevealSearch, RevealMarkdown, RevealHighlight ]
      });
    </script>
  </body>
</html>
