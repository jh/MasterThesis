%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lachaise Assignment
% LaTeX Template
% Version 1.0 (26/6/2018)
%
% This template originates from:
% http://www.LaTeXTemplates.com
%
% Authors:
% Marion Lachaise & François Févotte
% Vel (vel@LaTeXTemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Master's Thesis description} % Title of the assignment

\author{Jack Henschel\\ \texttt{jack.henschel@aalto.fi}} % Author name and email address

\date{\textit{Supervisors:}\\ Prof. Mario Di Francesco, Aalto University, Finland\\ Prof. Raja Appuswamy, EURECOM, France\\ \vspace{1em}\textit{Thesis instructor:}\\ Yacine Khettab, Ericsson, Finland} % University, school and/or department name(s) and a date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section{Introduction}

Cloud computing is the latest incarnation of a continuous trend to make computing resources more flexible.
The introduction of multi-tasking (also known as \emph{time-sharing}) operating systems allowed simultaneously running multiple different applications on the same physical hardware.
This trend is also reflected in the shift from specialized servers (\textit{mainframes}) to using commodity hardware at a large scale.
Finally, virtualization even abstracts away this commodity hardware such that only a computing substrate, an abstract platform for performing computations, is left.
System administrators no longer need to decide on which specific machine an application will run and worry about if there will be enough resources available.
This is the promise of ``the cloud'': full elasticity of computing resources.
In the cloud, one can dynamically request the amount of resources, unlike buying a fixed amount of physical servers, and only needs to pay for exactly these resources.

Currently, the most widely used platform that provides such a computing substrate is Kubernetes.
This is evident by the fact that all major public cloud platforms offer a managed Kubernetes service (AWS EKS, GCP GKE, Azure AKS, IBM Cloud Kubernetes, AlibabaCloud ACK).
At its core, Kubernetes is a system for automating the deployment, scaling and management of distributed applications.
Kubernetes provides a declarative API that is used to describe the desired state of the system.
For example, when instructed to \textit{run 5 instances of this application}, it will continuously ensure that there are 5 instances running somewhere, no matter how the physical resources below are behaving (machine failures, network outages etc.).

However, this resiliency to machine failures is not enough to truly deliver the elasticity promise of the cloud.
There needs to be a way to automatically scale the system, both in terms of total size (number of \textit{nodes} in the system) and the number of applications within the system (number of \textit{replicas}).
In recent years, a new application architecture has emerged which fulfills this promise: event-driven architectures or \textit{Serverless applications}.
In this computing model, even the application runtime (and all layers below it) is provisioned by the cloud platform.
The application is only running when there are incoming requests and terminated afterwards.
Unlike in long-lived server processes, no state is guaranteed to be persisted between different requests.
Thus, the developer does not need to deal with any servers or processes and hence the misnomer ``Serverless'' emerged.

\section{Challenges}

Over the course of the last decade, deploying applications into a distributed system --- safely and reliably --- has become incredibly easy.
This was mainly facilitated by the standardization and broad adoption of the OCI (\textit{open container image}) format, initially introduced by Docker.
However, scaling these distributed applications depending on workload still remains highly technical.
In particular, the following challenges need to be tackled:

\begin{itemize}
\item
  Specifying desired QoS (\textit{quality of service}) levels, such as response latency, throughput, CPU and memory usage etc.
\item
  Monitoring of the application to detect violation or near-violation of QoS levels.
\item
  Scaling the application in the right dimension (horizontal vs.\ vertical) and determining the right level (e.g.\ amount of replicas).
  Importantly, this includes both scaling up as well as scaling down.
\item
  Provisioning the right compute resources in the cluster to handle the current scale.
\end{itemize}

Ideally, most of the steps described above should happen automatically, with no or very little human operator intervention.

\section{Goals}

This thesis will investigate the challenges related to automatic scaling of cloud applications described above, so that in the future auto-scaling becomes accessible to a broader audience.
More specifically, the following goals will be worked towards:

\begin{enumerate}
\item
  \textbf{Introduce} the Kubernetes orchestration platform.
  Kubernetes is an incredibly extensible mesh of microservices, which also makes it prohibitively complex. Therefore it is absolute necessary to have firm understanding of its components in order to effectively operate and optimize it.
\item
  Perform an \textbf{extensive literature review} on the state-of-the-art in auto-scaling mechanism with a focus on Kubernetes-based solutions.
  In recent years, there has been a significant amount of research in this area, thus an overview of this body of research is required.
\item
  Based on the literature research, \textbf{compare and evaluate the suggested methods}.
\item
  Based on the literature research, \textbf{identify approaches and metrics} that can be used for detecting required scaling adjustments.
  This will also include aspects that have not been researched extensively, such as the deployment topology and the interactions between application components.
\item
  \textbf{Study} the suitability of event-driven architectures (``Serverless applications'') for the scaling of certain workloads.
  Serverless represents a radical departure from traditional service-driven architectures. The two concepts are not mutually exclusive, rather they complement each other and are suitable for different use cases.
\item
  \textbf{Implement} a proof-of-concept auto-scaling mechanism in one of Ericsson's applications.
\item
  \textbf{Evaluate} the implementation based on the metrics identified above. Also the practical usability of the implementation shall be considered (easy of use, robustness, technical complexity etc.).
\end{enumerate}

Furthermore, if time permits the following topics may be investigated:

\begin{enumerate}
  \item
    Research the availability of --- or propose --- a standardized test framework to compare different auto-scaling mechanisms.
  \item
    Explore the possibility of automatically identifying the connections between different application components (\textit{microservices}).
    Deriving the graph-like relationships between individual application components can be valuable to identify performance bottlenecks and scale the components accordingly.
\end{enumerate}

\section{Time schedule}

The work on the thesis begins on \textbf{February 15, 2021}.
The final two weeks of February will be used to explore and get to know the Ericsson's application, in order to better understand potential scaling needs and opportunities.
During March, the literature review described above will be performed.
During April, the proposed solutions found in the literature will be reviewed and compared.
Meanwhile, also the suitability of Serverless architectures will be studied.
From May on, work on the proof-of-concept auto-scaling implementation shall start.
In June, a preliminary thesis presentation will be given for Aalto University (date to be announced).
In July, the results of the implementation will be formally evaluated and the conclusion of the thesis will be written.
The submission deadline is \textbf{July 30, 2021}.

%% \subsection{Theoretical viewpoint}

%% Maecenas consectetur metus at tellus finibus condimentum. Proin arcu lectus, ultrices non tincidunt et, tincidunt ut quam. Integer luctus posuere est, non maximus ante dignissim quis. Nunc a cursus erat. Curabitur suscipit nibh in tincidunt sagittis. Nam malesuada vestibulum quam id gravida. Proin ut dapibus velit. Vestibulum eget quam quis ipsum semper convallis. Duis consectetur nibh ac diam dignissim, id condimentum enim dictum. Nam aliquet ligula eu magna pellentesque, nec sagittis leo lobortis. Aenean tincidunt dignissim egestas. Morbi efficitur risus ante, id tincidunt odio pulvinar vitae.

%% Curabitur tempus hendrerit nulla. Donec faucibus lobortis nibh pharetra sagittis. Sed magna sem, posuere eget sem vitae, finibus consequat libero. Cras aliquet sagittis erat ut semper. Aenean vel enim ipsum. Fusce ut felis at eros sagittis bibendum mollis lobortis libero. Donec laoreet nisl vel risus lacinia elementum non nec lacus. Nullam luctus, nulla volutpat ultricies ultrices, quam massa placerat augue, ut fringilla urna lectus nec nibh. Vestibulum efficitur condimentum orci a semper. Pellentesque ut metus pretium lacus maximus semper. Sed tellus augue, consectetur rhoncus eleifend vel, imperdiet nec turpis. Nulla ligula ante, malesuada quis orci a, ultricies blandit elit.

%% % Numbered question, with subquestions in an enumerate environment
%% \begin{question}
%% 	Quisque ullamcorper placerat ipsum. Cras nibh. Morbi vel justo vitae lacus tincidunt ultrices. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

%% 	% Subquestions numbered with letters
%% 	\begin{enumerate}[(a)]
%% 		\item Do this.
%% 		\item Do that.
%% 		\item Do something else.
%% 	\end{enumerate}
%% \end{question}

%% %------------------------------------------------

%% \subsection{Algorithmic issues}

%% In malesuada ullamcorper urna, sed dapibus diam sollicitudin non. Donec elit odio, accumsan ac nisl a, tempor imperdiet eros. Donec porta tortor eu risus consequat, a pharetra tortor tristique. Morbi sit amet laoreet erat. Morbi et luctus diam, quis porta ipsum. Quisque libero dolor, suscipit id facilisis eget, sodales volutpat dolor. Nullam vulputate interdum aliquam. Mauris id convallis erat, ut vehicula neque. Sed auctor nibh et elit fringilla, nec ultricies dui sollicitudin. Vestibulum vestibulum luctus metus venenatis facilisis. Suspendisse iaculis augue at vehicula ornare. Sed vel eros ut velit fermentum porttitor sed sed massa. Fusce venenatis, metus a rutrum sagittis, enim ex maximus velit, id semper nisi velit eu purus.

%% \begin{center}
%% 	\begin{minipage}{0.5\linewidth} % Adjust the minipage width to accomodate for the length of algorithm lines
%% 		\begin{algorithm}[H]
%% 			\KwIn{$(a, b)$, two floating-point numbers}  % Algorithm inputs
%% 			\KwResult{$(c, d)$, such that $a+b = c + d$} % Algorithm outputs/results
%% 			\medskip
%% 			\If{$\vert b\vert > \vert a\vert$}{
%% 				exchange $a$ and $b$ \;
%% 			}
%% 			$c \leftarrow a + b$ \;
%% 			$z \leftarrow c - a$ \;
%% 			$d \leftarrow b - z$ \;
%% 			{\bf return} $(c,d)$ \;
%% 			\caption{\texttt{FastTwoSum}} % Algorithm name
%% 			\label{alg:fastTwoSum}   % optional label to refer to
%% 		\end{algorithm}
%% 	\end{minipage}
%% \end{center}

%% Fusce varius orci ac magna dapibus porttitor. In tempor leo a neque bibendum sollicitudin. Nulla pretium fermentum nisi, eget sodales magna facilisis eu. Praesent aliquet nulla ut bibendum lacinia. Donec vel mauris vulputate, commodo ligula ut, egestas orci. Suspendisse commodo odio sed hendrerit lobortis. Donec finibus eros erat, vel ornare enim mattis et.

%% % Numbered question, with an optional title
%% \begin{question}[\itshape (with optional title)]
%% 	In congue risus leo, in gravida enim viverra id. Donec eros mauris, bibendum vel dui at, tempor commodo augue. In vel lobortis lacus. Nam ornare ullamcorper mauris vel molestie. Maecenas vehicula ornare turpis, vitae fringilla orci consectetur vel. Nam pulvinar justo nec neque egestas tristique. Donec ac dolor at libero congue varius sed vitae lectus. Donec et tristique nulla, sit amet scelerisque orci. Maecenas a vestibulum lectus, vitae gravida nulla. Proin eget volutpat orci. Morbi eu aliquet turpis. Vivamus molestie urna quis tempor tristique. Proin hendrerit sem nec tempor sollicitudin.
%% \end{question}

%% Mauris interdum porttitor fringilla. Proin tincidunt sodales leo at ornare. Donec tempus magna non mauris gravida luctus. Cras vitae arcu vitae mauris eleifend scelerisque. Nam sem sapien, vulputate nec felis eu, blandit convallis risus. Pellentesque sollicitudin venenatis tincidunt. In et ipsum libero. Nullam tempor ligula a massa convallis pellentesque.

%% %----------------------------------------------------------------------------------------
%% %	PROBLEM 2
%% %----------------------------------------------------------------------------------------

%% \section{Implementation}

%% Proin lobortis efficitur dictum. Pellentesque vitae pharetra eros, quis dignissim magna. Sed tellus leo, semper non vestibulum vel, tincidunt eu mi. Aenean pretium ut velit sed facilisis. Ut placerat urna facilisis dolor suscipit vehicula. Ut ut auctor nunc. Nulla non massa eros. Proin rhoncus arcu odio, eu lobortis metus sollicitudin eu. Duis maximus ex dui, id bibendum diam dignissim id. Aliquam quis lorem lorem. Phasellus sagittis aliquet dolor, vulputate cursus dolor convallis vel. Suspendisse eu tellus feugiat, bibendum lectus quis, fermentum nunc. Nunc euismod condimentum magna nec bibendum. Curabitur elementum nibh eu sem cursus, eu aliquam leo rutrum. Sed bibendum augue sit amet pharetra ullamcorper. Aenean congue sit amet tortor vitae feugiat.

%% In congue risus leo, in gravida enim viverra id. Donec eros mauris, bibendum vel dui at, tempor commodo augue. In vel lobortis lacus. Nam ornare ullamcorper mauris vel molestie. Maecenas vehicula ornare turpis, vitae fringilla orci consectetur vel. Nam pulvinar justo nec neque egestas tristique. Donec ac dolor at libero congue varius sed vitae lectus. Donec et tristique nulla, sit amet scelerisque orci. Maecenas a vestibulum lectus, vitae gravida nulla. Proin eget volutpat orci. Morbi eu aliquet turpis. Vivamus molestie urna quis tempor tristique. Proin hendrerit sem nec tempor sollicitudin.

%% % File contents
%% \begin{file}[hello.py]
%% \begin{lstlisting}[language=Python]
%% #! /usr/bin/python

%% import sys
%% sys.stdout.write("Hello World!\n")
%% \end{lstlisting}
%% \end{file}

%% Fusce eleifend porttitor arcu, id accumsan elit pharetra eget. Mauris luctus velit sit amet est sodales rhoncus. Donec cursus suscipit justo, sed tristique ipsum fermentum nec. Ut tortor ex, ullamcorper varius congue in, efficitur a tellus. Vivamus ut rutrum nisi. Phasellus sit amet enim efficitur, aliquam nulla id, lacinia mauris. Quisque viverra libero ac magna maximus efficitur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum mollis eros in tellus fermentum, vitae tristique justo finibus. Sed quis vehicula nibh. Etiam nulla justo, pellentesque id sapien at, semper aliquam arcu. Integer at commodo arcu. Quisque dapibus ut lacus eget vulputate.

%% % Command-line "screenshot"
%% \begin{commandline}
%% 	\begin{verbatim}
%% 		$ chmod +x hello.py
%% 		$ ./hello.py

%% 		Hello World!
%% 	\end{verbatim}
%% \end{commandline}

%% Vestibulum sodales orci a nisi interdum tristique. In dictum vehicula dui, eget bibendum purus elementum eu. Pellentesque lobortis mattis mauris, non feugiat dolor vulputate a. Cras porttitor dapibus lacus at pulvinar. Praesent eu nunc et libero porttitor malesuada tempus quis massa. Aenean cursus ipsum a velit ultricies sagittis. Sed non leo ullamcorper, suscipit massa ut, pulvinar erat. Aliquam erat volutpat. Nulla non lacus vitae mi placerat tincidunt et ac diam. Aliquam tincidunt augue sem, ut vestibulum est volutpat eget. Suspendisse potenti. Integer condimentum, risus nec maximus elementum, lacus purus porta arcu, at ultrices diam nisl eget urna. Curabitur sollicitudin diam quis sollicitudin varius. Ut porta erat ornare laoreet euismod. In tincidunt purus dui, nec egestas dui convallis non. In vestibulum ipsum in dictum scelerisque.

%% % Warning text, with a custom title
%% \begin{warn}[Notice:]
%%   In congue risus leo, in gravida enim viverra id. Donec eros mauris, bibendum vel dui at, tempor commodo augue. In vel lobortis lacus. Nam ornare ullamcorper mauris vel molestie. Maecenas vehicula ornare turpis, vitae fringilla orci consectetur vel. Nam pulvinar justo nec neque egestas tristique. Donec ac dolor at libero congue varius sed vitae lectus. Donec et tristique nulla, sit amet scelerisque orci. Maecenas a vestibulum lectus, vitae gravida nulla. Proin eget volutpat orci. Morbi eu aliquet turpis. Vivamus molestie urna quis tempor tristique. Proin hendrerit sem nec tempor sollicitudin.
%% \end{warn}

%% %----------------------------------------------------------------------------------------

\end{document}
