# Master Thesis




## LaTeX

Aalto Thesis Template (version 3.20) from https://wiki.aalto.fi/display/Aaltothesis/Aalto+Thesis+LaTeX+Template

Aalto Logo Package from https://wiki.aalto.fi/display/aaltolatex/

The produced PDF file should conform to PDF/A-1b or PDF/A-2b

Online PDF/A validator: https://www.pdf-online.com/osa/validate.aspx

## Diff

Generate a diff PDF with git-latexdiff: https://gitlab.com/git-latexdiff/git-latexdiff

```
./git-latexdiff --prepare "make $(echo include/*.tex)" --main thesis.tex -b --latexmk --ignore-latex-errors -t CFONT -o diff.pdf 7c9ad7c --
```
