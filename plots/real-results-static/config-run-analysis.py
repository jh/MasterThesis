import matplotlib.pyplot as plt
import seaborn as sns
import json
import sys

with open('out.json', 'r') as fd:
    data = json.load(fd)


# labels = list(data.keys())
labels = ['sp-v1', 'sp-v2', 'keda', '5', '10', '20']
expanded_labels = []
config_run_times = []

for label in labels:
    expanded_labels.append(label)
    exec_times = []
    for run in data[label]:
        exec_times += run['execution_times']

    config_run_times.append(exec_times)

fig, axl = plt.subplots(nrows=1, ncols=1, figsize=(7, 3))
axl.yaxis.grid(True, zorder=0.4)
axl.set_ylabel('Configuration Run Time (in Seconds)')
axl.set_xlabel('Scaling Setting')

# Do a regular boxplot:
# bplot2 = axl.boxplot(config_run_times,
#                      notch=False,  # notch shape
#                      vert=True,  # vertical box alignment
#                      patch_artist=True,  # fill with color
#                      labels=["[" + l + "]" for l in expanded_labels], # will be used to label x-ticks
#                      )
# axl.set_xticks(range(1, len(labels) + 1))
# axl.set_xticklabels(labels)

# Do a violin plot
plot = sns.violinplot(data=config_run_times,
                      ax=axl,
                      scale="count",
               )

axl.set_xticks(range(0, len(labels)))
axl.set_xticklabels(labels)

# Add median values at the top:
def flatten(list_of_lists):
    return [val for sublist in list_of_lists for val in sublist]

def median(alist):
    return "%.1f" % (sum(alist)/len(alist),)

#axl.set_yticks([i*10 for i in range(0,200,25)])
#plot_y_max = max(flatten(config_run_times))+600
#plot_y_min = min(flatten(config_run_times))-200
#offset = 200
axl.set_yticks(range(0,1500,150))
plot_y_max = max(flatten(config_run_times))+300
plot_y_min = min(flatten(config_run_times))-150
offset = 75


axl.set_ylim([plot_y_min,plot_y_max])

for i, label in enumerate(labels):
    axl.text(i, plot_y_max-offset, median(config_run_times[i]),
             horizontalalignment='center',color='b')


fig.tight_layout()
fig.savefig('config-run-variance.pdf',
            format='pdf',
            # dpi=100,
            # bbox_inches='tight',
            )
