import matplotlib.pyplot as plt
import seaborn as sns
import json
import sys

with open('scaling-benchmarks.json', 'r') as fd:
    data = json.load(fd)


#labels = list(data.keys())
#labels = ['2', '3', '5', '10', '15', '20', 'hpav0']
labels = ['5', '10', 'hpav0', 'hpav1', 'hpav2', 'hpav3', 'hpav4']

fig, axr = plt.subplots(nrows=1, ncols=1, figsize=(7, 3.5))
axr.grid(True, zorder=1.0)

for label in labels:
    x_vals = [run['time_to_completion'] for run in data[label]]
    y_vals = [run['replica_seconds'] for run in data[label]]
    sns.scatterplot(
        x=x_vals,
        y=y_vals,
        zorder=2.0, # make sure it's on top of the grid
        s=70, # controls size of dots
        label=label,
    )

    # text_x = max(x_vals) + 25
    # text_y = max(y_vals) + 25
    # if label == "2":
    #     text_x -= 15
    #     text_y += 0.1*text_y
    # if label == "20":
    #     text_y -= 0.01*text_y
    # axr.text(text_x, text_y, "[" + label + "]")

axr.legend(title='Scaling setting:')
axr.set_xlabel('Performance: Time to Completion (in Seconds)')
axr.set_ylabel('Cost: Replica Seconds')

fig.tight_layout()

# save the plot as a file
fig.savefig('performance-cost-benchmark-autoscaling.pdf',
            format='pdf',
            )
